addpath(genpath('../')) 
resultsPath = '../Results/2016-06-06_17-35-38/LAMBDA/';
gtPath = '../Images/UKE/images_crop/gt/';

% Get all scores as matrix NOFiles x Sweeps x 2 (Dicescore, edgescore)
files = dir([trainImagesPath '*.png']);
sweepValues = [0.0001:0.0001:0.001 0.002:0.001:0.02] ;
scores = zeros(length(files), length(sweepValues), 2);

paramName = 'LAMBDA';

% Get List of files
thetaFileNames = dir([resultsPath 'LAMBDA_*.mat']);
thetaFileNames = {thetaFileNames.name};

%% Loop
for i = 1:length(thetaFileNames)   

    currFile = char(thetaFileNames(i));
    
    % Load theta
    load(currFile);
    
    % Load GT
    currFileSplit = strsplit(currFile, '_');
    gtFileName = [strjoin(currFileSplit(3:5), '_') '_gt.png'];
    
    thetaGT = LoadGT(gtPath, gtFileName);
    
    
    % Parse sweepvalue
    sweepNameSplit = strsplit(char(currFileSplit(end)), '.');
    sweepValue = str2double(char(strjoin(sweepNameSplit(1:2), '.')));
    sweepVarIndex = find(round(sweepValues, 10) == round(sweepValue, 10));
    %% Evaluate Data
    currFile = [strjoin(currFileSplit(3:5), '_') '.png'];
    [~, fileIndex] = ismember(char(files.name), currFile, 'rows');
    fileIndex = find(fileIndex);
    
    scores(fileIndex, sweepVarIndex, 1) = calculateDiceScore(thetaGT, theta);
    
    
    theta = ResizeGT(theta);
    thetaGT = ResizeGT(thetaGT);
    scores(fileIndex, sweepVarIndex, 2) = EvaluateEdge(thetaGT, theta);
    
    if(~mod(i, 1))
        disp(num2str(i));
    end
end
   

h = PlotScores(sweepValues, scores, paramName);
saveas(h, [resultsPath paramName '.png']);


save([resultsPath 'SCORES_' paramName], 'scores')
