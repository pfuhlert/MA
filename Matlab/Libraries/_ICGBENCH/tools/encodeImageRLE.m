%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function output_string = encodeImageRLE(image)
% function output_string = encodeImageRLE(image)
%
% this function encodes an image using the run-lenght encoding algorithm
% therefore, it returns a string containing lines stating 
% [run_lenght element ] of the linearized image in breadth-first order

image_t = image';
encoded_data = rle(image_t(:));

output_string = [];

for i = 1:length(encoded_data{1})
    output_string = sprintf('%s%d %d ', output_string, encoded_data{1}(i), encoded_data{2}(i));    
end