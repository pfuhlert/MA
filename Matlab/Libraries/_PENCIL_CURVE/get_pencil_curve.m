function P = get_pencil_curve(color)
  % GET_PENCIL_CURVE Get a curve (sequence opoints) from the user by dragging
  % on the current plot window
  %
  % P = get_pencil_curve(color)
  % P = get_pencil_curve(color, f)
  % 
  % Inputs:
  %   f  figure id
  %   color color of curve
  % Outputs:
  %   P  #P by 2 list of point positions
  %
  %
  % Callback for mouse press
  function ondown(src,ev)
    % Tell window that we'll handle drag and up events
    set(gcf,'windowbuttonmotionfcn', @ondrag);
    set(gcf,'windowbuttonupfcn',     @onup);
    append_current_point();
  end

  % Callback for mouse drag
  function ondrag(src,ev)
    append_current_point();
  end

  % Callback for mouse release
  function onup(src,ev)
    % Tell window to handle down, drag and up events itself
    finish();
  end

  function append_current_point()
    % get current mouse position
    cp = get(gca,'currentpoint');
    cp = round(cp); 
    % append to running list
    P = [P;cp(1,:)];
        
    if isempty(p)
      % init plot
      hold on;
      
      % Convert marker size to pixel units like in [M2]
      currentunits = get(gca,'Units');
      set(gca, 'Units', 'Points');
      axpos = get(gca,'Position');
      set(gca, 'Units', currentunits);
      markerWidth = (PARAMS.BRUSHSIZE.^2)/diff(xlim)*axpos(3); % Calculate Marker width in points

      p = scatter(P(:,1),P(:,2), markerWidth, color, 'filled');
      
      hold off;
    else
      % update plot
      set(p,'Xdata',P(:,1),'Ydata',P(:,2));
    end
  end

  function finish()
    done = true;
    set(gcf,'windowbuttonmotionfcn','');
    set(gcf,'windowbuttonupfcn','');
    set(gcf,'windowbuttondownfcn','');
    set(gcf,'keypressfcn','');
  end
  
  set(gcf,'windowbuttondownfcn',@ondown);
  % clear living variables
  P = [];
  p = [];

  % loop until mouse up or ESC is pressed
  done = false;
  while(~done)
    drawnow;
  end

  % We've been also gathering Z coordinate, drop it
  P = P(:,[1 2]);
 
  

end