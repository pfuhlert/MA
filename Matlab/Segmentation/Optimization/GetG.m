function [ G ] = GetG( I)
%GETG Summary of this function goes here
%   Detailed explanation goes here

[gxr, gyr] = GetGradient(I(:,:,1));
[gxg, gyg] = GetGradient(I(:,:,2));
[gxb, gyb] = GetGradient(I(:,:,3));

% Norm Gradient to 1 to match parameter values from implementation [6]
GradXMax = max(max([gxr,gxg,gxb]));
GradYMax = max(max([gyr,gyg,gyb]));

if((GradXMax ~= 0) && (GradYMax ~= 0))
    gxr = gxr ./ GradXMax;
    gxg = gxg ./ GradXMax;
    gxb = gxb ./ GradXMax;
    gyr = gyr ./ GradYMax;
    gyg = gyg ./ GradYMax;
    gyb = gyb ./ GradYMax;
else
    warning('Norming of GetG gradient failed!');
end

% Take L2 gradient norm
GradXSquared = gxr.^2 + gxg.^2 + gxb.^2;
GradYSquared = gyr.^2 + gyg.^2 + gyb.^2;
GradNorm = sqrt(GradXSquared + GradYSquared);

% Equation 16 of [2]: g(x) = exp(-GAMMA * norm(gradient(I)))
G = exp(-PARAMS.GAMMA * GradNorm);

end