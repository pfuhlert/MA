function [ Glarepoints ] = GetGlarepoints( Image )
%GETGLAREPOINTS Orienting on the Canny Edge detector, this algorithm uses
%two thresholds to extract points with high brightness

T1 = 240;
T2 = 225;

% Neighborhood size around T1
K_MAX = 10;

GrayImage = rgb2gray(Image);

Image_T1 = GrayImage > T1;
Image_T2 = GrayImage > T2;

% Conv with window where T2 is allowed:
for k = 2:K_MAX

    Image_T1_k = logical(conv2(double(Image_T1), ones(k), 'same'));

    Image_T2_k = Image_T2 & Image_T1_k;
    
    if(k ~= 2)
    
        % Expand by new Image_T2
        Image_T2_k = Image_T2_k | Image_T2_Last;

        % Terminate if they are equal
        if(isequal(Image_T2_k, Image_T2_Last))
            break;
        end
    end  
    
    Image_T2_Last = Image_T2_k;   
    
end

Glarepoints = Image_T2_k;


% Do not use all found points, but only a few as seeds:
RandImage = rand(size(Image,1), size(Image, 2));

% Thin out randomly
Glarepoints = (Glarepoints .* RandImage) > (1 - CONST.GLAREPOINT_SCRIB_PCT);
end

