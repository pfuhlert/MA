function [pred] = classify_on_rgb(pca_dimension)
% Train + Classify for SIFT + PCA. 
% Depends on pca_dimension and test_patient_folder.

%Shared variable.
global num_patients;
global test_patient_folder;
global patients_liver;
global patients_bkg;
global numel_liver_images;
global numel_bkg_images;


%Set svm parameter.
option.MaxIter=50000;


size_of_test_images = numel(patients_liver(test_patient_folder).roi) + numel(patients_bkg(test_patient_folder).roi);

%Create an empty matrix.
pred = zeros(size_of_test_images,3);
%Create index. 
pred(:,1) = (1:1:size_of_test_images);
%Create Ground truth.
pred(:,2) = [ones(numel(patients_liver(test_patient_folder).roi),1); zeros(numel(patients_bkg(test_patient_folder).roi),1)];




%Load Training Data.
training_data_vector = [];
%Liver
for pat_no = 1:num_patients  %Load all patients for training.
    if pat_no ~=  test_patient_folder %Except the patient who is to be tested.
        for i = 1 : (numel(patients_liver(pat_no).roi)) %For every image in the folder.
            %Create training_data_vector.  Each image is a row of size
            %50*50*3.
            training_data_vector = [ training_data_vector ; resizeRoiForRGB(patients_liver(pat_no).roi{i})];
        end
    end
end

%Bkg.
for pat_no = 1: num_patients
    if pat_no ~=  test_patient_folder
        for i = 1 : (numel(patients_bkg(pat_no).roi))
            training_data_vector = [ training_data_vector ; resizeRoiForRGB(patients_bkg(pat_no).roi{i})];
        end
    end
end

%Create Training labels.
liver_images = ones(numel_liver_images- numel(patients_liver(test_patient_folder).roi) ,1);
bkg_images = zeros(numel_bkg_images - numel(patients_bkg(test_patient_folder).roi) ,1);
train_class_membership_vector = [liver_images ; bkg_images];



%Load Testing Data.
testing_data_vector = [];
for i = 1 : (numel(patients_liver(test_patient_folder).roi))
    testing_data_vector = [ testing_data_vector ; resizeRoiForRGB(patients_liver(test_patient_folder).roi{i})];
end

for i = 1 : (numel(patients_bkg(test_patient_folder).roi))
    testing_data_vector = [ testing_data_vector ; resizeRoiForRGB(patients_bkg(test_patient_folder).roi{i})];
end

%Create testing labels.
test_liver_images = ones(numel(patients_liver(test_patient_folder).roi) ,1);
test_bkg_images = zeros(numel(patients_bkg(test_patient_folder).roi) ,1);
test_class_membership_vector = [test_liver_images ; test_bkg_images];


%Do pca on Training data and reduce the dim to #pca_dimension. Get test
%data shifted to these pca_dimensions.
[W_pca TW_pca] = doPCAwithDimReduction(training_data_vector, testing_data_vector, pca_dimension);


%Train SVM.
svmModel = svmtrain(W_pca,train_class_membership_vector);

%Classify.
for i = 1: numel(pred(:,1))
    pred(i,3) = svmclassify(svmModel, TW_pca(i,:), 'Showplot',false);
end

%Show plot of the 1st 2 dimensions.
%HACK ALERT: Using a specefic test_patient_folder and pca_dimension check
%to ensure the plot happens only once. By design, this script will be
%called multiple times on the same data (leave one out!) and hence the
%check stops replotting of the same data multiple times.
if test_patient_folder == 1 && pca_dimension == 2
    figure;
    gscatter(W_pca(:,1),W_pca(:,2),train_class_membership_vector,[], '*oxs');
end

clear training_data_vector
clear train_class_membership_vector
clear testing_data_vector
clear test_class_membership_vector
clear W_pca
clear TW_pca


