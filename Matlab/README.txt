
This section gives a brief overview of how to use the functions of the
previously presented GUI provided with this work. It should always be 
started by choosing Matlab/GUI as the current matlab folder.

The first thing to do starting a new segmentation is loading an image on an
empty canvas. To make sure no previously loaded images, scribbles or
segmentation results are still present in the program, a restart should be done
whenever a new segmentation starts. The user can choose either loading an
arbitrary image or load an image, corresponding scribbles and groundtruth data
from the ICG database. It is also possible to clear the GUI completely.

After loading an image, The user can start drawing scribbles on the
top right canvas that was filled with the original image. To draw a scribble,
the user has to select the scribble group before clicking on ``Add Scribbles".
After that, it is possible to draw on the canvas as long as the left mouse
button is pressed. The scribbles should become visible. After the user release
the mouse, the process has to start again by first choosing the scribble
group, then clicking on ``Add Scribbles" again and start drawing. The user
input can be saved and loaded using the corresponding buttons. It is also
possible to save an image of the canvas as is.

The third region of the GUI runs the actual segmentation algorithm.

The last part of the GUI compares the segmentation result to previously
generated ground-truth data. The user can load a binary png image that he wants
to compare his segmentation to. For now, the groundtruth images that can be
loaded only support two regions. Additionaly, it is possible to quantify the
segmentation against the loaded groundtruth data for the dice or edge score. 

###

Throughout the implementation, there are several references to papers or the
overall thesis indicated by brackets [n]:

 [0] The thesis provided with the code by Patrick Fuhlert (2016):
     Variation Based Segmentation of Liver Surface from Monocular Mini-Laparoscopic Sequences

 [1] Nieuwenhuis, Toeppe, Cremers (2013):
     A survey and comparison of discrete and continuous multilabel segmentation approaches.

 [2] Nieuwenhuis, Cremers (2013): 
     Spatially varying color distributions for interactive multilabel segmentation.
     
 [4] Weight from Distance for edge based segmentation
     Li, Hui; Cai, Jianfei; Nguyen, Thi Nhat Anh; Zheng, Jianmin (2013): A benchmark for semantic image segmentation. In: Multimedia and Expo (ICME), 2013 IEEE International Conference on. IEEE, S. 1–6.

 [5] Sinha, Rahul: Automatic Seed Selection for Segmentation of Liver Cirrhosis in Laparoscopic Sequences.

 [6] Nieuwenhuis, Cremers (2013): 
     Implementation of [2]. http://vision.in.tum.de/_media/data/software/cudamultilabeloptimization-v1.0.zip

Implementation help:

 [M1] http://www.mathworks.com/matlabcentral/answers/23104-circular-window
 [M2] http://www.mathworks.com/matlabcentral/answers/101738-how-do-i-specify-the-size-of-the-markers-created-by-the-scatter-plot-in-units-proportional-to-the-da

 [S1] http://stackoverflow.com/questions/37138349/get-matrix-of-minimum-coordinate-distance-to-point-set/37141352#37141352
 