function [ScribImg] = DrawScribbles(ScribImg, I)

[height, width, ~] = size(I);

ScribImgR = zeros(height, width, 1);
ScribImgG = zeros(height, width, 1);
ScribImgB = zeros(height, width, 1);

for i=1:CONST.MAX_REGIONS

    currColor = NumberToColor(i);

    ScribImgR(ScribImg == i) = currColor(1);
    ScribImgG(ScribImg == i) = currColor(2);
    ScribImgB(ScribImg == i) = currColor(3);
    
end

ScribImg = cat(3, ScribImgR, ScribImgG, ScribImgB);

end

