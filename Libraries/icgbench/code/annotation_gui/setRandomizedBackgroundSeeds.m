%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function [seed_img] = setRandomizedBackgroundSeeds(label_img, seed_img)
%function [seed_img] = setRandomizedBackgroundSeeds(label_img, seed_img)
%
% Sets for the background label randomized seed points.
%
%**************************************************************************

%get out the number of seed pixels for every label
stats = regionprops(seed_img + 1, 'Area');
num_pixels = cat(1, stats.Area);

num_new_seeds = round(mean(num_pixels(2:end)));

%get randomized seed candidates
indices = find(label_img == 0);
rand_idx = randperm(length(indices));
indices = indices(rand_idx);
indices = indices(1:num_new_seeds);

%set the background seeds to -1
seed_img(indices) = 0; %set the randomized background seed to 0 again

end