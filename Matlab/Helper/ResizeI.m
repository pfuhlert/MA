function [ I ] = ResizeI( I )
%RESIZEI Summary of this function goes here
%   Detailed explanation goes here
    % Resize Image    
    if((0 < CONST.RESIZE_FACTOR) && (CONST.RESIZE_FACTOR < 1))
        warning(['Using RESIZE_FACTOR = ' num2str(CONST.RESIZE_FACTOR) ' on Image']);
        I = imresize(I, CONST.RESIZE_FACTOR, 'nearest');
    end
    

end

