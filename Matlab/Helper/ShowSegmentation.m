function [FinalImage] = ShowSegmentation( I, theta)
%DrawFinalSegmentation Summary of this function goes here
%   Detailed explanation goes here

    [height, width] = size(theta);
    
    % Get number of regions
    n = uint8(max(max(theta)))+1;
      
    imshow(I .* CONST.MASKIMAGE_MIN_BRIGHT); 
    hold on; 
    
    % Loop is reversed so that only the least made group (normally
    % background) edge is not shown
    FinalImage = [];
    
    for i = n-1:-1:0

        % Get Current Color as mask
        currColorMask = reshape(NumberToColor(n-i), 1,1,3);
        currColorMask = repmat(currColorMask, height, width);

        % Layer of Color adding in group i
        FinalImage = imshow(currColorMask);
        
        % Set alpha channel to x%
        set(FinalImage, 'AlphaData', (theta == i) * CONST.MASKIMAGE_ALPHA );

        % Layer of Edge of Region i
        [gCol, gRow] = GetGradient((theta == i));
        FinalImage = imshow(currColorMask .* repmat(sqrt(gCol.^2 + gRow.^2), 1,1,3));
        set(FinalImage, 'AlphaData', (sqrt(gCol.^2 + gRow.^2) ~= 0) .* 0.5 );
    end

    hold off
    
    
end

