\appendix

\chapter{Edge Evaluation Algorithm}
\label{ch:edge_eval}

The chapter gives further information for the edge based evaluation
algorithm in \cref{subsec:edgebased}. The general idea is to calculate an
energy
\begin{equation}
	E(A, B) = \sum_{\forall x \in A}\; \sum_{\forall y \in N_k(x)} W_{xy} (A(x)
	\oplus A(y))
  \label{eq:edge_energy}
\end{equation}
$$ \text{with } W_{xy} = \left\{
	\begin{array}{ll}
	W_{xy}^+ & \mbox{if } B(x) \neq B(y) \\
	W_{xy}^- & \mbox{else } \\
	\end{array} \right.$$
that finds every transition of a pixel $x$ in an image $A$ to pixels in the 
$k$-neighborhood $N(x)$. It searches for a corresponding transition in the
second image and adds or subtracts energy depending if a transition was found or
not. Additionally, the two weighting functions are defined as
\begin{align*}
	W_{xy}^+ & = \left\{
	\begin{array}{ll}
	1 								 	& \mbox{if } 0 \leq d(x, y) < a \\
	1-\frac{1}{2}(\frac{d-a}{b-a})^2 	& \mbox{if } a \leq d(x, y) < b \\
	\frac{1}{2} (\frac{c-d}{c-b})^2 	& \mbox{if } b \leq d(x, y) < c \\
	0 									& \mbox{if } c \leq d(x, y)\\
	\end{array} \right.\\
	W_{xy}^- & = \left\{
	\begin{array}{ll}
	0 								 	& \mbox{if } 0 \leq d(x, y) < a \\
	-\frac{1}{2}(\frac{d-a}{b-a})^2 	& \mbox{if } a \leq d(x, y) < b \\
	\frac{1}{2} (\frac{c-d}{c-b})^2 - 1	& \mbox{if } b \leq d(x, y) < c \\
	-1 									& \mbox{if } c \leq d(x, y) < c+a\\
	0 									& \mbox{if } c+a \leq d(x, y)\\
	\end{array} \right.
\end{align*}	
for positive and negative weighting that is visualized in \cref{im:wplusminus}.
For each pixel of the segmentation $A$, the algorithm defines a
k-neighborhood $N_k(x)$ around it looking for transitions between two pixels $x$ and $y$ in
image $A$ where binary XOR operation $A(x) \oplus A(y)$
results true. At these locations, the weighting function $W_{xy}$ rewards
the segmentation by obtaining a high energy if there is a corresponding edge in
the second image $B$ or punishes if there is no transition. Furthermore, the
weighting function $W_{xy}$ should not be binary meaning it is supposed to
return energies that are not only for example $1$ for a corresponding edge and
$-1$ for a missing edge but should also consider the distance of the current
pixel leading to the functions $W_{xy}^+$ and $W_{xy}^-$. This results in an
energy per transition in the range of $[-1, 1]$ with the parameters $a, b$ and $c$.

\begin{figure}[htb]\centering
    \setlength\figureheight{0.2\textwidth}
    \setlength\figurewidth{0.9\textwidth}
%###
		\input{figure/weighting/weighting.tex}
%###
	\caption{Weighting functions for edge evaluation}
	\label{im:wplusminus}
\end{figure}

\section*{Normalization}
Since the energy of the previously defined score heavily depends on the total
transitions of a segment, the algorithm has to be normalized to
produce comparable results between different segmentations. For that purpose,
the minimum and maximum energy of each segmentation are used to get a relative
segmentation result. At first, the maximum energy that should correspond to an
evaluation energy of 100\% is the comparison of any image $A$ with itself
resulting in $E_{max}(A) = E(A,A)$. Secondly, the minimum energy is retrieved
when no transition of the first segmentation can be found in the second. Thus,
to get the minimum energy, image $A$ and an empty image $\vec{O}$ are used resulting in
$E_{min}(A) = E(A, \vec{O})$. This energy in general is negative
because only negative weighting is applied to the overall energy function. After
calculating the minimum and maximum energy for an image $A$, the normalization process to get a score in the interval $[0, 1]$ is retrieved
by  
\begin{equation}
	Score(A, B) = \frac{E(A,B) - E_{min}(A)}{E_{max}(A) -
	E_{min}(A)}\,.
\end{equation}
\noindent
This evaluation is non-commutative meaning in general that $Score(A,
B) \neq Score(B, A)$. However, this work obtains a commutative score that can be
interpreted as a F-measure \cite{van2009information} with $\alpha = 0.5$ by
combining both scores to
\begin{equation}
	EdgeScore(A, B) \coloneqq 2 \frac{Score(A, B) \cdot Score(B, A)}{Score(A, B) +
	Score(B, A)}
  \label{eq:equation}
\end{equation}
resulting in a commutative score that lies in the interval $[0, 1]$. This makes
the result comparable to other images and segmentation results.\\