function [scores] = GetSweepScores(trainImagesPath, gtPath, resultsPath, paramName, sweepValues)

% Get List of files
files = dir([trainImagesPath '*.png']);
files = {files.name};

% Get all scores as matrix NOFiles x Sweeps x 2 (Dicescore, edgescore)
scores = zeros(length(files), length(sweepValues), 2);

%% Loop
for i = 1:length(files)

    %% Prerequisites

    fileName = char(files(i));
    % 1. Image
    I = LoadImage(trainImagesPath, fileName);

    % 2. Generate GT-file from imageName
    imageNameSplit = strsplit(fileName, '.');
    gtFileName = strjoin([[strjoin(imageNameSplit(1:end-1), '.') '_gt'] '.' imageNameSplit(end)], '');
    thetaGT = LoadGT(gtPath, gtFileName);

    % 3. Get Scribbles from GT data equally distributed
    randseed(1); % To reproduce results
    [height, width, ~] = size(I);
    ScribbleMap = (double(~thetaGT)+1);
    S = ScribbleMapToIndex(ScribbleMap);
    S = ReduceScribbles(S);

    % Resize for performance increase according to CONST.RESIZE_FACTOR
    I = ResizeI(I);
    thetaGT = ResizeGT(thetaGT);
    S = ResizeS(S);       

    for sweepVarIndex = 1:length(sweepValues)

        % Set new parameter
        eval(['PARAMS.' paramName '(sweepValues(sweepVarIndex));']);

        % Disp curr parameter set
        PARAMS.Get();
        
        %% Start of algorithm
        %disp (['SweepVarNo: ' num2str(sweepVarIndex) ' ' fileName]);
        if(length(unique(S(:,CONST.COL_SGROUP))) < 2)
            continue
        end
        
        theta = SegmentImage(I, S);

        %% Store Data
        fileNameNoExtension = strsplit(fileName, '.');
        fileNameNoExtension = strjoin(fileNameNoExtension(1:end-1), '.');        
        
        % Save Calculated Segmentation
        saveFileName = [resultsPath paramName '_' 'theta_' fileNameNoExtension '_' ...
            sprintf('%03d',sweepVarIndex) '_' num2str(sweepValues(sweepVarIndex)) '.mat'];
        save(saveFileName, 'theta')

        %% Evaluate Data
        
        scores(i, sweepVarIndex, 1) = calculateDiceScore(thetaGT, theta);
        % This takes too long. Not feasable for a whole sweep!
        %scores(i, sweepVarIndex, 2) = EvaluateEdge(thetaGT, theta);

    end
    
end

end