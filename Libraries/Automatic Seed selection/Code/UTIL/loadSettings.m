% Common parameters used in the set-up.


%Training Data Folder.
param.datafolder='U:\Projsem3\Automatic Seed selection\data\';

%Final Solutution Folder.
param.workfolder='U:\Projsem3\Automatic Seed selection\Code\CLASSIFY_FINAL_TEST';
%Test Data Folder.
param.testfolder='U:\Projsem3\Sebastian Bauch\Matlab\data';


test_image_name =  'AIH\dist_corr_deintAIH0350_3.png';
test_image_gt_name =  'AIH\dist_corr_deintAIH0350_3_GroundTruth2.png';
%test_image_name =   'ALV\dist_corr_deintALV0348_3.png';
%test_image_gt_name =   'ALV\dist_corr_deintALV0348_3_GroundTruth2.png';
%test_image_name =   'ASH\dist_corr_deintASH Zi0026_3.png';
%test_image_gt_name =   'ASH\dist_corr_deintASH Zi0026_3_GroundTruth2.png';
%test_image_name =   'HCV\dist_corr_deintHCV097_3.png';
%test_image_gt_name =   'HCV\dist_corr_deintHCV097_3_GroundTruth2.png';
%test_image_name =   'Mittelknotig\ZI382_3.png';
%test_image_gt_name =   'Mittelknotig\dist_corr_deintMittelkn ZI382_3_GroundTruth2.png';
%test_image_name =  'Narben\dist_corr_deintNarben0173_3.png';
%test_image_gt_name =  'Narben\dist_corr_deintNarben0173_3_GroundTruth2.png';


%Used to create probability maps of desired dimension (401*401 in our setup).
param.testImageOverlayMatrix = zeros(401,401);