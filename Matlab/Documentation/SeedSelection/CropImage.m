function [ I ] = CropImage( I )
%CROPIMAGE Summary of this function goes here
%   Detailed explanation goes here

[height, width, ~] = size(I);
cropMinX = width/6;
cropWidth = width - 2 * cropMinX;
cropMinY = height/6;
cropHeight = height - 2 * cropMinY;

I = imcrop(I, ceil([cropMinX cropMinY cropWidth cropHeight]));

end

