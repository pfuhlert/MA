%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function score = calculateDiceScore(a,b)

num_labels = max(length(unique(a)),length(unique(b)));

score = 0;
for i = 0:num_labels - 1
    in_a = (a == i);
    in_b = (b == i);
    in_ab = in_a & in_b;
    
    area_a = sum(in_a(:));  
    area_b = sum(in_b(:));
    area_ab = sum(in_ab(:));
    
    score = score + 2 * area_ab / (area_a + area_b);
end
score = score / num_labels;



