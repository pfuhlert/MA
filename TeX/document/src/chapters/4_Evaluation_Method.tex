\chapter{Evaluation Method}
The main evaluation methods and the image databases are discussed in this
chapter. They were used to obtain the qualitative and quantitative results
that are presented in \cref{ch:results}. 

\section{Databases}

\label{sec:databases}
\subsection{Graz Benchmark}
\label{subsec:icg}
The first database used to obtain qualitative and quantitative segmentation
results combines $244$ images of natural scenes with corresponding scribbles
and groundtruth data that reflect the intended segmentation
result \cite{Santner2010}. ``ICG'' stands for ``Institute for Computer Graphics
and Vision'' of the Graz University of Technology in Austria.
The database was used to get exemplary segmentation results of predefined scribbles and compare them to the provided
groundtruth data. The implemented GUI supports the evaluation of
the previously mentioned image, scribble and groundtruth data combination by
using the included \matlab library. This database was included in the thesis in
order to get comparable results of the adapted algorithm to previous
publications.
\begin{figure}[htbp]\centering
	
	\begin{tabular}{ccc}
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/1_org.png} &
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/1_scrib.png} &
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/1_gt.png}\\
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/2_org.png} &
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/2_scrib.png} &
		\includegraphics[width=.3\textwidth]{image/icgbench_exs/2_gt.png}\\
	
	(a) Original Image & (b) Scribbles & (c) Groundtruth Data\\
	\end{tabular}
	\caption{ICG-Benchmark data containing a natural image, user
	selected scribbles and groundtruth data}
	\label{fig:icgbench}
\end{figure}
\subsection{Laparoscopic Image Sequences}
\label{subsec:laparodb}
For evaluating the algorithm on laparoscopic image sequences, a database
of $115$ images was created from scratch. The images were extracted from
monocular mini-laparoscopic videos from the University Medical Center
Hamburg-Eppendorf. The videos show livers in pathological condition of five
anonymous patients with a total length of approximately \SI{7}{} minutes.
To create the database, every 50th frame of each video was extracted and
analyzed manually if it contains liver pixels. Images that did not 
contain liver pixels at all were discarded.\\

\subsubsection*{Image Properties}
To start with, the images of the database may be blurred on different
degrees. Also, the lightning conditions, liver shape and color as well as the
liver surroundings differ from patient to patient. Furthermore, the quality may
have been reduced by the video codec. This can be observed especially for images of
patient $5$. Also, in the image sequence of this patient, a trocar\footnote{A
trocar is a medical puncture instrument used in minimally-invasive procedures
\cite{romich2013illustrated}} is shown. It also damages the liver causing a
blood flow on the surface of the liver in some images. Sample images for each
patient can be found in \cref{fig:patients}.\\

\noindent
The images moreover include a very large amount of black pixels around the
circular region of interest. This black region was cropped for all images
leading to rectangular images with a lower amount of black pixel surrounding.
Also, the videos differ in resolution leading to different image sizes of the
cropped images depicted in \cref{tab:img_sizes}. The average image contains
$0.55$ megapixels.\\

\begin{figure}[htbp]\centering
	\begin{tabular}[t]{ccc}
		\includegraphics[width=.33\textwidth]{../../../Matlab/Images/UKE/images_crop/pat001_Video005_00151.png}
		&	
		\includegraphics[width=.33\textwidth]{../../../Matlab/Images/UKE/images_crop/pat002_Video002_00151.png}
		&	
		\includegraphics[width=.33\textwidth]{../../../Matlab/Images/UKE/images_crop/pat003_Video001_00351.png}\\
		(a) Patient 1 & (b) Patient 2 & (c) Patient 3\\
	\end{tabular}
	\begin{tabular}[b]{cc}
		\includegraphics[width=.33\textwidth]{../../../Matlab/Images/UKE/images_crop/pat004_Video001_00301.png}
		&	
		\includegraphics[width=.33\textwidth]{../../../Matlab/Images/UKE/images_crop/pat005_Video002_00501.png}\\
		(d) Patient 4 & (e) Patient 5
	\end{tabular}

	\caption{Exemplary images for all investigated patients}
	\label{fig:patients}
\end{figure}

\begin{table}[htbp]

\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Number of Images & Width & Height  \\
		\hline
		39 & 725 & 691 \\
		11 & 761 & 701 \\
		48 & 745 & 711 \\
		17 & 1001 & 720 \\
		\hline
	\end{tabular}
\caption{Resolutions of laparoscopic images}
\label{tab:img_sizes}
\end{table}

\begin{figure}[tbp]\centering
	
	\begin{tabular}[t]{cc}
		\includegraphics[width=.33\textwidth]{image/gt_exs/3.png} &	
		\includegraphics[width=.33\textwidth]{image/gt_exs/2.png} \\	
		\includegraphics[width=.33\textwidth]{image/gt_exs/1.png} &	
		\includegraphics[width=.33\textwidth]{image/gt_exs/4.png} \\
	\end{tabular}

	\caption{Exemplary images with groundtruth segmentation}
	\label{fig:gt_samples}
\end{figure}

\subsubsection*{Groundtruth Data}
\label{subsec:groundtruthdata}
To compare the segmentation results of the algorithm, groundtruth data for the
laparoscopic image sequences was manually generated. The image parts
containing liver were extracted by hand using
Adobe\textsuperscript{\textregistered} Photoshop\textsuperscript{\textregistered} Elements 13 mainly with aid of
the ``Magnetic Lasso'' tool that automatically snaps the user's selection to image
gradients. Using this tool, the livers on all 115 images were extracted and
saved as image files containing a white region where liver pixels are
located and a black region in the background pixels. The groundtruth data will
be used to quantitatively test the segmentation results. Some sample
laparoscopic images with groundtruth data overlay can be found in
\cref{fig:gt_samples}.

\section{Artificial Scribbles}
\label{sec:artificial_scribbles}
To apply the algorithm on an image, preselected scribbles are mandatory for a
successful segmentation result. The selection of scribbles depends on the image
itself as well as the user placing them. To exclude user input from the
evaluation step of quantitative results, this section introduces an automatic
way for scribble creation using the groundtruth data to avoid the user's
influence by manual scribble placement on the quantitative evaluation for
laparoscopic images. The generation process selects a fixed number of pixels of
the original image and classifies their corresponding scribble group using the
groundtruth data. The selected scribbles are randomly distributed and
classified perfectly. This thesis defines those scribbles as ``artificial
scribbles".\\

\section{Metrics}
\label{sec:eval_methods}

For a quantitative evaluation of the segmentation results, two metrics are used
to obtain a segmentation score compared to the groundtruth data. According to
\cite{Unnikrishnan.2005b}, such a measure should have the following requirements:
\begin{enumerate}
  \item \textbf{Non-degeneracy}: A randomly chosen segmentation
  		should not obtain a reasonably high score.
  \item \textbf{No assumptions about data generation}: The segment sizes of the
  evaluation should not influence the score result.
  \item \textbf{Adaptive accommodation of refinement}: The granularity of the
  evaluation should be the same as for the human perception meaning for example
  a high resolution image should not punish a segmentation edge that differs by only a
  few pixels.
  \item \textbf{Comparable scores}: The obtained evaluation score should be
  comparable to other images and segmentations. This means that for instance the
  number of pixels or number of regions should not influence the maximum applicable score.
\end{enumerate}
Keeping the requirements in mind, two different evaluation scores were obtained
which are presented in the following.
\subsection{Region Based}
Basically the used region based evaluation technique compares the groundtruth
data set to a specific segmentation set by their overlap. This region based
evaluation was introduced by Santner et al. in \cite{Santner2010}. The proposed
scoring scheme is called ``dice score'' and can be calculated with two sets A
and B as
\begin{equation}
	DiceScore(A,B) = \frac{2 |A \cap B|}{|A| + |B|}
	\label{eq:diceScore}
\end{equation}
where $|.|$ denotes the total number of pixels of the corresponding set. If $A$
and $B$ represent the same segmentation, the score results in $100\%$ yielding a perfect
match. If the regions do not overlap at all, the resulting score is $0$.\\

\noindent
This algorithm is easy to implement with low complexity and gives a good first
impression of the segmentation result. Nonetheless, the disadvantage of this
evaluation technique is that high scores can be achieved by segmentation
results that do not match the groundtruth data violating requirement $1$.
\subsection{Edge Based}
\label{subsec:edgebased}
The edge based evaluation of this section was published in \cite{Li.2013} and
was used as a second evaluation method in this work. In contrast to the
previously presented approach, this evaluation method is edge based meaning it
compares the transitions between the segments rather than the corresponding
overlapping regions. A detailed description of the evaluation algorithm is given
in \cref{ch:edge_eval}.

\subsubsection*{Parameters}
Since the obtained groundtruth data may not be seen as
completely correct on pixel granularity, some pixels or transitions may differ
from user to user that obtained the groundtruth data. Keeping this in mind, the
region boundaries of the segmentation result may also differ on pixel level as
long as they are consistent on a coarser granularity. Considering this, the
algorithm introduces three parameters $a, b, c$ that introduce a smooth
weighting function depending on the pixel's distance for the transitions.\\

\noindent
The publication \cite{Li.2013} suggests $a = 2$, $b = (a+c)/2$ and $c = 10$
which leads to reasonable results and is therefore not changed in this
implementation. This implies that a transition difference of two pixels remains
a perfect score while a difference of 12 pixels leads to the worst possible
score. The values of $a, b$ and $c$ are absolute. This means that the values
should be adapted to different image resolutions.

\subsubsection*{Drawback}
Lastly, the disadvantage of this evaluation's implementation is
its poor performance. This can be explained due to the fact that
this algorithm works pixel-wise and needs to calculate a pixel's value
multiple times. Also, the total runtime is tripled by getting the minimum and
maximum energy which require additional runs of the whole algorithm. However, by
introducing this evaluation technique, a comparison for the edges of
laparoscopic image segmentations is made possible.

