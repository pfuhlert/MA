function [pred] = classify_on_sift_pca(pca_dimension)
% Train + Classify for SIFT + PCA. 
% Depends on pca_dimension and test_patient_folder.

%Shared variable.
global num_patients;
global test_patient_folder;
global patients_liver;
global patients_bkg;
global numel_liver_images;
global numel_bkg_images;


%Set svm parameter.
option.MaxIter=50000;


% Set parameters for sift.
% Keypoint location (50,50) - Center of ROI of size (100,100).
% Guassian scale 10 - Fits the frame on top of (100,100) ROI.
% Orientation 0.
fc = [50;50;10;0] ;


%Number of Test images in this iteration.
size_of_test_images = numel(patients_liver(test_patient_folder).roi) + numel(patients_bkg(test_patient_folder).roi);


%Create an empty matrix. 
%Col 1: Index. Col 2: Ground truth. Col 3: Prediction.
pred = zeros(size_of_test_images,3); 
pred(:,1) = (1:1:size_of_test_images); %Index.
pred(:,2) = [ones(numel(patients_liver(test_patient_folder).roi),1); zeros(numel(patients_bkg(test_patient_folder).roi),1)]; %Ground truth.




%Load Training Data.
training_data_vector = [];

%Liver.
for pat_no = 1:num_patients %Load all patients for training.
    if pat_no ~= test_patient_folder %Except the patient who is to be tested.
        for i = 1 : (numel(patients_liver(pat_no).roi)) %For every image in the folder.
            %Calculate SIFT feature in this image. Use custom frame.
            [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_liver(pat_no).roi{i})),[0 0 100 100]),'frames',fc) ;
            %Create training_data_vector. Each image is a row of size 128.
            training_data_vector = [training_data_vector ; d'];
        end
    end
end

%Bkg.
for pat_no = 1:num_patients
    if pat_no ~= test_patient_folder
        for i = 1 : (numel(patients_bkg(pat_no).roi))
            [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_bkg(pat_no).roi{i})),[0 0 100 100]),'frames',fc) ;
            training_data_vector = [training_data_vector ; d'];
        end
    end
end



%Convert single back to double for svm.
training_data_vector = double(training_data_vector);

%Generate training Labels.
liver_images = ones(numel_liver_images- numel(patients_liver(test_patient_folder).roi) ,1);
bkg_images = zeros(numel_bkg_images - numel(patients_bkg(test_patient_folder).roi) ,1);
train_class_membership_vector = [liver_images ; bkg_images]; %Ground Truth.



%Load Test Data.
testing_data_vector = [];
for i = 1 : (numel(patients_liver(test_patient_folder).roi))
    [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_liver(test_patient_folder).roi{i})),[0 0 100 100]),'frames',fc) ;
    testing_data_vector = [testing_data_vector ; d'];
end
for i = 1 : (numel(patients_bkg(test_patient_folder).roi))
    [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_bkg(test_patient_folder).roi{i})),[0 0 100 100]),'frames',fc) ;
    testing_data_vector = [testing_data_vector ; d']; 
end

%Convert single back to double.
testing_data_vector = double(testing_data_vector);

%Create Test Ground truth Labels.
test_liver_images = ones(numel(patients_liver(test_patient_folder).roi) ,1);
test_bkg_images = zeros(numel(patients_bkg(test_patient_folder).roi) ,1);
test_class_membership_vector = [test_liver_images ; test_bkg_images]; %Ground Truth.





%Do pca on Training data and reduce the dim to #pca_dimension. Get test
%data shifted to these pca_dimensions.
[W_pca TW_pca] = doPCAwithDimReduction(training_data_vector, testing_data_vector, pca_dimension);





%Train Classifier.
svmModel = svmtrain(W_pca,train_class_membership_vector,'options',option);

%Classify test data based on model. Store result in the 3rd col of pred.
for i = 1:size_of_test_images
    pred(i,3) = svmclassify(svmModel,TW_pca(i,:), 'Showplot',false);
end




%Visualise Data. Check to stop multiple calls.
if test_patient_folder == 1 && pca_dimension == 3
    %Training Data.
    figure;
    gscatter(W_pca(:,1),W_pca(:,2),train_class_membership_vector,[], '*oxs');
    %Test Data.
    figure(1),hold on;
    gscatter(TW_pca(:,1),TW_pca(:,2),test_class_membership_vector,[], '*oxs');
    title('SIFT Feature space');
    xlabel('First Component');
    ylabel('Second Component');
    zlabel('Third Component');
end