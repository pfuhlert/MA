function [theta_compressed] = SegmentImage(Image, ScribbleIdx)
%   I: Image
%   S: Matrix of Scribble Indices : [SGroup SROW SCOL] 
%
%   theta_compressed: Size of Image, containing segmentation result

%% Parameter/Variable Testing

% ScribbleIdx cannot be empty
if(isempty(ScribbleIdx))
    error('No Scribbles found')
end    

% Minimum of 2 regions needed
if(length(unique(ScribbleIdx(:,CONST.COL_SGROUP))) < 2)
    error('At least 2 Regions from Seeds required');
end

% Parameter not initialized
if(isempty(PARAMS.ALPHA) || isempty(PARAMS.SIGMA) || isempty(PARAMS.GAMMA) || isempty(PARAMS.LAMBDA) || isempty(PARAMS.ENERGY_INF))
    error('PARAMS not initialized. Use ''PARAMS.Init'' before calling this function.');
end

%% Load picture

% Is it in correct range?
if(any(any(any((Image < 0) | (Image > 256)))))
    error('Image exceeds supported Range [0 - 255]');
end

[height, width, ~] = size(Image);

if (CONST.DEBUG_OUTPUT)
    disp(['Processing Image with ' num2str(height) 'px * ' num2str(width) 'px']);
end

% If Greyscale, treat as colored picture in the following algorithm
if(length(size(Image)) == 2) 
    Image = repmat(Image, 1, 1, 3);
end

%Show Initial Picture
if(CONST.SHOW_IMG_ORG)
    figure
    imshow(Image);
end

%% Scribbles

% Make sure Scribbles are sorted by region for easier access later on
ScribbleIdx = sortrows(ScribbleIdx);

% Get Image of Scribbles
ScribbleMap = ScribbleIndexToMap(Image, ScribbleIdx);    
    
% Check number of regions
if(length(unique(ScribbleMap))-1 > CONST.MAX_REGIONS)
    error('Exceeded number of supported regions.');
end

% Should the algorithm automatically extract glare points?
if(CONST.EXTRACT_GLAREPOINTS)
    % Extract glare points as additional scribble group?
    Glarepoints = GetGlarepoints(Image);

    % Append Glarepoints to Scribbles
    ScribbleMap(Glarepoints) = max(max(ScribbleMap))+1;
    
    ScribbleIdx = ScribbleMapToIndex(ScribbleMap);
    
end


% Visualize Scribbles
if(CONST.SHOW_IMG_SCRIBBLES)
    
    figure
    
    % Darken Picture to highlight Scribbles
    imshow(Image * CONST.MASKIMAGE_MIN_BRIGHT);
        
    % Show Scribbles on top of original Image
    ScribbleImage = DrawScribbles(ScribbleMap, Image);
    
    % Visualize Result
    ShowScribbleMap(Image, ScribbleImage);
    
end

%% Dataterm
if(CONST.DEBUG_OUTPUT)
    tic
end

Dataterms = GetDataTerms(Image, ScribbleIdx);

if(CONST.DEBUG_OUTPUT)
    elapsedTime = toc;
    disp(['Calculating Dataterm took ' num2str(round(elapsedTime,2)) 's']);
end

if(CONST.SHOW_IMG_DATATERM)
    for i = 1:GetNoSGrps(ScribbleIdx)
        figure
        imshow(mat2gray(Dataterms(:,:,i)));
        title(['-log(P(I(x), x | u(x) = ' num2str(i) ')']);
    end
end

%% Optimization
if(CONST.DEBUG_OUTPUT)
    tic
end

[PrimalEnergy, DualEnergy, theta] = Optimization(Image, Dataterms);

if(CONST.SHOW_ENERGY_PLOT)
    currf = gcf;
    PlotEnergies(PrimalEnergy, DualEnergy);
    figure(currf);
end

if(CONST.DEBUG_OUTPUT)
    elapsedTime = toc;
    disp(['Calculating Optimization took ' num2str(round(elapsedTime,2)) 's']);
end

% Binarize result
theta_bin = Backproject(theta);

%% Compress Theta to 2D matrix

theta_compressed = GetThetaCompressed(theta_bin);

%% Show results
if(CONST.SHOW_OUTPUT_REGIONS)
    figure
    ShowSegmentation(Image, theta_compressed);
end

end