function [ LogRegClassifier, pcaCoeff ] = GetSIFTClassifier( folder )
% From folder with images+gtdata, returns trained classifier

trainImages = dir([folder '*.png']);

%GETCLASSIFIER Outputs 
TrainDescriptors = [];
TrainClass = [];

% Get only real images, avoid GTdata (every 2nd file is just gt)
imageFiles = trainImages(1:2:end);

if(isempty(imageFiles))
    error('No Training data found!');
end

for i = 1:length(imageFiles)
    imageFile = char(imageFiles(i).name);
    
    %% Preprocess Images
    I = imread([folder imageFile]);
    
    % Convention: gt file name is same as image w '_gt'
    folder2 = [folder '../gt/'] ;
    gt = imread([folder2 imageFile(1:end-4) '_gt.png']);
    
    % Crop image borders to minimize wrong classification 
    ICrop = CropImage(I);
    gtCrop = CropImage(gt);

    %% Do SIFT FEATURE EXTRACTION on custom frames
    
    % Let vl_sift choose the feature point locations
    [fc,~] = vl_sift(rgb2gray(im2single(ICrop)));

    % Round to actual image pts
    fc(1, :) = round(fc(1,:));
    fc(2, :) = round(fc(2,:));
    
    % Manually set scale to 10 and orientation to 0
    fc(3,:) = repmat(10, 1, size(fc,2));
    fc(4,:) = zeros(1, size(fc,2));

    % Perform SIFT with new frames, Windowsize according to [5]
    [~,d] = vl_sift(rgb2gray(im2single(ICrop)), 'frames', fc) ;

    % Get classifier of points in f from GT
    idces = sub2ind(size(gtCrop), fc(2,:), fc(1,:));
    Class = gtCrop(idces)';
    
    TrainDescriptors = [TrainDescriptors; d'];
    TrainClass = [TrainClass; Class];
end

%% Do PCA reduction on all training data
[pcaCoeff ,~,~,~,~] = pca(single(TrainDescriptors), 'NumComponents', 2);

% Project data on PCA components
dataCentered = bsxfun(@minus,single(TrainDescriptors), mean(TrainDescriptors,1));
pcaScore = dataCentered * pcaCoeff;

%% Get Logistic Regression Coefficients from Projected Training Data
LogRegClassifier = glmfit(pcaScore, TrainClass, 'binomial', 'link', 'logit');

end

