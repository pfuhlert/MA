function [ ScribbleIdx ] = ScribbleMapToIndex( ScribbleMap )
%SCRIBBLEMAPTOINDEX Summary of this function goes here
%   Detailed explanation goes here

ScribbleIdx = [];

for i=1:max(max(ScribbleMap))
    [SCol, SRow] = find(ScribbleMap == i);
    
    SGroup = repmat(i, length(SCol), 1);
    
    ScribbleIdx = [ScribbleIdx; SGroup, SCol, SRow];
end

end

