function [ S ] = ReduceScribbles( S )
%REDUCESCRIBBLES Reduce number of used Scribbles to maximum

    randomNumbers = rand(sum(GetNoSPts(S)), 1);
    [~,sortIndex] = sort(randomNumbers(:),'descend'); 

    index = zeros(sum(GetNoSPts(S),1), 1);
    
    if((CONST.RESIZE_FACTOR < 1) && (CONST.RESIZE_FACTOR > 0))    
        maxNumber = ceil(CONST.MAX_SCRIBBLE_PTS * CONST.RESIZE_FACTOR);
    else
        maxNumber = CONST.MAX_SCRIBBLE_PTS;
    end
    index(sortIndex(1:maxNumber)) = 1;

    S = S(logical(index), :);  

end

