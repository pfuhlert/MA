%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function [labels, seeds, user_name, image_name] = readFromFile(filename)

% create file
fid = fopen(filename, 'r');
if (fid < 0)
    error('unable to open file');
end

% read header
buffer = fgetl(fid);
if (~strcmp(buffer, 'ICGBENCH GROUNDTRUTH FILE'))
    error('unable to parse file');
end
user_name = fscanf(fid, ['USER:' '%s']);
image_name = fscanf(fid, ['\nIMAGE:' '%s']);
num_labels = fscanf(fid, ['\n#LABELS:' '%d']);
width = fscanf(fid, ['\nWIDTH:' '%d']);
height = fscanf(fid, ['\nHEIGHT:' '%d']);

% read data
fscanf(fid, ['\nLABELS:']);
labels_rle = fscanf(fid, ['%d']);
fscanf(fid, ['\nSEEDS:']);
seeds_rle = fscanf(fid, ['%d']);

% decode data
labels = decodeImageRLE(num2str(labels_rle'), width, height);
seeds = decodeImageRLE(num2str(seeds_rle'), width, height) - 1;

fclose(fid);


