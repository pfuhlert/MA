function [I_vector] = resizeRoiForRGB(I)
% RESIZEROIFORRGB Crop and Shrink I and return a vector of dimesion 7500.
% resizeRoiForRGB(I) crops a 100*100 patch from I and shrinks it to size 50*50.
% The choice of 50*50 is completely arbitrary. It finally returns a vector
% of size 7500.

%Convert to double.
I_d = im2double(I);
%Crop a 100*100 pix patch.
I_crop = imcrop(I_d,[0 0 100 100]);
%Shrink image to reduce computational effort. Size 50*50 is chose arbitrarily. 
I_crop = imresize(I_crop,[50,50]);

%Initialize an empty vector.
I_vector = [];
%For Red, Green and Blue channels.
for color_channels = 1:3
    Image_channel = I_crop(:,:,color_channels);
    I_vector = [I_vector , transpose(Image_channel(:)) ] ;
end
