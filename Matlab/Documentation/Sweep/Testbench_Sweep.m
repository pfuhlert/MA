%% Prerequisites

clear
clc
close all
addpath(genpath('../../')) 
set(0,'DefaultFigureWindowStyle','Docked')

trainImagesPath = '../../Images/UKE/paramtweak/test/';
gtPath = '../../Images/UKE/images_crop/gt/';

% For current run, create a folder w current datetime
datetime = datestr(now, 'yyyy-mm-dd_HH-MM-SS');
resultsPath = ['../../Results/' datetime '/'];
mkdir(resultsPath);

% Also create a diary log
diary([resultsPath 'log.txt']);
diary on

% Print used Parameters on start of log file
disp(PARAMS)
disp(CONST)
overallTimer = tic;

    
%% Set Parameter and Values 
% First Reset to original values
PARAMS.InitOriginal


% Get List of files
files = dir([trainImagesPath '*.png']);
files = {files.name};

% Get all scores as matrix NOFiles x Sweeps x 2 (Dicescore, edgescore)
scores = zeros(length(files), 1, 2);

%% Loop
for i = 1:length(files)

    %% Prerequisites

    fileName = char(files(i));
    % 1. Image
    I = LoadImage(trainImagesPath, fileName);

    % 2. Generate GT-file from imageName
    imageNameSplit = strsplit(fileName, '.');
    gtFileName = strjoin([[strjoin(imageNameSplit(1:end-1), '.') '_gt'] '.' imageNameSplit(end)], '');
    thetaGT = LoadGT(gtPath, gtFileName);

    % 3. Get Scribbles from GT data equally distributed
    randseed(1); % To reproduce results
    [height, width, ~] = size(I);
    ScribbleMap = (double(~thetaGT)+1);
    S = ScribbleMapToIndex(ScribbleMap);
    S = ReduceScribbles(S);

    % Resize for performance increase according to CONST.RESIZE_FACTOR
    I = ResizeI(I);
    thetaGT = ResizeGT(thetaGT);
    S = ResizeS(S);       

    % Set new parameter
    PARAMS.Init
    PARAMS.Get();

    %% Start of algorithm
    if(length(unique(S(:,CONST.COL_SGROUP))) < 2)
        warning('At least 2 Regions from Seeds required');
        continue;
    end
    
    theta = SegmentImage(I, S);

    %% Store Data
    fileNameNoExtension = strsplit(fileName, '.');
    fileNameNoExtension = strjoin(fileNameNoExtension(1:end-1), '.');        

    % Save Calculated Segmentation
    saveFileName = [resultsPath 'theta_' fileNameNoExtension '_' ...
        '.mat'];
    save(saveFileName, 'theta')

    %% Evaluate Data
    scores(i, 1, 1) = calculateDiceScore(thetaGT, theta);
    % This takes too long. Not feasable for a whole sweep!
    if((sum(sum(theta))) == 0 || (sum(sum(thetaGT)) == 0))
        warning('Evaluation cannot be computed on empty mask')
        continue;
    end
    scores(i, 1, 2) = EvaluateEdge(thetaGT, theta);
end

%% Saving, Plotting 
save([resultsPath 'scores'], 'scores');

handler = PlotScores(1:length(files), scores, 'Original Parameter results');

saveas(handler,[resultsPath 'score'],'png');

toc(overallTimer)
disp('done.');
diary off

% When done, shut down in 10 minutes
%system('shutdown -s -t 600')
