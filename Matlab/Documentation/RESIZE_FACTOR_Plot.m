
%PLOTSCORES Summary of this function goes here
%   Detailed explanation goes here


paramVals = [1 1/2 1/4 1/8 1/16 1/32 1/64];
DiceScores = [97.2 93.2 91.8 89.5 84.6 77.9 64.2];
EdgeScores = [66.1 58.3 57.8 45.7 38 36.1 34 ];

%computationTime = mean(reshape(time_split(:,1) + time_split(:,2), 13, 29), 2);
 
handler = figure;
[hAx hLine1 hLine2] = plotyy(paramVals, DiceScores, paramVals, EdgeScores, 'semilogx');

hLine1.Marker = 'o';
hLine2.Marker = 'o';
%ylabel(hAx(1),'Edge Score'); % left y-axis
%ylabel(hAx(2),'Dice Score'); % right y-axis
set(gca,'XTickLabel',get(gca,'xtick'));

legend('Dice Score', 'Edge Score');

grid on
xlabel('RESIZE_FACTOR');


%To export:
% matlab2tikz([CONST.TEXFIGUREPATH 'name.tikz'], 'height', '\figureheight', 'width', '\figurewidth');

