%% Prerequisites

clear
clc
close all
addpath(genpath('../../')) 
set(0,'DefaultFigureWindowStyle','Docked')

trainImagesPath = '../../Images/UKE/paramtweak/test/';
gtPath = '../../Images/UKE/images_crop/gt/';

% For current run, create a folder w current datetime
datetime = datestr(now, 'yyyy-mm-dd_HH-MM-SS');
resultsPath = ['../../Results/' datetime '/'];
mkdir(resultsPath);

% Also create a diary log
diary([resultsPath 'log.txt']);
diary on

% Print used Parameters on start of log file
disp(PARAMS)
disp(CONST)
overallTimer = tic;

    
%% Set Parameter and Values 
% First Reset to original values
PARAMS.InitOriginal


% Get List of files
files = dir([trainImagesPath '*.png']);
files = {files.name};

% Get all scores as matrix NOFiles x Sweeps x 2 (Dicescore, edgescore)
scores = zeros(length(files), 2, 2);

%% Loop
for i = 1:length(files)

    disp(num2str(i));
    
    %% Prerequisites

    fileName = char(files(i));
    % 1. Image
    I = LoadImage(trainImagesPath, fileName);

    % 2. Generate GT-file from imageName
    imageNameSplit = strsplit(fileName, '.');
    gtFileName = strjoin([[strjoin(imageNameSplit(1:end-1), '.') '_gt'] '.' imageNameSplit(end)], '');
    thetaGT = LoadGT(gtPath, gtFileName);

    % 3. Get Scribbles from GT data equally distributed
    randseed(1); % To reproduce results
    [height, width, ~] = size(I);
    ScribbleMap = (double(~thetaGT)+1);
    S = ScribbleMapToIndex(ScribbleMap);
    S = ReduceScribbles(S);

    % Resize for performance increase according to CONST.RESIZE_FACTOR
    I = ResizeI(I);
    
    S = ResizeS(S);       



    %% Start of algorithm
    if(length(unique(S(:,CONST.COL_SGROUP))) < 2)
        warning('At least 2 Regions from Seeds required');
        continue;
    end
    
    % Set new parameter
    PARAMS.InitOriginal
    theta = SegmentImage(I, S);

    %% Store Data
    fileNameNoExtension = strsplit(fileName, '.');
    fileNameNoExtension = strjoin(fileNameNoExtension(1:end-1), '.');        

    % Save Calculated Segmentation
    saveFileName = [resultsPath 'theta_' fileNameNoExtension '_1' ...
        '.mat'];
    save(saveFileName, 'theta')    
    
    %% Evaluate Data
    theta = imresize(theta, size(thetaGT), 'nearest');
    scores(i, 1, 1) = calculateDiceScore(thetaGT, theta);
    % This takes too long. Not feasable for a whole sweep!
    if((sum(sum(theta))) == 0 || (sum(sum(thetaGT)) == 0))
        warning('Evaluation cannot be computed on empty mask')
        continue;
    end
    %scores(i, 1, 2) = EvaluateEdge(thetaGT, theta);
    
    PARAMS.Init
    theta = SegmentImage(I, S);

    %% Store Data
    fileNameNoExtension = strsplit(fileName, '.');
    fileNameNoExtension = strjoin(fileNameNoExtension(1:end-1), '.');        

    % Save Calculated Segmentation
    saveFileName = [resultsPath 'theta_' fileNameNoExtension '_2' ...
        '.mat'];
    save(saveFileName, 'theta')       
    
    %% Evaluate Data
    theta = imresize(theta, size(thetaGT), 'nearest');
    scores(i, 2, 1) = calculateDiceScore(thetaGT, theta);
    % This takes too long. Not feasable for a whole sweep!
    if((sum(sum(theta))) == 0 || (sum(sum(thetaGT)) == 0))
        warning('Evaluation cannot be computed on empty mask')
        continue;
    end
    %scores(i, 1, 2) = EvaluateEdge(thetaGT, theta);
    
end

%% Saving, Plotting 
save([resultsPath 'scores'], 'scores');

%handler = PlotScores(1:length(files), scores, 'Original Parameter results');

%saveas(handler,[resultsPath 'score'],'png');

toc(overallTimer)
disp('done.');
diary off


% Edge and dice score for 86 images

plot(1:86, [scores(:,1,1) scores(:,1,1)], '-o');
xlabel('Image Number');
ylabel('Dice Score');
legend('Original Parameters', 'Adapted Parameters', 'Location','southeast');

figure
plot(1:86, [scores(:,1,2) scores(:,2,2)], '-o');
xlabel('Image Number');
ylabel('Edge Score');
legend('Original Parameters', 'Adapted Parameters', 'Location','northeast');

[median ] index median box_top box_bottom whisker_top whisker_bottom

[1 median(scores(:,1,1)) quantile(scores(:,1,1),0.75) quantile(scores(:,1,1),0.25) quantile(scores(:,1,1),0.975)  quantile(scores(:,1,1),0.025)]
% Boxplot
disp(['Mean Dice Score1: ' num2str(mean(scores(:,1,1)*100)) '%']);
disp(['Mean Edge Score1: ' num2str(mean(scores(:,1,2)*100)) '%']);
disp(['Mean Dice Score2: ' num2str(mean(scores(:,2,1)*100)) '%']);
disp(['Mean Edge Score2: ' num2str(mean(scores(:,2,2)*100)) '%']);
