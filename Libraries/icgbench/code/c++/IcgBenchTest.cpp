/*
* Copyright (c) ICG. All rights reserved.
*
* Institute for Computer Graphics and Vision
* Graz University of Technology / Austria
*
* This software is distributed WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE.  See the above copyright notices for more information.
*
* Author     : Jakob Santner
* EMail      : santner@icg.tugraz.at
*/

#include "IcgBench.h"

#include <iostream>
#include <string>

int main(int argc, char *argv[]) {
  try {
    std::string base_path = "D:/projects/icgbench/code/c++/";
    std::string filename_a = "a.gt";
    std::string filename_b = "b.gt";

    std::cout << "--- IcgBenchFileIO Unit Test ---" << std::endl;

    std::cout << "- trying to construct with file " << filename_a << std::endl;    
    IcgBench::IcgBenchFileIO my_io_a(base_path + filename_a);

    std::cout << "- trying to construct with file " << filename_b << std::endl;    
    IcgBench::IcgBenchFileIO my_io_b(base_path + filename_b);

    std::cout << "- testing filename " << std::endl; 
    if (strcmp(my_io_a.getFileName().c_str(), "image_0084.jpg"))
      throw(std::runtime_error("file i/o failed"));

    std::cout << "- testing username " << std::endl; 
    if (strcmp(my_io_a.getUserName().c_str(), "9000"))
      throw(std::runtime_error("file i/o failed"));

    std::cout << "- testing dimensions " << std::endl; 
    IcgBench::LabelImage* my_img = my_io_a.getLabels();
    if (my_img->width() != 625)
      throw(std::runtime_error("file i/o failed"));
    if (my_img->height() != 391)
      throw(std::runtime_error("file i/o failed"));

    std::cout << "- testing a few values " << std::endl; 
    if (my_img->get(301, 128) != 2)
      throw(std::runtime_error("file i/o failed"));
    if (my_img->get(302, 128) != 0)
      throw(std::runtime_error("file i/o failed"));
    if (my_img->get(399, 231) != 1)
      throw(std::runtime_error("file i/o failed"));
    if (my_img->get(400, 233) != 2)
      throw(std::runtime_error("file i/o failed"));
    if (my_img->get(624, 390) != 1)
      throw(std::runtime_error("file i/o failed"));
    delete my_img;

    std::cout << "- testing dice score " << std::endl;
    IcgBench::LabelImage* a = my_io_a.getLabels();
    IcgBench::LabelImage* b = my_io_b.getLabels();
    double score = IcgBench::computeDiceScore(*a, *b);
    if (score != 1.0)
      throw(std::runtime_error("computation of score failed"));
    delete a;
    delete b;


    std::cout << "- all tests passed successfully" << std::endl;
  }
  catch(...) {
    std::cout << " failed!" << std::endl;
  }
  return 0;
}