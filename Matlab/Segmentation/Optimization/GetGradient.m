function [ GradX, GradY ] = GetGradient( I )
%GETGRADIENT Calcs Gradient according to [1]
% in section 4.5.1
dim = size(I, 3);

if (dim ~= 1)
    error('Wrong Use of GetGradient(I) in Dimensions of I');
end

[GradX, GradY] = imgradientxy(I, 'IntermediateDifference');

end

