function [ Kernels ] = GetKernels( I, S )
%GETKERNELS Summary of this function goes here
% Get Spatial Probability Estimator as in (Equation 14 of [2])

[height, width, ~] = size(I);
nSGroups = GetNoSGrps(S);
nSPts = GetNoSPts(S);

% Preallocate overall estimator 
Kernels = zeros(height, width, GetNoSGrps(S));

% Get Slicing width and height
regionWidth = ceil(width / CONST.SLICE_N);
regionHeight = ceil(height / CONST.SLICE_N);

% Colors of scribbles
% impixel(I,c,r) takes columns then rows!
% Get Scribble Colors
SColors = impixel(I,S(:,CONST.COL_SCOL), S(:,CONST.COL_SROW));

% Reshape to match image (Color in dim3)
SColors = single(reshape(SColors, [size(SColors,1) 1 3]));

% Do slicing
for iX = 1:CONST.SLICE_N
    for iY = 1:CONST.SLICE_N
        
        xMin = regionWidth * (iX - 1) + 1;
        xMax = regionWidth * (iX);
        if(xMax > width)
            xMax = width;
        end
        
        yMin = regionHeight * (iY - 1) + 1;
        yMax = regionHeight * (iY);
        if(yMax > height)
            yMax = height;
        end
        
        %% ALGORITHM
        KernelRegion = zeros(yMax - yMin + 1, xMax - xMin + 1, nSGroups);
        I_Single = double(I(yMin:yMax, xMin:xMax, :));
        
        %% Get coordinate matrix of every pixel in the (sliced) image.
        [Xcoords, Ycoords] = meshgrid(xMin:xMax, yMin:yMax); 
        
        for i = 1:nSGroups;

            currIndices = 1:nSPts(i);
            if(i > 1)
                currIndices = currIndices + sum(nSPts(1:i-1));
            end
            
            %% Spatial 
            
            % Get points and reshape them to a vector along the 3rd dim.
            Xpts=single(reshape(S(currIndices,CONST.COL_SCOL),1,1,[]));
            Ypts=single(reshape(S(currIndices,CONST.COL_SROW),1,1,[]));

            % diffs between coordinates and scribbles
            Xdiffs=bsxfun(@minus,(Xcoords),Xpts);
            Ydiffs=bsxfun(@minus,(Ycoords),Ypts);

            % Calculate all the distances (hypthenuse = sqrt(a^2 + b^2))
            SpatDist= bsxfun(@hypot,Xdiffs,Ydiffs);
            
            % Get the minimum distances for each pixel
            D_min = min(SpatDist, [], 3) ;
            
            % Rho has to be the same size as D_min, so expand
            % in third dimension to match SpatDist
            Roh_expanded = repmat(PARAMS.ALPHA .* D_min, 1,1,size(SpatDist, 3));

            % Apply Gaussian Kernel function with zero mean and calculated
            % standard deviation.
            SpatKernels = normpdf(SpatDist, 0, Roh_expanded);
            
            %% COLOR
            ColorKernels = single(zeros(yMax - yMin + 1, xMax - xMin + 1, length(currIndices)));
            for j = currIndices

                    ColorDist = I_Single - repmat(SColors(j,1,:),yMax - yMin + 1,xMax - xMin + 1);
                    ColorDist = sqrt(sum(ColorDist.^2, 3));

                    % Use builtin normpdf:        
                    % mu = 0. if Colordistance = 0 -> max Value
                    ColorKernels(:,:,j - currIndices(1) + 1) = single(normpdf(ColorDist, 0, PARAMS.SIGMA));

            end
                       
            %% Combine

            % Sum over all Kernels to get current Estimation, then
            % normalize
            KernelRegion(:,:,i) = sum(SpatKernels .* ColorKernels, 3) ./ nSPts(i);
        end
        
        % Put sliced image parts together
        Kernels(yMin:yMax,xMin:xMax, :) = KernelRegion;
        
    end
end

if(CONST.DEBUG_OUTPUT)
    disp('Finished Kernels');
end

end

