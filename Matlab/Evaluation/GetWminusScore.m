function [score] = GetWminusScore(WminusDists)

    if(isempty(WminusDists))
        score = 0;
        return;
    end       

    WminusScores = zeros(size(WminusDists));

    % Is already zero. No need to do.
    %index1 = (0 <= WminusDists) & (WminusDists < PARAMS.EVAL_A);
    %WminusScores(index1) = 0;

    index2 = (PARAMS.EVAL_A <= WminusDists) & (WminusDists < PARAMS.EVAL_B);
    WminusScores(index2) = -0.5 * ((WminusDists(index2) - PARAMS.EVAL_A) / ...
                                    (PARAMS.EVAL_B - PARAMS.EVAL_A)).^2;

    index3 = (PARAMS.EVAL_B <= WminusDists) & (WminusDists < PARAMS.EVAL_C);
    WminusScores(index3) = 0.5 * ((PARAMS.EVAL_C - WminusDists(index3)) / ...
                                   (PARAMS.EVAL_C - PARAMS.EVAL_B)).^2 - 1;

    index4 = (PARAMS.EVAL_C <= WminusDists) & (WminusDists < (PARAMS.EVAL_C + PARAMS.EVAL_A));
    WminusScores(index4) = -1;

    score = sum(WminusScores);

end