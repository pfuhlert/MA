function [ PrimalEnergy, DualEnergy, theta_n] = Optimization( I, f )
%CALCOPTIMUM Summary of this function goes here
%   Detailed explanation goes here
% Number of ScribbleGroups

[height, width, n] = size(f);

%% Initialization 

% ... of xi, Vector field on every point of image for each segment group.
xi_n = zeros(height, width, n, 2);
xi_np1 = xi_n;

% ... of theta

theta_n = zeros(height, width, n);
theta_np1 = zeros(height, width, n);
theta_bar = zeros(height, width, n);

% ... of Energy
PrimalEnergy = zeros(CONST.OPTI_MAX_STEPS,1);
DualEnergy = zeros(CONST.OPTI_MAX_STEPS,1);
energyDiff = zeros(CONST.OPTI_MAX_STEPS,1);

% ... of Image Edge favor

G = GetG(I);
R = G ./ 2;

%% Iteration

% Used to break from loop
exitLoop = false;

for steps = 1:CONST.OPTI_MAX_STEPS
     
    % Primal/Dual Variables
    xi_np1      = GetNewDual(xi_n, theta_bar, R);
    theta_np1   = GetNewPrimal(theta_n, xi_np1, f);
    
    % Overrelaxation step
    theta_bar = 2 * theta_np1 - theta_n;
    
    % Energy        
    PrimalEnergy(steps) = GetEnergyPrimal(G, f, Backproject(theta_np1));
    DualEnergy(steps) = GetEnergyDual(f, xi_np1);
    energyDiff(steps) = PrimalEnergy(steps) - DualEnergy(steps);

    % Show progress every CONST.OPTI_OUTPUT_EVERY_N steps
    if(CONST.DEBUG_OUTPUT && ~mod(steps, CONST.OPTI_OUTPUT_EVERY_N))
        disp(['Step ' num2str(steps) ': - Primal Energy: ' num2str(PrimalEnergy(steps)) ...
              ', Dual Energy: ' num2str(DualEnergy(steps))]);
    end
    
    
% ### Iteration ending criteria ###
    
    % Reached maximum number of iteration steps?
    if(steps == CONST.OPTI_MAX_STEPS)
        if(CONST.DEBUG_OUTPUT)
            disp('Maximum optimization steps reached.')
        end
        exitLoop = true;
        
        
    elseif(((steps>2) && ((energyDiff(steps) < eps) || (energyDiff(steps-1) * (1-PARAMS.OPTI_PRIMDUAL_MIN_DIFF) < energyDiff(steps)))))
        if(CONST.DEBUG_OUTPUT)
            disp(['Primal-Dual-Gap changed by less than ' num2str(PARAMS.OPTI_PRIMDUAL_MIN_DIFF*100) '%']);
        end
        exitLoop = true;        
    end
      
    % n -> n+1
    xi_n = xi_np1;
    theta_n = theta_np1;   
    
    %% Show results
    
    % Live Visualization For Region 1
    if(CONST.SHOW_ANIMATE_ITERATION)
        theta_bin = Backproject(theta_n);    
        ShowSegmentation(I, theta_bin(:,:,1));
               
        drawnow
        
        if(CONST.SAVE_ANIMATE_ITERATION)
            F = getframe();
            Image = frame2im(F);
            if ~exist('../Iteration', 'dir')
                mkdir('../Iteration');
            end
            imwrite(Image, ['../Iteration/Image-' num2str(steps) '.png'])
        end
        
    end
       
    if(exitLoop)
        
        if(CONST.DEBUG_OUTPUT)
            disp(['Terminating Loop at step ' num2str(steps)]);
        end
        
        % Cut primal and dual energy length on current step
        % Also exclude step1 (xi == 0 not representative!)
        PrimalEnergy = PrimalEnergy(2:steps);
        DualEnergy = DualEnergy(2:steps);
        break;
    end    
end

end

