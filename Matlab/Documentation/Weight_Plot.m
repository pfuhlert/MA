
addpath(genpath('../'))

figure
hold on

line([2 2],[-1 1], 'Color', 'k', 'LineStyle', '--');
text(2,-1.2,'a','HorizontalAlignment','center');
line([6 6],[-1 1], 'Color', 'k', 'LineStyle', '--');
text(6,-1.2,'b','HorizontalAlignment','center');
line([12 12],[-1 1], 'Color', 'k', 'LineStyle', '--');
text(12,-1.2,'c','HorizontalAlignment','center');


fplot(@(x) GetWplusScore(x),[0 14], '-')
fplot(@(x) GetWminusScore(x),[0 14], '-.')

%title('Weighting Functions for Edge Evaluation');
legend('W_{xy}^+', 'W_{xy}^-');
xlabel('d(x, y)');
ylabel('Energy');