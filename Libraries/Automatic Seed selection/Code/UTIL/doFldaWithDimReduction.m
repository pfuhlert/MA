function [W_lda, TW_lda] = doFldaWithDimReduction(training_data_vector, train_class_membership_vector,testing_data_vector, flda_dimension)
% DOFLDAWITHDIMREDUCTION Do FLDA and return the first #flda_dimension's.
% doFldaWithDimReduction() takes Training-Data, Testing-Data and the
% desired number of dimensions. It performs mean centering according to
% the Training-Data; It also finds the first best #flda_dimensions and then
% projects Training and testing data onto these dimensions.
% W_lda is the Projected Training Data.
% TW_lda is the Projected Test Data.

%Compress data to 20 dimensions to reduce computational complexity during FLDA.
%[training_data_vector, testing_data_vector] = doPCAwithDimReduction(training_data_vector, testing_data_vector, 20);

%Mean centering.
train_image_vector = bsxfun(@minus,training_data_vector, mean(training_data_vector,1));

%total mean
mu = mean(train_image_vector, 1);

%within-class scatter matrix
Sw = zeros(size(train_image_vector,2),size(train_image_vector,2));
%between-class scatter matrix
Sb = zeros(size(train_image_vector,2),size(train_image_vector,2));

%Do for each class.
for i = [1 0]
    %mean for class
    mu_i = mean(train_image_vector((train_class_membership_vector==i),:),1);
    %within-class scatter matrix
    X = bsxfun(@minus,train_image_vector((train_class_membership_vector==i),:), mu_i);
    Sw = Sw + X'*X;
    %between-class scatter matrix
    Sb = Sb + (mu_i - mu)'*(mu_i - mu);
end



%Fisher's linear discriminant analysis

[B_lda, lambda_lda] = eig(Sb, Sw, 'qz');
%sort eigenvectors in descending order
[lambda_lda, ind] = sort(diag(lambda_lda), 'descend');
lambda_lda = lambda_lda(1:flda_dimension);
%Chose the first #flda_dimension's.
B_lda = B_lda(:,ind(1:flda_dimension));


%Project training data. 
W_lda = train_image_vector*B_lda;

%Mean centering w.r.t Training data.
test_image_vector = bsxfun(@minus,testing_data_vector, mean(training_data_vector,1));
%Project Testing data.
TW_lda = test_image_vector*B_lda;
