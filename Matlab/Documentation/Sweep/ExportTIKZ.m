function [  ] = ExportTIKZ(paramVals, scores, paramName, tikzFileName )
%EXPORTTIKZ Summary of this function goes here
%   Detailed explanation goes here

PlotScores(paramVals, scores, paramName); 
matlab2tikz([CONST.TEXFIGUREPATH tikzFileName], 'height', '\figureheight', 'width', '\figurewidth', 'showInfo', false);

end

