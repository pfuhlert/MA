% Train a SVM classifier on GLCM descriptors and produce precision and
% recall values for cross validation data settaken from 7 distinct
% pateints.

%%%%%%%%%%%%%%%% Clear and Do Setup. %%%%%%%%%%%%%%%%%%%%%
clear all;
close all;
clc;


%IO files.
addpath('..\FILE_IO\')
%Util files.
addpath('..\UTIL\')

%Initialize.
loadSettings

%Init VLFEATROOT toolbox.
run('..\..\VLFEATROOT\toolbox\vl_setup')



%%%%%%%%%%%%%%%% Initialize global variables. %%%%%%%%%%%%%%%%%%%%%
global patients_liver;
global patients_bkg;
global numel_liver_images;
global numel_bkg_images;
global test_patient_folder;
global num_patients;



%%%%%%%%%%%%%%%%%%%%%% Read Data. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Read Liver images.                    Class 1 ==> Liver.
[numel_liver_images, patients_liver] = importFibroData(1,param.datafolder);
%Read background images.               Class 0 ==> Background.
[numel_bkg_images, patients_bkg]     = importFibroData(0,param.datafolder);

% Get number of Patients.
num_patients = numel(dir([param.datafolder '.\pat*']));


%%%%%%%%%%%%%%%%%%%%%% Train and Classify %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Init prediction_set.
prediction_set = [];

%For each patient in the training data set, do:
for pat_no = 1:(num_patients)

    %Test on this patient while using everyone else for training.
    %MANTRA: Leave one patient out cross validation.
    test_patient_folder = pat_no;
    
    %Create labels for Training.
    liver_images = ones(numel_liver_images- numel(patients_liver(test_patient_folder).roi) ,1);
    bkg_images   = zeros(numel_bkg_images - numel(patients_bkg(test_patient_folder).roi) ,1);
    class_membership_vector = [liver_images ; bkg_images]; % 1 = Liver. 0 = Background.

    %Classify using GLCM.
    pred = classifyOnGlcm();
      
    % Get the prediction and corresponding ground truth.
    prediction_set = [prediction_set ; pred(:,2:3)];
end




%%%%%%%%%%%%%%%%%%%%%% Calculate Precision & Recall. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
glcm_tp_liver = sum(prediction_set((prediction_set(:,2)==1),1) == 1);
glcm_fp_liver = sum(prediction_set((prediction_set(:,2)==1),1) == 0);
glcm_fn_liver = sum(prediction_set((prediction_set(:,2)==0),1) == 1);

%Precision.
GLCM_PERCISION_LIVER = glcm_tp_liver/(glcm_tp_liver+glcm_fp_liver)
%Recall.
GLCM_RECALL_LIVER    = glcm_tp_liver/(glcm_tp_liver+glcm_fn_liver)



glcm_tp_bkg = sum(prediction_set((prediction_set(:,2)==0),1) == 0);
glcm_fp_bkg = sum(prediction_set((prediction_set(:,2)==0),1) == 1);
glcm_fn_bkg = sum(prediction_set((prediction_set(:,2)==1),1) == 0);

%Precision.
GLCM_PERCISION_BKG = glcm_tp_bkg/(glcm_tp_bkg+glcm_fp_bkg)
%Recall.
GLCM_RECALL_BKG    = glcm_tp_bkg/(glcm_tp_bkg+glcm_fn_bkg)
