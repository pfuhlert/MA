function [ F_measure ] = EvaluateEdge( testMask, gtMask)
%% This function is implemented as in [4] with horrible performance

    if((sum(sum(testMask))) == 0 || (sum(sum(gtMask)) == 0))
        error('Evaluation cannot be computed on empty mask')
    end

    if(length(unique(testMask)) ~= length(unique(gtMask)))
        error('Number of regions mismatch.')
    end

    % Get maximum values by measure with itself
    Pmax = GetEdgeMetric(testMask, testMask);
    Rmax = GetEdgeMetric(gtMask, gtMask);
    
    % Get minimum values by measure with mask without edges
    Pmin = GetEdgeMetric(testMask, zeros(size(testMask)));
    Rmin = GetEdgeMetric(gtMask, zeros(size(gtMask)));

    % If 1st = test and 2nd = GT -> Precision
    % If 1st = GT and 2nd = test -> Recall
    scorePrecision = GetEdgeMetric(testMask,gtMask);
    scoreRecall    = GetEdgeMetric(gtMask,testMask);

    % Norm to [0, 1]
    Precision = NormMinMax(scorePrecision, Pmin, Pmax);
    Recall    = NormMinMax(scoreRecall, Rmin, Rmax);

    % Combine using F-Measure
    F_measure = (Precision * Recall) / ((1-PARAMS.F_MEASURE_ALPHA) * Recall + PARAMS.F_MEASURE_ALPHA * Precision);

end

% To transform from [a,b] to interval [0, 1] this formula helps:
% x = (x - a) / (b-a); 
function [normed] = NormMinMax(x, min, max)

    if((min > x) || (max < x))
        error('Error: X is not between min and max.');
    end
    
    if(max == min)
        error('Error: max cannot equal min.');
    end

    normed = (x - min) / (max - min);

end
   
    
