This folder contains everything that is submitted along with the master thesis on
"Variation Based Segmentation of Liver Surface from Monocular Mini-Laparoscopic Sequences"
by Patrick Fuhlert.

The content is split in the LaTeX and Matlab code.
Take a look at the individual readmes for further instructions.