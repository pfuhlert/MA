figure
hold on
for gam = [1 5 10 20]
    fplot(@(x) exp(-gam * abs(x)), [-1 1])
end
legend('\gamma = 1', '\gamma = 5', '\gamma = 10', '\gamma = 20')
xlabel('|| \nabla I ||');
ylabel('g(x)');
hold off