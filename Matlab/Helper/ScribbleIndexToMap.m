function [ ScribbleMap ] = ScribbleIndexToMap( I, S )
%SCRIBBLEINDEXTOMAP Summary of this function goes here
%   Detailed explanation goes here

[height, width, ~] = size(I);

ScribbleMap = zeros(height, width);

for i = 1:size(S,1)
    ScribbleMap(S(i,CONST.COL_SROW), S(i,CONST.COL_SCOL)) = S(i,CONST.COL_SGROUP);
end

end
