% 

%%%%%%%%%%%%%%%% Clear and Do Setup. %%%%%%%%%%%%%%%%%%%%%

%Cleanup.
clear all;
close all;
clc;

%IO files.
addpath('..\FILE_IO\')
addpath('..\UTIL\')


%Initialize.
loadSettings

%Init VLFEATROOT toolbox.
run('..\..\VLFEATROOT\toolbox\vl_setup')

%Profile.
profile on



%test_image_name =  'AIH\dist_corr_deintAIH0350_3.png';
%test_image_name =  'ALV\dist_corr_deintALV0348_3.png';
%test_image_name =  'ASH\dist_corr_deintASH Zi0026_3.png';
%test_image_name =  'HCV\dist_corr_deintHCV097_3.png';
%test_image_name =   'Mittelknotig\dist_corr_deintMittelkn ZI376_3.png';
%test_image_name =  'Narben\dist_corr_deintNarben0173_3.png';
%%%%%%%%%%%%%%%%%%%%%% Read Data. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Training Data.

%Read Liver images.
[numel_liver_images patients_liver] = importFibroData(1,param.datafolder);
%Read background images.
[numel_bkg_images patients_bkg]     = importFibroData(0,param.datafolder);

% Get number of training Patients.
num_patients = numel(dir([param.datafolder '.\pat*']));



%%%%%%%%%%%%%%%%%%%%%% SIFT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Classify based on SIFT vectors.


%Set parameters for sift
fc = [50;50;10;0] ;

%Set pca parameter.
pca_dimension = 2;


%Load Training Data.

    disp('Creating training data vector for SIFT. ');

    %Liver.
    
    %Counter.
    cc = 0;
    
    %Size hardcoded. 718 images and 128 SIFT dimensions. Matrix size 718 * 128. 
    %Each row represents 1 data point.
    training_data_vector = zeros(718,128);
    
    for pat_no = 1:num_patients % Do for each patient.    
        for i = 1 : (numel(patients_liver(pat_no).roi)) % Each image for every patient.
            cc = cc + 1;
            %This patient: pat_no. This image number: pat_no.i . Convert to
            %grey, single and finally crop to a standard size of 100*100
            %pixels! 
            %Size 100*100 is an arbitrary design choice.
            [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_liver(pat_no).roi{i})),[0 0 100 100]),'frames',fc) ;
            %d is a 128 dimensional vector.
            training_data_vector(cc,:) = d';
        end
    end
    %Bkg.
    for pat_no = 1:num_patients
        for i = 1 : (numel(patients_bkg(pat_no).roi))
            cc = cc + 1;
            [f,d] = vl_sift(imcrop(im2single(rgb2gray(patients_bkg(pat_no).roi{i})),[0 0 100 100]),'frames',fc) ;
            training_data_vector(cc,:) = d';
        end
    end
    
    %Convert single SIFT vectors to double.
    training_data_vector = double(training_data_vector);

    %Generate training Labels.
    liver_images = ones(numel_liver_images, 1);
    bkg_images = zeros(numel_bkg_images, 1);
    train_class_membership_vector = [liver_images ; bkg_images]; %Ground Truth.


    
%Load Testing data.

    disp('Creating testing data vector for SIFT. ');
   
    %Test data dir.
    cd(param.testfolder)
    im_gt =  rgb2gray(   im2double(imread(test_image_gt_name)));
    
    %Read image.
    %im =  imread('dist_corr_deintNarben0173_3.png');
    im =  imread(test_image_name);
    
    %We focus ONLY on the central region of image. This is a simple heuristic to
    %avoid expensive calculations outside of the field of view laparascopic
    %camera. We arbitrarily chose a rectangular region starting from pixel
    %number (150,50) to (550,450). Each test_roi is a 100*100 patch and
    %represents the neigborhood pixel no (50,50) of the patch.
    counter = 0;
    test_roi = [];
    for i = 150:450
        for j = 50:350
            counter = counter + 1;
            test_roi{counter}=imcrop(im,[i j 100 100]);
        end
    end

    disp('....read all test images pixel by pixel.')
    
    %Go back to working dir.
    cd(param.workfolder)

    %Create an empty matrix to store classification results.
    pred_sift = zeros(size(test_roi,2),2);
    %Create index. counter = 90601.
    pred_sift(:,1) = 1:counter;
  
    %Create testing data vector.
    testing_data_vector = zeros(counter,128);    
    for i = 1:counter
        %Get SIFT feature vectors. Processing similar to that done on
        %training data vector.
        [f,d] = vl_sift(imcrop(im2single(rgb2gray(test_roi{i})),[0 0 100 100]),'frames',fc) ;
        testing_data_vector(i,:) =  d';
    end
        
    %Convert single back to double.
    testing_data_vector = double(testing_data_vector);
   
    
    
%Train & Classify.
    disp('Starting to do pca on SIFT data.');
    %Do pca on DATA and reduce the dim to #pca_dimension. Get test data shifted
    %to these pca_dimensions. pca_dimension = 3.
    [W_pca TW_pca] = doPCAwithDimReduction(training_data_vector, testing_data_vector, pca_dimension);
    
    %TRAIN Logistic Classifier.
    disp('Training on SIFT data...');
    hyp_fit = glmfit(W_pca,train_class_membership_vector,'binomial');

    %Classify.
    disp('...Classifying SIFT data.');
    pred_sift(:,2) = glmval(hyp_fit,TW_pca, 'logit');
    disp('...Done Classifying SIFT data!');
   
    
    
%%%%%%%%%%%%%%%%%%%%%% RGB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Classify based on RGB vectors.
   


%Load Training Data.

    disp('Creating training data vector for RGB. ');

    
    %Liver
    
    cc = 0;
    %718 test images. 7500 = 50 * 50 * 3 ( 100*100 patch shrunk to 50*50 and 3 for RGB values.)
    training_data_vector = zeros(718, 7500);
    for pat_no = 1:num_patients
        for i = 1 : (numel(patients_liver(pat_no).roi))
            cc = cc + 1;
            training_data_vector(cc,:) = resizeRoiForRGB(patients_liver(pat_no).roi{i});
        end
    end
    
    %Bkg.
    for pat_no = 1: num_patients
        for i = 1 : (numel(patients_bkg(pat_no).roi))
            cc = cc + 1;
            training_data_vector(cc,:) = resizeRoiForRGB(patients_bkg(pat_no).roi{i});
        end
    end

    
    %Create Training labels. Liver = 1. Bkg = 0.
    liver_images = ones(numel_liver_images,1);
    bkg_images = zeros(numel_bkg_images,1);
    train_class_membership_vector = [liver_images ; bkg_images]; %Ground Truth.


        
%Load testing Data. 

    %Create an empty matrix of size 90601 * 2 to store classification results. 
    pred_rgb = zeros(size(pred_sift));
    %Create index. counter = 90601.
    pred_rgb(:,1) = 1:counter;

    %testing data is available in test_roi[].
    disp('Creating testing data vector for RGB. ');
    testing_data_vector = zeros(counter, 7500);
    for i = 1:counter
        testing_data_vector(i,:) = resizeRoiForRGB(test_roi{i});
    end
        
    
 
    
%Train and Classify.
    %Do pca on DATA and reduce the dim to #pca_dimension. Get test data shifted
    %to these pca_dimensions. pca_dimension = 3.
    disp('Starting to do pca on rgb data.');
    [W_pca TW_pca] = doPCAwithDimReduction(training_data_vector, testing_data_vector, pca_dimension);
    
    %TRAIN Logistic Classifier.
    disp('Training on SIFT data...');
    hyp_fit = glmfit(W_pca,train_class_membership_vector,'binomial');
    
    %Classify.
    disp('Classifying rgb data....');
    pred_rgb(:,2) = glmval(hyp_fit,TW_pca, 'logit');
    disp('....Done classifying rgb data.');
  
    
    
    
    
    
%%%%%%%%%%%%%%%%%%%%%% PLOT Probability maps and overlays %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    
% Get original image. We are interested in a 400*400 region somewhere in the
%center. We chose x_min and y_min to be 150 and 50 arbitrarily to avoid
%expensive calculations in irrelevant out of view region of laprascopic
%images.
I = imcrop(im,[150,50,400,400]);
    

%Plot Original image.
imshow(I);
title('Original Image','FontWeight','bold');
figure;




%Initilze probability map of correct size.
p_map_liver = zeros(401,401);
%Initialize.
p_map_liver(:,:) = .5;

    for i = pred_sift(:,1)'
        %Get the row and col values. This arithmatic depends on the way the
        %testing data was created. Just translating index no to corresponding
        %image pixel location.
        row = rem(pred_sift(i,1),301);
        col = ceil(pred_sift(i,1)/301);
    
        % MATLAB does not like index values to be zero. Use of rem works well
        % for other indixes except at the end. HACK to fix that.
        if row == 0
            row = 301;
        end
 
        %OFFSET. The p value associated with a 100*100 patch is assigned to the
        %central pixel. Offset ensures the p value is assigned to the 
        %(x_min+50, y_min+50)
        row = row + 50;
        col = col + 50;
    
        %Assign the p value to the central pixel.
        p_map_liver(row,col) = pred_sift(i,2);
    end
    


    %Plot SIFT probability map.
    imshow(p_map_liver);
    title('SIFT Probability Map','FontWeight','bold');
    figure;
    




    %Plot.
    imshow(I);
    title('SIFT Probability Map Overlay','FontWeight','bold');
    hold on;
    p_overlay_liver = imshow(p_map_liver);
    set(p_overlay_liver,'AlphaData',.6); %.6 is arbitrary. You can use any number between 0 to 1 to set the level of transparency.
    hold off;
    figure;
    
    
    
    clear p_map_liver;
    %Initilze probability map of correct size.
    p_map_liver = zeros(401,401);
    %Initialize.
    p_map_liver(:,:) = .5;
    
    
    for i = pred_rgb(:,1)'
        %Get the row and col values. This arithmatic depends on the way the
        %testing data was created. Just translating index no to corresponding
        %image pixel location.
        row = rem(pred_rgb(i,1),301);
        col = ceil(pred_rgb(i,1)/301);
    
        % MATLAB does not like index values to be zero. Use of rem works well
        % for other indixes except at the end. HACK to fix that.
        if row == 0
            row = 301;
        end
 
        %OFFSET. The p value associated with a 100*100 patch is assigned to the
        %central pixel. Offset ensures the p value is assigned to the 
        %(x_min+50, y_min+50)
        row = row + 50;
        col = col + 50;
    
        %Assign the p value to the central pixel.
        p_map_liver(row,col) = pred_rgb(i,2);
    end
    

    %Plot RGB probability map.
    imshow(p_map_liver);
    title('RGB Probability Map','FontWeight','bold');
    figure;
    

    

    %Plot.
    imshow(I);
    title('RGB Probability Map Overlay','FontWeight','bold');
    hold on;
    p_overlay_liver = imshow(p_map_liver);
    set(p_overlay_liver,'AlphaData',.6); %.6 is arbitrary. You can use any number between 0 to 1 to set the level of transparency.
    figure;
 
    
%%%%%%%%%%%%%%%%%%%%%% Get SEEDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% Threshold probability at > 95% to get liver seeds.
predicted_Liver = [];
for i = 1:counter
    if ((pred_sift(i,2)>.95)) 
        predicted_Liver = [predicted_Liver ; i ];
    end
end
   
% Threshold probability at < 5% to get bkg seeds.
predicted_Bkg = [];
for i = 1:counter
    if ((pred_sift(i,2)<.05)) 
        predicted_Bkg = [predicted_Bkg ; i ];
    end
end

%These are huge data structure. Purge it to speed up save in the next step.
clear test_roi; clear testing_data_vector; 

%Save the working envt. Seeds stored in  predicted_Liver and predicted_Bkg
%to be used for graph cuts.
save SEEDS




%Plot seeds.

full_image_overlay = zeros(544,720); %Matrix size = image size.
full_image_overlay(:) = .5; % Initilize to grey.
%Liver
for im_no = predicted_Liver'
    
    row = ceil(im_no/301);
    col = rem(im_no,301);
    
    if col == 0
        col = 301;
    end
 
    row = row + 50;
    col = col + 50;
    
   full_image_overlay(col+50, row+150) = 1;  %Liver is denoted with white.
end
%Background.
for im_no = predicted_Bkg'

    row = ceil(im_no/301);
    col = rem(im_no,301);
    
    if col == 0
        col = 301;
    end
 
    row = row + 50;
    col = col + 50;
    
   full_image_overlay(col+50, row+150) = 0;  %Bavkground is denoted with black.
end


liver_false_positive = 0;
liver_true_positive = 0;
bkg_false_positive = 0;
bkg_true_positive = 0;
for i = 1:544
    for j = 1:720
        if((full_image_overlay(i,j) == 1) && (im_gt(i,j) == 0))
           liver_false_positive = liver_false_positive + 1;
        end
        if((full_image_overlay(i,j) == 0) && (im_gt(i,j) == 1))
           bkg_false_positive = bkg_false_positive + 1;
        end
        if ((full_image_overlay(i,j) == 1) && (im_gt(i,j) == 1))
            liver_true_positive = liver_true_positive + 1;
        end
        if ((full_image_overlay(i,j) == 0) && (im_gt(i,j) == 0))
            bkg_true_positive = bkg_true_positive + 1;
        end
    end
end

Precision_liver = liver_true_positive/(liver_true_positive+liver_false_positive)
Precision_bkg = bkg_true_positive/(bkg_true_positive+bkg_false_positive)


%Plot full image and overlay seeds.
    imshow(im);
    title('SEEDS plotted on real test image.','FontWeight','bold');
    hold on;
    seeds_overlay = imshow(full_image_overlay);
    set(seeds_overlay,'AlphaData',.6); %.6 is arbitrary. You can use any number between 0 to 1 to set the level of transparency.

    
    
    
    
    
    
    
    