function [ S ] = ApplyBrushsize( S, I )
% Set Brush size and resize Points using convolution

if(isempty(S))
    error('No scribbles found.');
end

if(isempty(I))
    error('No image found.');
end

if(PARAMS.BRUSHSIZE ~= 1)
    
    [height, width, ~] = size(I);

    ScribbleMap = zeros(height, width);

    ScribbleMap(sub2ind(size(ScribbleMap), S(:,CONST.COL_SROW), S(:,CONST.COL_SCOL))) = S(:,CONST.COL_SGROUP);

    %Get Radius as 'window' function to convolve with
    % Create radius matrix like [M1]
    [xgrid, ygrid] = meshgrid(1:2*(PARAMS.BRUSHSIZE/2) + 1, 1:2*(PARAMS.BRUSHSIZE/2) + 1);   
    x = xgrid - (PARAMS.BRUSHSIZE/2) - 1;    % offset of origin
    y = ygrid - (PARAMS.BRUSHSIZE/2) - 1;
    RadiusWindow = x.^2 + y.^2 <= (PARAMS.BRUSHSIZE/2).^2;

    % Widen every pixel to desired brushsize using convolution
    completeMap = zeros(height, width);
    for i=1:CONST.MAX_REGIONS
        ScribbleMapConv = conv2(double(ScribbleMap==i), double(RadiusWindow), 'same');
        completeMap(ScribbleMapConv~=0) = i;
    end

    % Use only PARAMS.BRUSH_DENS of previously defined pixels
    completeMap(rand(size(completeMap)) < (1 - PARAMS.BRUSH_DENS)) = 0;
    
    % Get indexed version
    S = ScribbleMapToIndex(completeMap);
end

%delete all redundant rows
S = unique(S, 'rows');

end

