\chapter{Implementation}
\label{ch:implementation}
This chapter deals with the implementation of the proposed algorithm in
\cite{nieuwenhuis-cremers-pami12_2}. The vast number of image processing and
computer vision possibilities as well as easy interactive opportunities led to
an implementation in \matlab \textsc{R2015b} \cite{MATLAB:2015b}. A drawback
by choosing \matlab as the programming language is a relatively slow runtime of
the algorithm compared to other languages like C++.

\section{Segmentation Algorithm}
\label{sec:segmentation_algorithm}
The main idea of the implementation is to provide a function that processes an image and corresponding scribbles and
compute a valid segmentation result which is done in the steps that are shown
in \cref{alg:segmentation}.
\begin{algorithm}[htb]
\caption{Segmentation Algorithm}
\label{alg:segmentation}
\begin{algorithmic}[1]
	\Function{SegmentImage}{$Image, Scribbles$}
	\State Calculate Dataterm $f$ from $Image$ and $Scribbles$
	\State Get Segmentation result $\theta$ from relaxed Optimization($Image$, $f$)
	\State Backproject entries of $\theta$ to binary set \{0, 1\}
	\State \Return $\theta$
	\EndFunction
\end{algorithmic}
\end{algorithm}
\subsection{Probability Estimation}
The algorithm starts by creating the pixel-wise probability estimation to the
corresponding set of scribbles by obtaining the color and spatial
estimators. They can be calculated independently due to the
separability of the Gaussian kernel as shown in \cref{eq:finalprob_sep_kernels}.
Afterwards, they are combined element-wise by multiplication before the overall
estimator can be gathered by the sum of the combined kernel functions as shown
in \cref{alg:estimation}.

\begin{algorithm}[htb]
\caption{Probability Estimation Algorithm}
\label{alg:estimation}
\begin{algorithmic}[1]
	\Function{ProbabilityEstimation}{$Image, Scribbles$}
	\For{$i = 1, \ldots, n$ scribble groups}
		\For{$j = 1, \ldots, m_i$ scribbles in $S_i$}
			\State $Kernel(i,j)$ = $GetColorKernel(i,j)$ * $GetSpatialKernel(i,j)$
		\EndFor
	\EndFor
	\State\Return $Kernel$
	\EndFunction
\end{algorithmic}
\end{algorithm}

\subsubsection*{Color Probability Estimation}
The implementation of the color probability estimator 
\begin{equation}
	\tilde{\mathcal{P}}_{Color}(I(x)\;|\;u(x) = i) =
	\sum_{j=1}^{m_i} k_\sigma(I - I_{ij})
  \label{eq:kernel_color_general}
\end{equation}
neglects the spatial argument $x$ because it has no impact on
the estimation result. In the implementation, a Gaussian kernel with zero mean
and standard deviation $\sigma$ as presented in \cref{eq:gaussian_kernel} is used following the original algorithm proposal. The
estimator therefore results in 
\begin{equation}
  \tilde{\mathcal{P}}_{Color}(I(x)\;|\;u(x) = i) = \sum_{j = 1}^{m_i}
  \mathcal{N}_{0; \sigma^2}(d_C(I, I_{ij}))
  \label{eq:kernel_color}
\end{equation}
with the color distance metric for RGB images
\begin{equation}
	d_C(I_1, I_2) \coloneqq \sqrt{(R_1 - R_2)^2 + (G_1 - G_2)^2 + (B_1 -
	B_2)^2} \;.
  \label{eq:colorDist}
\end{equation}
It applies a Gaussian kernel to color distances
$d_C(I, I_{ij})$ where $I$ corresponds to the original image and $I_{ij}$ to the color of the $j$-th scribble of scribble group $i$.
The idea of the implementation is to create a 3D-matrix $D_C \in \mathbb{R}^{I_H
\times I_W \times m_i}$ for every scribble group $S_i$ that contains the color
distances of every pixel to every scribble color $I_{ij}$ in the third
dimension. Afterwards, Gaussian kernels are applied element-wise to the
distances with zero mean and the standard deviation $\sigma$.
\cref{alg:colorprob} demonstrates the pseudocode of the color
probability estimation.
\begin{algorithm}[htb]
\caption{Color Probability Estimation Algorithm}
\label{alg:colorprob}
\begin{algorithmic}[1]
	\Function{GetColorKernel}{$Image, Scribbles$}
	\For{$i = 1, \ldots, n$ scribble groups $S_i$}
		\For{$j = 1, \ldots, m_i$ scribble colors $I_{ij}$}
			\State $ColorKernels(i, j) \leftarrow \mathcal{N}_{0; \sigma^2}(d_c(I,
			I_{ij})) $
		\EndFor
	\EndFor
	\State\Return $ColorKernels(i, j)$
	\EndFunction
\end{algorithmic}
\end{algorithm}

\paragraph{Example}
To get a better understanding of how the algorithm works, consider an image $I$
with nine pixels as shown in \cref{im:ex_3d_colordist_orgimage}. The top left
and top right pixels are chosen as scribbles of $S_1$. The user's intention is
to segment the red region from the blue. A perfect segmentation
result would choose all pixels except for the lower left and lower right pixel
to belong to one group $S_i$. After that, the resulting 3D-matrix contains the
color distances of all pixels to all scribbles.
The middle pixel in the top row is calculated as an example. Its color distance
$d_C(I, I_{11}) = 0$ to the first scribble is $0$, because it is the exact same
color as scribble $S_{11}$. Next, the color distance to the second scribble
$S_{12}$ is $$d_C(I, I_{12}) = d_C\left(\colvec{255;0;0},
\colvec{200;0;0}\right) = 55 \, .$$ This calculation is done for every $9$ pixels that all feature
two color distances which is shown in \cref{fig:ex_3d_colordist_mat}. The
highlighted square of each 2D submatrix corresponds to the respective scribble
itself always leading to a distance of $0$. Finally,
the color estimation $\tilde{\mathcal{P}}_{Color}(I(x)\;|\;u(x) = 1)$ is shown
in \cref{im:ex_3d_colordist_orgimage} (b) for each pixel.
All pixels that mainly feature a red color component have a higher value than
the pixels with a higher blue color component which corresponds to the desired
result. The matrix was normalized by the maximum to emphasize the
probability difference between red and blue pixels to get a better qualitative
impression of the estimation result.
\begin{figure}[htb]\centering
%###
	\begin{subfigure}[t]{0.48\textwidth}\centering
		\input{figure/colormatrix/image.tex}
		\caption{Original image}
	\end{subfigure}
    \begin{subfigure}[t]{0.48\textwidth}\centering
		\input{figure/colormatrix/final.tex}
		\caption{Estimated and normalized probabilities}
	\end{subfigure}
%###
	\caption{Exemplary image for color probability estimation}
	\label{im:ex_3d_colordist_orgimage}
\end{figure}
\begin{figure}[htb]\centering
%###
\input{figure/colormatrix/matrix}
%###
\caption{Exemplary 3D-matrix of calculated color distances}
\label{fig:ex_3d_colordist_mat}
\end{figure}

\subsubsection*{Spatial Probability Estimation}

The spatial probability estimator in the implementation follows the
formula
\begin{equation}
	 \tilde{\mathcal{P}}_{Spatial}(x\;|\;u(x) = i) = \sum_{j = 1}^{m_i}
	k_\rho(\lVert x - x_{ij} \rVert)
  \label{eq:kernel_spatial_general}
\end{equation}
where $k_\rho$ is implemented as a Gaussian kernel with zero mean and
standard deviation $\rho$. The argument $\lVert x - x_{ij}
\rVert$ denotes the euclidean distance between the location of every pixel
and the location of the scribble $S_{ij}$. These prerequisites lead to the final
spatial probability estimation term
\begin{equation}
	 \tilde{\mathcal{P}}_{Spatial}(x\;|\;u(x) = i) = \sum_{j = 1}^{m_i}
	\mathcal{N}_{0;\rho_i^2}(\lVert x - x_{ij} \rVert)\,.
  \label{eq:kernel_space}
\end{equation}
The value of $\rho_i$ has to be obtained for every pixel and
scribble group according to \cref{eq:rho}. This means the standard
deviation of the used Gaussian kernels is defined by the minimum distance of every pixel $x
\in \Omega$ and the closest scribble location $x_{ij} \in S_i$. The
implementation uses this idea as shown in the pseudocode in
\cref{alg:spatialprob} to obtain the kernels for spatial probability estimation. It creates a matrix
$D_S \in \mathbb{R}^{I_H \times I_W \times m_i}$ where the entries correspond to
the distances between each pixel and all scribble locations. Every 2D submatrix
along the third dimension corresponds to the spatial distance of all pixels to
one scribble location $x_{ij}$. Afterwards, the minimum in the third dimension
of $D_S$ multiplied with parameter $\alpha$ yields the standard deviation matrix
$\rho_i(x)$. Now, the Gaussian kernel function $\mathcal{N}_{0; \rho_i^2}(D_S)$
is applied to retrieve the final spatial probability estimation for every pixel.
\begin{algorithm}[htb]
\caption{Spatial Probability Estimation Algorithm}
\label{alg:spatialprob}
\begin{algorithmic}[1]
	\Function{GetSpatialKernel}{$Image, Scribbles$}
	\For{$i = 1, \ldots, n$ scribble groups $S_i$}
		\For{$j = 1, \ldots, m_i$ scribble locations $x_{ij}$}
			\State Get all Distances $D(i, j) \leftarrow \lVert x - x_{ij} \rVert$
		\EndFor
		\State Get standard deviation $\rho_i \leftarrow \alpha \min_j(D(i, j))$
		\For{$j = 1, \ldots, m_i$ scribble locations $x_{ij}$}
			\State $SpatialKernels(i, j) \leftarrow \mathcal{N}_{0; \rho_i^2}(D(i, j))$		
		\EndFor
	\EndFor
	\State\Return $SpatialKernels(i, j)$
	\EndFunction
\end{algorithmic}
\end{algorithm}
\subsubsection*{Slicing}
Since a lot of equal operations have to be done for every pixel, it is favorable
to do them for all pixels at once. This means that
the calculations are not done in for-loops but on matrices with the same width
and height as the actual image. Therefore, an operation gets computed faster at
the cost of memory usage. Dependent on the number of scribbles and reasonable
image sizes, the approximate memory used by the algorithm can extend the
available memory of the computer. To overcome this issue and cap the memory
usage, the implementation offers the possibility to limit the actual use of RAM by
slicing the image and do the kernel calculations in several parts.\\

\noindent The algorithm offers the possibility to divide the processed image in
non-overlapping subimages to both benefit from matrix-wise operations and lower
memory usage. The constant \verb+SLICE_N+ controls the number of segments that
the width and height split as presented in \cref{fig:slice_factor}. This
approach reduces the overall used memory by approximately
$1/SLICE\_N^2$. The slicing is done before the individual spatial and
color kernels are obtained in \cref{alg:estimation}. To point out, the
calculation of each pixel in the subimage is influenced by all scribbles even
though they do not lie in the current region that is calculated. This
is why the scribbles cannot be split by this method meaning all scribbles have
to be used on each subimage.
\begin{figure}[htb]\centering
	\input{figure/slice/example.tex}
	\caption{Image segments for different values of SLICE\_N}
	\label{fig:slice_factor}
\end{figure}
\subsection{Data Fidelity Term}
Before the dataterm can be calculated from the estimated probabilities, the
previously separated kernels are summed up in the third dimension of the
multiplied matrix following \cref{eq:finalprob_sep_kernels}. Afterwards the
product is normalized by $m_i$ for every scribble group $S_i$. At this point,
the final probability estimation is found and can be transformed into the data
fidelity term $f$ by taking the negative logarithm:
\begin{equation}
	f = -\log \tilde{\mathcal{P}}(I(x),\,x\;|\;u(x) = i)
  \label{eq:prob_to_energy}
\end{equation}
The implementation uses the natural logarithm to obtain the dataterm. Also, 
to avoid the singularity of the logarithm at $x = 0$, the obtained probability
estimations that are in the interval $[0, 1]$ are projected to the interval
$(0, 1]$ by calculating $\hat{\mathcal{P}} = (\tilde{\mathcal{P}} + \epsilon)
\cdot (1 - \epsilon)$ with $\epsilon$ being the float accuracy of the computing
machine.\\

\noindent Moreover, the dataterm conversion cannot be done for the user
selected pixels. \cref{eq:prob_to_energy} fails because they have a
spatial distance $D_S$ of $0$. As a result, $\rho_i$ results in $0$ leading to
a Gaussian kernel with zero variance. This work assumes that $\mathcal{N}_{\mu;
0}(x)$ is undefined so the scribbles' energy has to be set manually as
\begin{equation}
f_i (x_{kj}) = \left\{
	\begin{array}{ll}
		0 & \mbox{if } k = i \\
		E_\infty & \mbox{else }
	\end{array} \right.,\;
	\forall j = 1, \ldots m_i,\; \forall i = 1, \ldots, n
  \label{eq:scribble_energy_set}
\end{equation}
with $i$ representing the $i$-th layer of $f$ that corresponds to the $i$-th
scribble group and $E_\infty$ being the energy for scribbles of other scribble
groups $S_k$. This means that user selected scribbles get a very low energy
when they belong to the current region or a very high energy if the do not.\\

\noindent
To get a better understanding of the resulting dataterms,
\cref{fig:dataterm_exs} shows examples for $f_i$. The
brightness in the pictures of column (b) and (c) correspond to high energy,
meaning the brighter a pixel, the less likely it belongs to the current scribble group.
\begin{figure}[htbp]\centering

	\begin{tabular}{ccc}
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/raupe_scrib.png} &
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/raupe_dataterm_1.png}
		&
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/raupe_dataterm_2.png} \\
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/ducks_scrib.png} &
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/ducks_dataterm_1.png}
		&
		\includegraphics[width=.33\textwidth]{image/Dataterm_exs/ducks_dataterm_2.png}
		\\

	(a) Original Image & (b) Dataterm for $S_1$ & (c) Dataterm for $S_2$ \\[6pt]
	\end{tabular}


  \caption{Dataterms for selected images}
  \label{fig:dataterm_exs}
\end{figure}

\subsection{Optimization}
\begin{algorithm}[htb]
\caption{Optimization Algorithm}
\label{alg:optimization}
\begin{algorithmic}[1]
	\State Initialize $\theta^1$ and $\xi^1$ with zeros
	\State Get Image Gradient Metric G as in \cref{eq:std_regularization}
	\For{$t = 1, \ldots, Step_{max}$}
		\State Update Dual variable $\xi^{t+1} = \Pi_{\mathcal{K}_g} (\xi^t + \tau_d
 \nabla \bar{\theta}^t)$
		\State Update Primal variable $\theta^{t+1} = \Pi_{\tilde{\mathcal{B}}} \left( \theta^t + \tau_p (\divergence \xi^{t+1} -
 \lambda f) \right)$
		\State Extrapolation: $\bar{\theta}^{t+1} = 2 \theta^{t+1} - \theta^t$
		\If{Primal-Dual gap changed by less than $\mathcal{G}_{thresh}$}
			\State Exit Loop
		\EndIf
	\EndFor
	\State $\hat{\theta} = \argmax_i\left(Backproject(\theta_i^{t+1})\right)$
	\State \Return $\hat{\theta}$

\end{algorithmic}
\end{algorithm}
The overall iteration process shown in \cref{alg:optimization} solves
\cref{eq:final_formula} by using the Primal-Dual-Algorithm introduced in
\cref{eq:primdual} to get a segmentation result with close to minimal energy.\\

\noindent
The algorithm starts with initializing the primal variable $\theta$ and
dual variable $\xi$ with zeroes. Afterwards, it enters the iteration process and
updates the primal and dual variables accordingly. It exits the iteration
process if either the maximum number of iteration steps is reached or the
primal-dual gap change is smaller than the stated threshold. The number of
steps $Step_{max}$ that the iteration process will take before it eventually
terminates should only be used to ensure that the algorithm can guarantee to
terminate and not to increase the performance in dependency of the segmentation
quality. For this purpose, the minimum relative primal-dual gap
$\mathcal{G}_{pd}$ change per iteration introduced in
\cref{par:convergence_crit} should be used. After the iteration part, the
backprojection of $\theta$ is done by setting the maximum value of
$\theta$ in the third dimension for every pixel to $1$ and the others to $0$.
The function returns a compressed version $\hat{\theta}$ of the thresholded
result by storing only the index of the (first) maximum value of $\theta$ along the third
dimension for every pixel. The matrix $\hat{\theta}$, which is the same size as
the original image and contains the labeling for each pixel, is then returned.
\section{Graphical User Interface}
\begin{figure}[htb]\centering
  \includegraphics[width=.9\textwidth]{image/GUI.png}
  \caption{Implemented \matlab GUI overview}
  \label{im:gui_overview}
\end{figure}
Besides the algorithm implementation, a graphical user interface was developed
using the internal \matlab GUI creator called GUIDE \cite{smith2006matlab}.
The main reason to develop a GUI is to provide easy access to interactively
drawing scribbles directly on any image and calculate a segmentation with a
simple button press. For that purpose, a canvas is necessary for drawing on a
previously defined picture. This is why the external library \cite{Jacobs.2015}
was included in the project. Furthermore, the GUI is also capable of displaying groundtruth data. On top of that, dice and edge score
that are shown in \cref{sec:eval_methods} can be calculated for the previously
calculated segmentation compared to the loaded groundtruth data.\\

\noindent
To give an overview of the final GUI, it is separated into four regions. In
general, it lets the user select an image, draw scribbles of different
segments, calculate the segmentation result and compare the solution to a
groundtruth image. The final GUI is presented in \cref{im:gui_overview}. The GUI
should be used in the following order:
\begin{enumerate}
  \item The top left part consists of two buttons to
		either load any image or load an image with predefined scribbles and
		groundtruth data from the ICG database from \cref{subsec:icg}. It is also
		possible to clear all previously entered data.
  \item The top right holds the canvas that allows user interaction. The image
		selected in the first part gets automatically copied here to let the user
		draw scribbles on it.
  \item The bottom left button starts the segmentation algorithm with
  		predefined parameters and shows the result on the corresponding canvas.
  		Optionally, it illustrates intermediate segmentation steps.
  \item With the ``Load GT'' button, a groundtruth image can be loaded and
  		compared to the segmentation result. This image should
		correspond to the previously loaded and segmented image and be
  		consistent with \cref{subsec:groundtruthdata}. It is also offered the
  		possibility to compare the resulting segmentation to
  		groundtruth data for the scoring schemes introduced in
  		\cref{sec:eval_methods} quantifying the segmentation result.
\end{enumerate}
\subsection{Scribble Input}
To allow the user to draw scribbles on an image, the
library \cite{Jacobs.2015} was modified to fit the needs of this GUI
implementation. It returns an array of the user selected pixels and draws them
on the canvas. The user has to select a scribble group and click on
``Add Scribbles'' prior to each drawing after releasing the mouse button.\\

\noindent
Moreover, the implementation offers the possibility to combine reasonable
brush sizes while keeping the runtime as low as possible by letting the user
select pixels and then choosing only a random subset of them for the
segmentation algorithm. To add scribbles, the user needs to select the scribble
group, then click ``Add scribbles'' and start drawing. The user has
to push this button every time after the mouse is released if he wants to add
more scribbles. Even though the brush size heavily depends on the actual image
size, reasonable brush sizes should be chosen to better cover
color variations within the scribble regions.\\

\noindent
The brush size $B$ is also implemented in the algorithm. The GUI defines a
radius $R = \lfloor B / 2\rfloor$ around the initially selected pixel. When
a pixel is selected, all surrounding pixels inside the given circle get
selected as well. This is illustrated in \cref{fig:brushsize}. Afterwards,
the brush size density factor only takes a fraction of the previously selected
pixels as scribble input.\\
\begin{figure}[htb]\centering
%###
    \begin{subfigure}[t]{.32\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Scribbles/scribs_bird}
    \end{subfigure}
    \begin{subfigure}[t]{.32\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Scribbles/scribs_metal}
    \end{subfigure}
    \begin{subfigure}[t]{.32\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Scribbles/scribs_starfish}
    \end{subfigure}
%###
  \caption{Samples of user selected scribbles for different images}
  \label{im:scribble_example}
\end{figure}
\begin{figure}[htb]\centering
%###
    \begin{subfigure}[t]{0.3\textwidth}\centering
	  \input{figure/brushsize/pixelsize4.tex}
      \caption{B = 4}
    \end{subfigure}
    \begin{subfigure}[t]{0.3\textwidth}\centering
	  \input{figure/brushsize/pixelsize7.tex}
      \caption{B = 7}
    \end{subfigure}
    \begin{subfigure}[t]{0.3\textwidth}\centering
	  \input{figure/brushsize/pixelsize8.tex}
      \caption{B = 8}
    \end{subfigure}
%###
\caption{Different brush sizes visualized on pixel level}
\label{fig:brushsize}
\end{figure}
\subsection*{Intermediate Segmentation Results}

The implementation offers the opportunity to show the backprojected
mask $\hat{\theta}$ of the optimization process after each iteration step in the
GUI and also save it as an image. This way the user gets a better idea how the
energy optimization process works and what regions are affected the most. If the
segmentation result does not correspond to the user's intention, the scribbles
can be adjusted accordingly. \cref{fig:live_segmentation} shows sample
intermediate masks generated from the intermediate segmentation results of the
GUI for different optimization steps $t$. It can be seen that for $t = 1$, a
lot of small blue regions are present in the red areas of the image. These
areas get removed in the iteration process leading to nearly complete removal
at $t = 25$. For step $t = 50$, only one blue region remains that corresponds
to the user intended segmentation.

\begin{figure}[p]\centering
%###
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/scribs.png}
	  \caption{Image with user scribbles}
    \end{subfigure}
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/image-1.png}
	  \caption{Optimization step $t = 1$}
    \end{subfigure}
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/image-5.png}
	  \caption{Optimization step $t = 5$}
    \end{subfigure}
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/image-10}
	  \caption{Optimization step $t = 10$}
    \end{subfigure}
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/image-25}
	  \caption{Optimization step $t = 25$}
	\end{subfigure}
    \begin{subfigure}[t]{.48\textwidth}\centering
        \includegraphics[width=\textwidth, height
        = .75\textwidth]{image/Optisteps/image-50}
	  \caption{Optimization step $t = 50$}
    \end{subfigure}
%###
  \caption{Exemplary intermediate optimization results for $\theta$}
  \label{fig:live_segmentation}
\end{figure}

\subsection*{External Libraries}

This section briefly covers the main \matlab libraries that are used in the
implementation of the graphical user interface.\\

\noindent
Firstly, a library that lets the user draw on a figure or image like on a
canvas was found with \cite{Jacobs.2015}. It was modified for this work's scope.\\

\noindent
Secondly, to evaluate the dice score discussed in \cref{eq:diceScore}, the
original \matlab implementation that is provided with \cite{Santner2010} is used. The
dice score implementation and image database including user scribbles also
helped in the evaluation process.\\

\noindent
At last, the original \matlab implementation of \cite{chen2011projection} was
used to get the projection onto a simplex as depicted in \cref{subsec:simplex}.
However, for $n = 2$ regions, a new algorithm operating on a whole image instead
of a pixel-wise computation was developed and used in the implementation.\\