function [ xi ] = GetProjectionK( xi, R )
%GETPROJECTIONK Project back to disk of size r by clipping

n = size(xi, 3);

for i = 1:n

    % If norm of xi > R -> Clip at R
    currNorm = sqrt(xi(:,:,i,1).^2 + xi(:,:,i,2).^2);
    xiNorm = currNorm ./ R;

    % Only norm where normFactor > 1, else set normFactor = 1
    xiNorm(xiNorm < 1) = 1;
    
    % Also norm where values are missing
	xiNorm(isnan(xiNorm)) = 1;
    xiNorm(isinf(xiNorm)) = 1;

    % Norm each xi_i in first and second dim
    xi(:,:,i,:) = xi(:,:,i,:) ./ repmat(xiNorm, 1,1,1,2);

end

end

