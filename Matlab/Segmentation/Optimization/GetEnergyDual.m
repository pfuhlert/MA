function [ DualEnergy ] = GetEnergyDual( f, xi)
%GETENERGY Calculate Energy as in EQ 31

% Third dimension of f, theta or xi equals no of Labels
[height, width, n] = size(f);

tempMat = zeros(height, width, n);

for i = 1:n

    % Calculate energy
    tempMat(:,:,i) = PARAMS.LAMBDA .* f(:,:,i) - GetDivergence(xi, i);
end

    PtMin = min(tempMat, [], 3);
    DualEnergy = sum(sum((PtMin)));

end
