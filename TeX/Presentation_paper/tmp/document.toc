\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Overview}{3}{0}{1}
\beamer@sectionintoc {2}{Rate Monotonic Scheduling}{17}{0}{2}
\beamer@sectionintoc {3}{Earliest Deadline First}{39}{0}{3}
\beamer@sectionintoc {4}{Comparison}{57}{0}{4}
