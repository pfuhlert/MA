function [  ] = ShowScribbleMap( I, ScribImg )
%SHOWSCRIBBLEMAP Summary of this function goes here
%   Detailed explanation goes here

    imshow(I * CONST.MASKIMAGE_MIN_BRIGHT);
    hold on
    currFigure = imshow(mat2gray(ScribImg));
    
    % Remove Image where no Scribbles are
    set(currFigure, 'AlphaData', sum(ScribImg,3) ~= 0 );
    hold off

end

