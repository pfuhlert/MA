function [pred] = classifyOnGlcm()
% CLASSIFYONGLCM Train and classify on a cross validation data set of 7
% patients.
% classifyOnGlcm() calculate GLCM features and produces classification
% predictions.

%Global variables.
global num_patients;
global test_patient_folder;
global patients_liver;
global patients_bkg;
global numel_liver_images;
global numel_bkg_images;

%Calculate the number of test images. This varies based on which patient is
%a test patient in this iteration.
size_of_test_images = numel(patients_liver(test_patient_folder).roi) + numel(patients_bkg(test_patient_folder).roi);

%Create an empty matrix.
pred = zeros(size_of_test_images,3);
%Create index. 
pred(:,1) = (1:1:size_of_test_images);
%True labels.
pred(:,2) = [ones(numel(patients_liver(test_patient_folder).roi),1); zeros(numel(patients_bkg(test_patient_folder).roi),1)];


%Create Training data vector.
data_elements = [];
counter=1;
%For each patient
for pat_no = 1:num_patients
    %But not the test patient
    if pat_no ~=  test_patient_folder
        %For every image of patient.
        for i = 1 : (numel(patients_liver(pat_no).roi))
            %calculate GLCM metric for Liver ROIs.
            glcm = graycomatrix(rgb2gray(patients_liver(pat_no).roi{i}),'NumLevels',8);
            stats = graycoprops(glcm);
            data_elements(counter,1) =  stats.Contrast;
            data_elements(counter,2) =  stats.Energy;
            data_elements(counter,3) =  stats.Homogeneity;
            %Count.
            counter = counter + 1;
        end
    end
end

%Similarly, do for Background ROIs.
for pat_no = 1:num_patients
    if pat_no ~=  test_patient_folder
        for i = 1 : (numel(patients_bkg(pat_no).roi))
             glcm = graycomatrix(rgb2gray(patients_bkg(pat_no).roi{i}),'NumLevels',8);  
             stats = graycoprops(glcm);
             data_elements(counter,1) =  stats.Contrast;
             data_elements(counter,2) =  stats.Energy;
             data_elements(counter,3) =  stats.Homogeneity;
             counter = counter + 1;
        end
    end
end


%create labels for training. 
liver_images = ones(numel_liver_images - numel(patients_liver(test_patient_folder).roi) ,1);
bkg_images   = zeros(numel_bkg_images  - numel(patients_bkg(test_patient_folder).roi) ,1);
labels = [liver_images ; bkg_images];




%Train SVM.
svmModel = svmtrain(data_elements,labels);



%Create Testing data vector.
test_elements = [];
counter = 1;
%Liver.
for i = 1 : (numel(patients_liver(test_patient_folder).roi))
        glcm = graycomatrix(rgb2gray(patients_liver(test_patient_folder).roi{i}),'NumLevels',8);
        stats = graycoprops(glcm);
        test_elements(counter,1) =  stats.Contrast;
        test_elements(counter,2) =  stats.Energy;
        test_elements(counter,3) =  stats.Homogeneity;
        counter = counter + 1;
end

%Background.
for i = 1 : (numel(patients_bkg(test_patient_folder).roi))
        glcm = graycomatrix(rgb2gray(patients_bkg(test_patient_folder).roi{i}),'NumLevels',8);  
        stats = graycoprops(glcm);
        test_elements(counter,1) =  stats.Contrast;
        test_elements(counter,2) =  stats.Energy;
        test_elements(counter,3) =  stats.Homogeneity;
        counter = counter + 1;
end



%Classify using the trained SVM classifier.
for i = 1: numel(pred(:,1))
    pred(i,3) = svmclassify(svmModel,test_elements(i,:), 'Showplot',false);
end



%Show plot of the 1st 3 dimensions.
%HACK *** ALERT: Using a specefic test_patient_folder check
%to ensure the plotting happens only once. By design, this function will be
%called multiple times on the same data (leave one out!) and hence the
%check stops multiple replottings.
if test_patient_folder == 1 %HACK ***
    
    figure;
    %Liver.
    scatter3(data_elements((labels==1),1),data_elements((labels==1),2),data_elements((labels==1),3),ones(sum((labels==1)), 1)*5,'fill');
    hold on;
    %Background.
    scatter3(data_elements((labels==0),1),data_elements((labels==0),2),data_elements((labels==0),3),ones(sum((labels==0)), 1)*5,'fill');
    
    %metadata.
    title('Classification on GLCM');
    xlabel('Contrast Component');
    ylabel('Energy Component');
    zlabel('Homogeneity Component');
    legend('Liver-Train','Bkg-Train');
end
