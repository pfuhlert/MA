%% Prerequisites

clear
clc
close all
addpath(genpath('../../')) 
set(0,'DefaultFigureWindowStyle','Docked')

imagesPath = '../../../Libraries/icgbench/groundtruth/';

% For current run, create a folder w current datetime
datetime = datestr(now, 'yyyy-mm-dd_HH-MM-SS');
resultsPath = ['../../Results/' datetime '/'];
mkdir(resultsPath);

% Also create a diary log
diary([resultsPath 'log.txt']);
diary on

%% Set Parameter and Values 
% First Reset to original values
PARAMS.Init

% Print used Parameters on start of log file
PARAMS.Get;
disp(CONST)
overallTimer = tic;

% Get List of files
files = dir([imagesPath '*.gt']);
files = {files.name};

% Get all scores as matrix NOFiles x Sweeps x 2 (Dicescore, edgescore)
scores = zeros(length(files), 1, 2);

%% Loop
for i = 1:length(files)

    %% Prerequisites

    gtName = char(files(i));
    
    % Add path
    gtFile = [imagesPath gtName];    
    
    %% GT
    [gtLabels, ScribbleMap, ~, image_name] = readFromFile(gtFile);
    I = imread([imagesPath '../images/' image_name]);
    
    ScribMap = ScribbleMap + 1;
    
    S = ScribbleMapToIndex(ScribMap);
    
    gtLabels = gtLabels + 1;
    
    % Resize for performance increase according to CONST.RESIZE_FACTOR
    I = ResizeI(I);
    thetaGT = ResizeGT(gtLabels);
    S = ResizeS(S);       

    %% Set new parameter
    PARAMS.Init
    PARAMS.Get();

    %% Start of algorithm
    if(length(unique(S(:,CONST.COL_SGROUP))) < 2)
        warning('At least 2 Regions from Seeds required');
        continue;
    end
    
    theta = SegmentImage(I, S);

    %% Store Data
    % Save Calculated Segmentation
    saveFileName = [resultsPath 'theta_' image_name '.mat'];
    save(saveFileName, 'theta')

    %% Evaluate Data
    scores(i, 1, 1) = calculateDiceScore(thetaGT, theta);
    % This takes too long. Not feasable for a whole sweep!
    if((sum(sum(theta))) == 0 || (sum(sum(thetaGT)) == 0))
        warning('Evaluation cannot be computed on empty mask')
        continue;
    end
    scores(i, 1, 2) = EvaluateEdge(thetaGT, theta);
end

%% Saving, Plotting 
save([resultsPath 'scores'], 'scores');

handler = PlotScores(1:length(files), scores, 'Original Parameter results');

saveas(handler,[resultsPath 'score'],'png');

toc(overallTimer)
disp('done.');
diary off

% When done, shut down in 10 minutes
%system('shutdown -s -t 600')
