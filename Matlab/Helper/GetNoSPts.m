function [ nSPts ] = GetNoSPts( S )
%GETNOSPTS No of ScribblePts in each GRP
% Get numbers of Scribblepoints within each group

nSPts = accumarray(S(:,CONST.COL_SGROUP),1);

end

