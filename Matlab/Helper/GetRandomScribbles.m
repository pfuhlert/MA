function [ S ] = GetRandomScribbles( I )
%GETRANDOMSCRIBBLES Place random scribbles on an image
%   The amound of scribbles is set in ReduceScribbles()

randseed(1);
[height, width, ~] = size(I);
randImage = rand(height, width);
ScribbleMask = randImage > (1 - CONST.GT_SEED_PCT);
ScribbleMap = (double(~I)+1);
ScribbleMap = ScribbleMap .* ScribbleMask;
S = ScribbleMapToIndex(ScribbleMap);
S = ReduceScribbles(S);

end

