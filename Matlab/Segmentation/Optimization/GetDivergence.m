function [ divXi ] = GetDivergence( xi, i )
%GetDivergence Divergence of ith component of xi(:,:,i,:) as described in [1]
% in section 4.5.1

[height, width, ~, ~] = size(xi);

% Cancel third dimension (singular)
xiRow = squeeze(xi(:,:,i,1));
xiCol = squeeze(xi(:,:,i,2));

% Calc xi1
xiRow_dy = zeros(height,width);
xiRow_dy(1,:) = xiRow(1,:);
xiRow_dy(end,:) = - xiRow(end-1,:);
xiRow_dy(2:end-1,:) = xiRow(2:end-1,:) - xiRow(1:end-2,:);


% Calc xi2
xiCol_dx = zeros(height,width);
xiCol_dx(:,1) = xiCol(:,1);
xiCol_dx(:,end) = - xiCol(:,end-1);
xiCol_dx(:, 2:end-1) = xiCol(:, 2:(end-1)) - xiCol(:, 1:(end-2));


% output
divXi = xiRow_dy + xiCol_dx;

end











