function [ gtLabels ] = LoadGT( gtPath, gtFileName )
%LOADGT Summary of this function goes here
%   Detailed explanation goes here
% Try to load GT file automatically, if its not present, open filebox

if(~exist([gtPath gtFileName], 'file'))
    
    [gtFileName, gtPath, FilterIndex] = uigetfile({'*_gt.png', 'Groundtruth Image (*_gt.png)'}, 'Select File for Groundtruth', gtPath);

    if(FilterIndex == 0)
        errordlg('GT Could not be loaded.');
        return;
    end

end

gtLabels = imread([gtPath gtFileName]);


end

