function [score] = GetWplusScore(WplusDists)

    if(isempty(WplusDists))
        score = 0;
        return
    end

    WplusScores = zeros(size(WplusDists));

    index1 = (0 <= WplusDists) & (WplusDists < PARAMS.EVAL_A);
    WplusScores(index1) = 1;

    index2 = (PARAMS.EVAL_A <= WplusDists) & (WplusDists < PARAMS.EVAL_B);
    WplusScores(index2) = ...
        1 - 0.5 * ((WplusDists(index2) - PARAMS.EVAL_A)/(PARAMS.EVAL_B - PARAMS.EVAL_A)).^2;

    index3 = (PARAMS.EVAL_B <= WplusDists) & (WplusDists < PARAMS.EVAL_C);
    WplusScores(index3) = ...
        0.5 * ((PARAMS.EVAL_C - WplusDists(index3))/(PARAMS.EVAL_C - PARAMS.EVAL_B)).^2;

    score = sum(WplusScores);

end