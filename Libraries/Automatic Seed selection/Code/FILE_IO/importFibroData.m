function [numfiles patients]=importFibroData(class,strFolder,varargin)
%   IMPORTFIBRODATA Import fibrosis data from strFolder.
%   importFibroData reads roi (.png) images from the folder defined by
%   strFolder and Class. Class = 1 reads data from all roi folders, which
%   contains Liver images. Class = 2 reads data from all roi2 folders, 
%   which contains background images. numfiles returns the total number 
%   of images read. patients contain the read images.
%   Adapted from Dipl. Ing. Jan Marek Marcinczak.


% All patient folders with images.
patientFolders=dir([strFolder '.\pat*']);

%Number of Patients in data folder.
files=1:numel(patientFolders);


for k=1:2:length(varargin)
    switch lower(varargin{k})  
        case 'outliers'
            files = setdiff(files,varargin{k+1});
        otherwise
            error(['Unknown parameter ''' varargin{k} '''.']) ;
    end
end


%create a patients structure with rois of that patient stored in roi {}. 
patients = struct('roi', {});

%Initialize counters.
numfiles=0;
counter=1;

%For each patient, do.
for i=files
    
    fprintf(['Importing Data for patient ' num2str(i) '.\n'])
    
    %Read Liver OR background image by setting the correct path.
    if class == 1
        strRoiPath=[strFolder '.\' patientFolders(i).name '\videos_deinterlaced\distortion_corrected\roi_liver\'];
    else 
        strRoiPath=[strFolder '.\' patientFolders(i).name '\videos_deinterlaced\distortion_corrected\roi_bkg\'];
    end
    
    %All images in this folder.
    roiFiles=dir([strRoiPath '*.png']);
    roi=[];
    roi{numel(roiFiles)}=[];
    
    %For each image in the folder, do.
    for ii=1:numel(roiFiles)
        %Read the image.
        roi{ii}=(imread([strRoiPath roiFiles(ii).name]));
        numfiles=numfiles+1;
    end
    
    %Set the roi for this patient.
    patients(counter).roi=roi;
    %Next patient.
    counter=counter+1;
end

%Total number of files read.
fprintf('%i images loaded.\n',numfiles)