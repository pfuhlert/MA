function [S] = ResizeS(S)


     if((0 < CONST.RESIZE_FACTOR) && (CONST.RESIZE_FACTOR < 1))

         warning(['Using RESIZE_FACTOR = ' num2str(CONST.RESIZE_FACTOR) ' on Scribbles']);
         %% Resize Scribbles
        S(:,[CONST.COL_SROW CONST.COL_SCOL]) = S(:,[CONST.COL_SROW CONST.COL_SCOL]) .* CONST.RESIZE_FACTOR;

        % Duplicates can occur -> remove
        S = unique(round(S), 'rows');

        % Negative entries can occur -> remove
        S = S(S(:,CONST.COL_SCOL) > 0, :);
        S = S(S(:,CONST.COL_SROW) > 0, :);  
     end
end