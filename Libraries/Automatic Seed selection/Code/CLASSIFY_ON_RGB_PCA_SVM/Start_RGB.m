% 

%%%%%%%%%%%%%%%% Clear and Do Setup. %%%%%%%%%%%%%%%%%%%%%

%Cleanup.
clear all;
close all;
clc;


%IO files.
addpath('..\FILE_IO\')
addpath('..\UTIL\')

%Initialize.
loadSettings

%Init VLFEATROOT toolbox.
run('..\..\VLFEATROOT\toolbox\vl_setup')


%%%%%%%%%%%%%%%% Define Global Variables. %%%%%%%%%%%%%%%%%%%%%

%Liver Images.
global patients_liver;
%Bkg Images.
global patients_bkg;

%Count of Liver images.
global numel_liver_images;
%Count of Bkg images.
global numel_bkg_images;

%Which patient (numbered 1..7) is currently the TEST pateint (Leave one patient out approach).  
global test_patient_folder;
%Total no of patients (7 in our setup).
global num_patients;


%%%%%%%%%%%%%%%%%%%%%% Read Data (Image ROIs). %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Read Liver images.
[numel_liver_images, patients_liver] = importFibroData(1,param.datafolder);
%Read background images.
[numel_bkg_images, patients_bkg] = importFibroData(0,param.datafolder);

% Get number of Patients (7).
num_patients = numel(dir([param.datafolder '.\pat*']));



%%%%%%%%%%%%%%%%% Classify on RGB %%%%%%%%%%%%%%%%%%%%%


%Initialize vector to store percision and recall based on PCA dim used.
%Col 1: dim. Col 2: Precision. Col 3: Recall.
pca_dim_based_percision_recall_liver = zeros(25,3);
pca_dim_based_percision_recall_bkg = zeros(25,3);

pca_dim_based_percision_recall_liver(:,1) = 1:25; %dim.
pca_dim_based_percision_recall_bkg(:,1) = 1:25; %dim.

%Counter
cc = 1;

for pca_dim = 1:25
    
    
    
    fprintf('Classifying data on %i PCA Dimensions.\n',pca_dim)
    
    
    %Initialize a variable to store the ground truth + classifier output.
    prediction_set = [];
    %Leave one out, train with remaining. Test on 'this' patient.
    for pat_no = 1:(num_patients)

        %Test on this patient while using everyone else for training.
        test_patient_folder = pat_no;
        
        %Classify using RGB values.
        pred = classify_on_rgb(pca_dim);
   
        
        %Store classification result. When the loop terminates, this matrix
        %will contain ground truth and test results of all images in the
        %data set. Note: prediction_set has only 2 cols.
        prediction_set = [prediction_set ; pred(:,2:3)];
    end


    
    %Create True positive, False positive and False negative metric for
    %class liver.
    rgb_tp_liver = sum(prediction_set((prediction_set(:,2)==1),1) == 1);
    rgb_fp_liver = sum(prediction_set((prediction_set(:,2)==1),1) == 0);
    rgb_fn_liver = sum(prediction_set((prediction_set(:,2)==0),1) == 1);

    RGB_PERCISION_LIVER = rgb_tp_liver/(rgb_tp_liver+rgb_fp_liver);
    RGB_RECALL_LIVER = rgb_tp_liver/(rgb_tp_liver+rgb_fn_liver);

    %Stroe the PERCISION and RECALL metric for this pca_dim.
    pca_dim_based_percision_recall_liver(cc,2) = RGB_PERCISION_LIVER;
    pca_dim_based_percision_recall_liver(cc,3) = RGB_RECALL_LIVER;
    
    
    
    %Create True positive, False positive and False negative metric for
    %class bkg.
    rgb_tp_bkg = sum(prediction_set((prediction_set(:,2)==0),1) == 0);
    rgb_fp_bkg = sum(prediction_set((prediction_set(:,2)==0),1) == 1);
    rgb_fn_bkg = sum(prediction_set((prediction_set(:,2)==1),1) == 0);

    RGB_PERCISION_BKG = rgb_tp_bkg/(rgb_tp_bkg+rgb_fp_bkg);
    RGB_RECALL_BKG = rgb_tp_bkg/(rgb_tp_bkg+rgb_fn_bkg);

    %Stroe the PERCISION and RECALL metric for this pca_dim.
    pca_dim_based_percision_recall_bkg(cc,2) = RGB_PERCISION_BKG;
    pca_dim_based_percision_recall_bkg(cc,3) = RGB_RECALL_BKG;
    
    
    %Increment loop counter.
    cc = cc + 1;
    
end

%%%%%%%%%%%%%%%%%%%% Show Classification Result %%%%%%%%%%%%%%%%%

%Display the results.
pca_dim_based_percision_recall_liver
pca_dim_based_percision_recall_bkg



%Plot bar graph to show effect of dim reduction.
figure(2), bar(pca_dim_based_percision_recall_liver(:,1),pca_dim_based_percision_recall_liver(:,2));
title('Liver:Bar Chart of Percision');
figure(3), bar(pca_dim_based_percision_recall_liver(:,1),pca_dim_based_percision_recall_liver(:,3));
title('Liver:Bar Chart of Recall')

figure(4), bar(pca_dim_based_percision_recall_bkg(:,1),pca_dim_based_percision_recall_bkg(:,2));
title('Bkg:Bar Chart of Percision');
figure(5), bar(pca_dim_based_percision_recall_bkg(:,1),pca_dim_based_percision_recall_bkg(:,3));
title('Bkg:Bar Chart of Recall')