%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function saveToFile(filename, user_name, image_name, labels, seeds)

% create file
fid = fopen(filename, 'w');
if (fid < 0)
    error('unable to open file')
end

% write header
[height, width] = size(labels);
fprintf(fid, 'ICGBENCH GROUNDTRUTH FILE\n');
fprintf(fid, 'USER: %s\n', user_name);
fprintf(fid, 'IMAGE: %s\n', image_name);
fprintf(fid, '#LABELS: %d\n', length(unique(labels)));
fprintf(fid, 'WIDTH: %d\n', width);
fprintf(fid, 'HEIGHT: %d\n', height);

%encode labels and seeds
labels_string = encodeImageRLE(labels);
seeds_string = encodeImageRLE(seeds + 1);

fprintf(fid, 'LABELS: %s\n', labels_string);
fprintf(fid, 'SEEDS: %s\n', seeds_string);

fclose(fid);


