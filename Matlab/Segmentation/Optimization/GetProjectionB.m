function [ theta ] = GetProjectionB(theta)
%PROJECTIONK Backproject value to convex set [0,1] on n-simplex with 
% n = no of Layers

[height, width, n] = size(theta);

% Faster algorithm for 2 regions
theta_projected = zeros(height, width, n);

% A faster approach to operate on matrices was developed for n = 2
if(n == 2)

    theta_sorted = sort(theta, 3);
    
    %Loop1
    t = theta_sorted(:,:,2) - 1;
    Indices1 = (t >= theta_sorted(:,:,1));
    Indices1 = repmat(Indices1, 1,1,2);
    t = repmat(t, 1,1,2);
    
    theta_projected(Indices1) = theta(Indices1) - t(Indices1);
    
    if(~all(all(all(Indices1)))) % if there are still pixels left
    
        %Loop2
        t = (sum(double(theta_sorted), 3) - 1) / 2;
        t = repmat(t, 1,1,2);
        Indices2 = ~Indices1;
        theta_projected(Indices2) = theta(Indices2) - t(Indices2);

    end    
    
    % Result
    theta = max(theta_projected, 0);
    
else %just use the (slower) form for n>2
    for xx = 1:width
        for yy = 1:height
            theta(yy,xx,:) = projsplx(theta(yy,xx,:));
        end
    end
end

% are all theta in sum(dim3) equal to 1?
% or is any theta outside values
if(~all(all(round(sum(theta, 3), 10) == 1)) || ...
    any(any(any(theta < 0 | theta > 1))))
       warning('Projecting theta to simplex went out of bounds (probably rounding issue)');
end

end