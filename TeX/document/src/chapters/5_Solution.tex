\chapter{Application to Laparoscopic Sequences}

\section{Overview}

The following chapter deals with the application of the previously presented
segmentation algorithm to laparoscopic image sequences. At first, the
original algorithm gets a parameter optimization for a sequence of
laparoscopic training images as depicted in \cref{subsec:laparodb}. Also, a
glare point extraction is introduced that allows to automatically detect high
brightness pixels that are extracted in an additional segment. Furthermore,
manual scribble placement is discussed and a final algorithm suggestion is
made.

\section{Parameter Optimization}
\label{sec:paramopti}
As mentioned in \cref{subsec:org_parameters_intro}, the segmentation algorithm
offers several possibilities to alter the segmentation result. For parameter
optimization, the database of $115$ laparoscopic images discussed in
\cref{sec:databases} was divided into a training set of 29 and a test set of 86
images. The training images were chosen manually as the best images of the database for each patient where
the liver was clearly visible. The parameters that
were optimized on the laparoscopic training set are $\lambda$, $\sigma$,
$\alpha$, $\gamma$, $E_\infty$ and $\mathcal{G}_{pd}$. It was chosen to optimize
the parameters independently of each other to avoid extensive computation for a
full parameter sweep of all parameters combined even though possible
correlations of the parameter may exist. This means that all other parameters
were fixed on their original value while one parameter was altered.\\

\noindent
Furthermore, artificial scribbles as introduced in 
\cref{sec:artificial_scribbles} using groundtruth data were used to exclude
user interaction in the parameter sweep process. All parameters were optimized
using the dice score as depicted in \cref{sec:eval_methods}.\\

\noindent
The parameter sweep was done for each parameter independently on all
training images resulting different dice scores for one parameter set. The
arithmetic mean of the score for all images was saved and is shown for
each parameter in the following diagrams. Note that the originally proposed
value as depicted in \cref{tab:org_params} is marked in red.\\

\noindent
Also, the step size parameters $\tau_p$ and $\tau_d$ of the primal-
and dual algorithm are not investigated in this section. Instead they are set
according to \cite{pock2011diagonal}. Additionally, the brush size $B$ is
not taken into account in this analysis, because the segmentation result is highly
correlated to the actual user input that was avoided during the parameter
optimization.

\subsection*{Dataterm Weight}
To start with, the dataterm weight $\lambda$ that acts as the weight parameter
between Dataterm and regularization term as shown in \cref{eq:final_formula} is
investigated. It should accept all values $\lambda \in \mathbb{R}^+$. The larger
$\lambda$, the more influence receives the Dataterm relative to the
regularization term making the total boundary of the overall segmentation less
important. Keep in mind that for values of $\lambda$ close to $0$ the influence of the
dataterm would be totally neglected. Therefore choosing a value close to $0$,
the construction of the dataterm should rather be revisited before basically
neglecting it.\\

\noindent
As shown in \cref{fig:sweep_lambda}, the dice score results for
different $\lambda$ are all in a reasonable range of around $97.5\%$. Thus,
the scores increase up to a value of $97.65\%$ for $\lambda = 0.003$. All
larger values of $\lambda$ result in a decreasing score. This analysis suggests
using a value of $\lambda = 0.003$ for the final algorithm. It should be kept
in mind that a change of $\lambda$ does not influence the overall
segmentation score significantly within a small range.\\

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{.9\textwidth}
    \input{figure/sweep/sweep_lambda.tex}
    \caption{Parameter sweep scores for $\lambda$}
    \label{fig:sweep_lambda}
\end{figure}

\subsection*{Color Kernel Width}

Secondly, the parameter $\sigma$ is responsible for the color kernel width in
the Dataterm calculation in \cref{eq:finalprob_sep_kernels} meaning a larger
value reduces the color influence of the algorithm.\\

\noindent
The parameter sweep illustrated in \cref{fig:sweep_sigma} shows a maximum
overall score at $\sigma = 5$ with almost equal scores in the range
from $2 \leq \sigma \leq 10$. Larger values result in notably
worse scores as well. This leads to the conclusion that the parameter value
should be chosen to $\sigma = 5$ but can also be altered between $2$ and $10$.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_sigma.tex}
    \caption{Parameter sweep scores for $\sigma$}
    \label{fig:sweep_sigma}
\end{figure}

\subsection*{Spatial Kernel Width}

Next, the parameter $\alpha$ is indirectly responsible for the spatial kernel
bandwidth in \cref{eq:rho}. The larger $\alpha$, the more
widespread gets the spatial kernel resulting in less influence of the spatial
distance between a pixel and the scribble group. The parameter sweep leads to a
rather large maximum value of $\alpha = 30$ meaning the spatial influence would
be very low. These results have to be handled with care because they strongly
depend on the scribble placement on the image.\\

\noindent
Due to the fact that this analysis uses artificial scribbles that are randomly
distributed throughout the whole image, the average minimum distance of a scribble is
significantly smaller than for user selected scribbles. This means that the
results for parameter $\alpha$ are not as meaningful as the other
results.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_alpha.tex}
    \caption{Parameter sweep scores for $\alpha$}
    \label{fig:sweep_alpha}
\end{figure}

\subsection*{Gradient Affinity}

Next, the gradient affinity parameter $\gamma$ is inspected. It controls how
strong the final segmentation uses the gradient magnitude in the original
image. \cref{fig:sweep_gamma} shows that the dice score is also nearly the same
for $0.3 \leq \gamma \leq 3$ with the maximum at $\gamma = 0.7$. Significantly
larger values of $\gamma$ that would lead to less influence of the original
image's gradient obtain worse scores. It was also observed that small values of
$\gamma$ resulted in worse qualitative segmentation results to the human eye.
Combining the previously mentioned points, this leads to the conclusion that
$\gamma$ is set to $3$ for the adapted algorithm.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_gamma.tex}
    \caption{Parameter sweep scores for $\gamma$}
    \label{fig:sweep_gamma}
\end{figure}

\subsection*{Scribble Energy}

The scribble energy $E_\infty$ controls the manually set dataterm energy of all
scribbles that do not belong to the current scribble group. As the user 
preselected the pixels, the distinct scribble points should get a
reasonably high energy because the user explicitly stated that these points do
belong to another segment. As a reference of finding the optimal value, the
maximum energy of a pixel that is not preselected as a scribble is obtained
when $P(x\;|\;u(x) \neq i) = 0$. The normalization process would set it to an energy
of $-log(\epsilon) \approx 744$ with $\epsilon \approx \num{5e-324}$ for
the current implementation that provided the sweep scores.\\

\noindent
As shown in \cref{fig:sweep_energy_inf}, the overall score is steadily
increasing and reaches a constant value for $E_\infty > 500$. This can be
explained because for the training set, a value of $E_\infty > 500$ lets no
scribble be relabeled to another segment than initially suggested by the
artificial scribbles. Due to the fact that no artificial scribbles are 
misclassified, all pixels are kept in the segment they belong according to the
groundtruth data. Thus, the parameter sweep leads to the conclusion that a
reasonable large value for $E_\infty$ should be used. This property is fulfilled
with the default value $E_\infty = 1000$ that is also the suggestion for the
adapted solution.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_energy.tex}
    \caption{Parameter sweep scores for $E_\infty$}
    \label{fig:sweep_energy_inf}
\end{figure}

\subsection*{Other Parameters}

Some further modifications can be made to the following influences of the
algorithm. The primal-dual gap size $\mathcal{G}_{pd}$, total number of
selected user scribbles and the resizing of the whole image can be used to
alter the segmentation result and the computation time of the algorithm. 

\subsubsection*{Primal-Dual Gap Size}
The relative change in the primal-dual gap size $\mathcal{G}_{pd}$ per iteration
step determines when the optimization process terminates. The larger its value,
the shorter the runtime of the optimization algorithm. In
\cref{fig:sweep_pd_gap}, the dice score of the training set for different
minimum relative primal-dual gap sizes $\mathcal{G}_{thresh}$ is shown. It can
be seen that the overall dice score only slightly decreases for values of
$\mathcal{G}_{thresh} \leq 0.5\%$. This behaviour is expected, because a
smaller threshold should always lead to a better segmentation result. For larger
values, the segmentation quality starts decreasing rapidly. As a result, the threshold
should be chosen as $0.05\%$ as a compromise between segmentation quality and
algorithm runtime.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_pd_gap.tex}
    \caption{Analysis of $\mathcal{G}_{thresh}$ and dice Score}
    \label{fig:sweep_pd_gap}
\end{figure}

\subsubsection*{Number of Scribbles}
The number of preselected scribbles is very important for the segmentation
results. The more scribbles are selected and classified correctly, the
better the segmentation quality at the cost of a longer algorithm runtime as
depicted in \cref{fig:performance_results_scribbles}.
The number of scribbles influences the segmentation result meaning the more
(artificial) scribbles are used, the better gets the result. This dependency 
is shown in \cref{fig:sweep_scribble_no}. 
\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
    \input{figure/sweep/sweep_scribble_no.tex}
    \caption{Segmentation scores for different scribble amounts}
    \label{fig:sweep_scribble_no}
\end{figure}

\subsubsection*{Resize Factor}
The resize factor can be applied to an image before the segmentation
algorithm starts. It resizes the image width and height as well as the scribble
coordinates to a fraction of the original size to improve the algorithm's
performance by lowering the total number of processed pixels. After the
algorithm finished, the segmentation is resized to the original image's
width and height. The resize factor should be in the interval $(0, 1]$ while a
value outside the interval will be neglected by the implementation meaning the
image size remains unchanged. An qualitative example of a resized segmentation
result compared to the original can be found in \cref{fig:resize_factor_exs}.
It can be recognized that the edges of the resized segmentation are coarser
compared to the original image and tend to misclassify pixels that are close to
the full sized segmentation's edges.\\



\noindent
For the analysis in \cref{fig:sweep_resize_factor}, the 29 training images with
artificial scribbles were used to investigate the segmentation quality
dependent on the resize factor. To get consistent results, the number of
scribble points is also refined for each resize factor because the relative
amount of scribbles in each image should remain the same compared to the total
number of pixels. Otherwise, images with very small sizes would contain up to
100\% scribbles which would have significant impact on the segmentation
result. The mean dice and edge score are shown for different resize factors.
The edge score was included in the analysis, because the qualitative inspection
of the segmentation results suggested problems of resizing on the segmentation's
edges. It can be observed that, for values below $0.2$, the
overall segmentation quality is notably reduced. Resize factors of $0.5$
and above generate overall segmentation results that lead to comparable scores to full
size computation. 

\begin{figure}[htbp]\centering
%###
	\begin{subfigure}[t]{0.32\textwidth}\centering
		 \includegraphics[width=\textwidth]{image/resize_fac_exs/scribbles.png}
		\caption{Image with scribbles}
	\end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}\centering
		 \includegraphics[width=\textwidth]{image/resize_fac_exs/resize_fac_1.png}
		\caption{Resize factor = $1$}
	\end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}\centering
		 \includegraphics[width=\textwidth]{image/resize_fac_exs/resize_fac_0p1.png}
		\caption{Resize factor = $0.1$}
	\end{subfigure}
%###
    \caption{Exemplary segmentation result for different resize factors}
    \label{fig:resize_factor_exs}
\end{figure}

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.8\textwidth}
    \input{figure/sweep/sweep_resize_factor.tex}
    \caption{Dice and edge scores for different resize factors}
    \label{fig:sweep_resize_factor}
\end{figure}

\section{Glare Point Extraction}

Another feature of the overall algorithm is the extraction of glare points. By
considering the whole scope of this project being the 3D reconstruction of
liver surfaces, the extraction of glare points may be advantageous because they
could be managed independently in the reconstruction process. Glare
points occur due to lighting reflections from the camera both on the liver
surface as well as on the background. They can distort the 3D reconstruction result because they do not
represent the correct color nor texture of the liver surface that is
reconstructed. Thus, these pixels should be excluded from the reconstruction
process. This leads to the separate segmentation of glare points. To achieve
this, the algorithm gets expanded by having the ability to introduce an
additional scribble group that contains characteristic glare points. This group
of pixels can then be neglected by the reconstruction algorithm. The glare
points can be characterized by a very high brightness which makes them
separable from the foreground-background segmentation of the liver itself by
using a thresholded hysteresis. The surrounding pixels can be slightly darker
than the middle of a glare point.\\

\noindent
\cref{alg:glarepoints} illustrates the pseudocode that provides automatic glare
point extraction from laparoscopic images. The algorithm uses two brightness
thresholds $T_1$ and $T_2$ where $T_1 > T_2$. After selecting all pixels as
glare points that are brighter than $T_1$, it performs a hysteresis in the direct
neighborhood of those pixels selecting them even though they have a lower
brightness with threshold $T_2$. Finally, select only a fraction of all found
pixels and add them as a new scribble group for the main segmentation
algorithm. Note that this algorithm will overwrite user selected pixels that
are classified as glare points. In the implementation, $T_1$ is set manually to
a brightness level of $240$ while $T_2$ uses a value of $225$. An exemplary
segmentation result using automatic glare point extraction is shown in
\cref{fig:glarepoint_segmentation}.

\begin{algorithm}[htbp]
\caption{Glare Point Extraction}
\label{alg:glarepoints}
\begin{algorithmic}[1]

	\Function{GetGlarepoints}{$Image, T1, T2, K\_MAX$}
	\State $GrayImage \leftarrow$ Grayscale of $Image$
	\State $Image\_T1 \leftarrow GrayImage > T1$;
	\State $GlarePoints = Image\_T1$
	\For{$k = 1, \ldots, K\_MAX$}
		\State Expand selected pixels of Image\_T1 by $k$ pixels
		\State Add all new pixels with brightness $> T2$ to $GlarePoints$
		\If{$GlarePoints$ did not change}
			\State Break 
		\EndIf 
	\EndFor 
	\State Choose only fraction selected $GlarePoints$ as output
	\State\Return $GlarePoints$
	\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Manual Scribble Placement}
\label{sec:manualplacement}
While segmenting a lot of laparoscopic and non-laparoscopic images, several
user scribbles were created with different segmentation results. This
section can be used as a starting guide on how to place scribbles on a new
and unknown image:

\begin{itemize}
  \item The whole color spectrum of the desired segmentation object should be
  		included. The algorithm's color probability estimation can diminish
  		drastically if only a fraction of the segment's color variation was
  		preselected.
  \item The scribbles should focus on regions that lack in color differences but
  		belong to different scribble groups. For laparoscopic images, this means
  		that scribbles should be placed around the image region with the transition
  		from liver to background pixels.
  \item It may be helpful to run the algorithm on only a few scribbles before
  placing new ones in order to see which parts of the image still get
  misclassified.
\end{itemize}
\noindent
As a result, the user should select the whole range of color for each segment
and also apply scribbles to regions where color has little differences between
different desired segments. Additionally, intermediate segmentation results may
help placing additional scribbles on the image.

\begin{figure}[t]\centering
%###
	\begin{subfigure}[t]{0.33\textwidth}\centering
		 \includegraphics[width=\textwidth]{image/glarepoints/scribbles.png}
		\caption{Original image}
	\end{subfigure}
    \begin{subfigure}[t]{0.33\textwidth}\centering
		 \includegraphics[width=\textwidth]{image/glarepoints/glarepoints.png}
		\caption{Segmentation result}
	\end{subfigure}
%###
    \caption{Exemplary segmentation result with glare point extraction}
    \label{fig:glarepoint_segmentation}
\end{figure}

\section{Brushsize}
\label{sec:Adapted_Brushsize}

The brush size determines how many surrounding pixels get selected when the user
starts drawing on the scribble canvas. It should be sufficiently large to cover
color differences of close pixels. However, a large
number of selected pixels leads to very long computation times of the algorithm.
To avoid that, the brush density parameter determines which percentage of pixels
- that were previously selected - get actually used in the segmentation
algorithm as introduced in \cite{Nieuwenhuis.2013c}. The suggestion is to prefer the same
number of scribbles that were obtained by a large brush size with low density
over a small one with higher density. Thus, the implementation arbitrarily sets
the brush size to $15$ with a brush density factor of $0.2$. 

\section{Adapted Algorithm Suggestions}
\begin{table}[!ht]
\centering
\begin{tabular}{|c|c|c|l|}
\hline
Parameter & Original Value & Adapted Value & Description\\
\hline
$\lambda$ 				& $0.008$	&	$0.003$	& Dataterm Weight\\
$\sigma$ 				& $5$		&	$5$ 	& Controls the Color Kernel Width\\
$\alpha$  				& $5$		&	$30$	& Controls the Spatial Kernel Width\\
$\gamma$  				& $5$		&	$3$ 	& Original Image Gradient Influence\\
$E_\infty$  			& $1000$	&	$1000$ 	& Scribble Energy\\
$\mathcal{G}_{thresh}$	& $0.0001$	&	$0.0005$	& Minimum Relative Primal-Dual
Gap\\
$B$			  			& $5$		&	$15$ 	& Brushsize\\
$B_d$  					& $0.5$		&	$0.2$	& Brush Density\\
\hline
\end{tabular}
\caption{Adapted algorithm parameters for laparoscopic images}
\label{tab:adapted_params}
\end{table}

This section concludes the previously mentioned adaptions of the segmentation
algorithm to laparoscopic image sequences. To sum up, \cref{tab:adapted_params}
gives an overview of the parameters that are used in the implementation.\\

\noindent
Furthermore, the number of scribbles and their placement is crucial for a good
segmentation that heavily depends on the actual image meaning no global
suggestion can be made. However, the scribbles should not be placed at one
spot of the image. They rather should spread across large parts of
favorably in regions that are hard to segment by color only. Also, a reasonably
large brush size should be chosen with a corresponding density to limit the number of selected scribbles.\\

\noindent
Moreover, the algorithm offers an optional automatic glare point extraction
that can be used to identify very bright areas in the image that are not useful
for further investigation. This is done by hysteresis thresholding to find all
bright regions in the image. The pixels that were found by the algorithm are
used as a separate scribble group within the segmentation scope.\\