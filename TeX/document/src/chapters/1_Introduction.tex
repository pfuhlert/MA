
\chapter{Introduction}
The master thesis is a part in a project that originated in
collaboration of the Vision Systems Institute of the Hamburg-Harburg University of Technology
and the University Medical Center Hamburg-Eppendorf (UKE). This work contributes
to the overall project on 3D liver reconstruction from
laparoscopic images regarding liver cirrhosis.

\section{Motivation}

Mini-laparoscopy is a technique to investigate the abdomen and giving
opportunity to get visual information of inner organs like the liver. In
contrast to classical laparoscopy, the aim of mini-laparoscopy is to gain
medical information about the health of a patient with a minimal invasive
examination. The mini-laparoscopes related to this work provide monocular
image sequences as exemplary shown in \cref{fig:liver_examples} from the inside
of the abdomen. In the scope of this work, the investigated organ is the liver.\\
\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat001_Video002_00001.png}
		&
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat002_Video001_00051.png}
		&
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat004_Video002_00351.png}
		&	
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat003_Video001_00301.png}
		\\
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat001_Video004_00201.png}
		&
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat002_Video001_00401.png}
		&
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat004_Video004_00001.png}
		&	
		\includegraphics[width=.2\textwidth]{../../../Matlab/Images/UKE/images_crop/pat003_Video001_00351.png}
		\\
	\end{tabular}
		\caption{Mini-laparoscopic liver images}
	\label{fig:liver_examples}
\end{figure}

\noindent
The thesis came up as a part of the project on ``Variation Based Dense 3D
Reconstruction'' which is the PhD topic of M. Sc. Sven Painer
\cite{PAINER2016variation} \cite{MAR}. Due to the high computation effort and
complexity of the 3D reconstruction algorithm, it is favorable to exclude
regions that do not contain useful information from the reconstruction process.
For this purpose, a pixel-wise classification is needed to avoid
unnecessary calculations that do not belong to the liver. Finding a solution to
decide wether a pixel is part of the liver or not is the main target of this
work. For this purpose, the interactive segmentation algorithm
\cite{nieuwenhuis-cremers-pami12_2} is investigated and applied to the
monocular mini-laparoscopic image sequences.\\

\noindent
The main idea of the algorithm is to combine spatial and color information of
interactively preselected pixels, called scribbles, to obtain a powerful
dataterm that allows getting satisfying segmentation results even for varying
color and brightness levels in a single segment. After introducing a regularization term
to keep the number of extracted segments of an image as low as possible, the
segmentation algorithm uses a variation based approach to optimize the
formulated energy by convex relaxation. It was chosen to implement the
algorithm in \matlab because of its image processing capabilities that allow
analysis to understand and inspect the algorithm to adapt it to laparoscopic
image sequences.

\section{Task Description}

This work's main task is the investigation of the image segmentation
algorithm published in \cite{nieuwenhuis-cremers-pami12_2} and to apply it to
laparoscopic image sequences. This publication and \cite{Pock.2009} were the
starting point for further research. The tasks that guided the way through this
work are:

\begin{itemize}
  \item Understand and implement the image segmentation algorithm  
  \cite{nieuwenhuis-cremers-pami12_2}
  \item Generate groundtruth data to analyze the segmentation results
  \item Evaluate the implementation on laparoscopic images without adaption
  of the algorithm
  \item Adapt the implementation considering the previous evaluation results
  \item Perform qualitative and quantitative evaluation of the final algorithm
\end{itemize}

\section{Related Work}
Complex images make it hard to create an algorithm without any human help that
segments the image in a way the user expects it to. Typically, some kind of
user interaction like providing bounding boxes or scribbles is necessary and
needs to be provided to the segmentation algorithm. Despite the approach
presented in this work, other algorithms with interactive input exist such as
random walks \cite{grady2006random} or level set methods
\cite{vese2002multiphase}. This kind of algorithm also was previously applied
on medical images for example in \cite{li2011integrating},
\cite{grady2005random} or \cite{gao1998deformable}.\\

\noindent
Also, an earlier diploma thesis of the Vision Systems Institute at the
Hamburg-Harburg University of Technology uses Graph Cuts to segment
laparoscopic images \cite{Bauch.2012}.\\

\noindent
On top of this, following the publication \cite{nieuwenhuis-cremers-pami12_2} of
their segmentation algorithm, there is also an implementation
\cite{Nieuwenhuis.2013c} written in C++ with the aid of GPU computation
\cite{CUDA}.
