function [ theta ] = LoadSegmentation( segmentationPath, segmentationFile )
%LOADGT Summary of this function goes here
%   Detailed explanation goes here
% Try to load GT file automatically, if its not present, open filebox

if(~exist(segmentationFile, 'file'))
    
    [segmentationFile, segmentationPath] = uigetfile({'*_seg.png', 'Groundtruth Image (*_seg.png)'}, 'Select File for Segmentation', segmentationPath);
    if(segmentationFile == 0) && (segmentationPath == 0)
        warning('Nothing selected. Returning...');
        return
    end    
    if(length(segmentationFile) == 1)
        errordlg('GT Could not be loaded.');
        return;
    end

end

theta = imread([segmentationPath segmentationFile]);

if(length(unique(theta)) == 2)
    theta = logical(theta);
end


end

