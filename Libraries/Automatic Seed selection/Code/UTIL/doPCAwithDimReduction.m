function [W_pca TW_pca] = doPCAwithDimReduction(training_data_vector, testing_data_vector, pca_dimension)
% DOPCAWITHDIMREDUCTION Do PCA and return the first #pca_dimension's.
% doPCAwithDimReduction takes Training-Data, Testing-Data and the
% desired number of dimensions. It performs mean centering according to
% the Training-Data; It also finds the first best #pca_dimensions and then
% projects Training and testing data onto these dimensions.
% W_lda is the Projected Training Data.
% TW_lda is the Projected Test Data.


%Mean centering.
train_image_vector = bsxfun(@minus,training_data_vector, mean(training_data_vector,1));

%Principle component analysis.
[B_pca, W_pca, lambda_pca] = princomp(train_image_vector);

%Get the first #pca_dimension's.
B_pca = B_pca(:,1:pca_dimension);

%Project the training data to these dimensions.
W_pca = W_pca(:,1:pca_dimension);

%Mean center Test Data.
test_image_vector = bsxfun(@minus,testing_data_vector, mean(training_data_vector,1));

%Project the test data to these dimensions.
TW_pca = test_image_vector*B_pca;