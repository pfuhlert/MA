function [ I, uiFileName ] = LoadImage( path, fileName )
%LOADIMAGE Summary of this function goes here
%   Detailed explanation goes here

if(~exist('fileName', 'var') || isempty(fileName))
    
    [uiFileName, uiFilePath, FilterIndex] = uigetfile({'*.jpg;*.png', 'Pictures (*.jpg, *.png)'}, 'Select Image', path);

    if(FilterIndex == 0)
        warning('Image Could not be loaded. Returning...');
        I = [];
        uiFileName = [];
        return;
    end
    
    fileName = [uiFilePath uiFileName];
    
else
    fileName = [path, fileName];
end

if(CONST.DEBUG_OUTPUT)
    disp(['Image "' fileName '" loaded']);
end

% Store Image
I = imread(fileName);

end

