function [ Dataterms ] = GetDataTerms(I, S)
%GETCOLORPROBS Summary of this function goes here
%   I: Image
%   S: Matrix of Scribble Indices : [SGroup SROW SCOL] 

nSGroups = GetNoSGrps(S);

% Just as a note:
% [6] uses additional normalization of each individual scribble group to
% 0,1 instead of only norming by m_i.
[SpaceColorProb] = GetKernels(I, S);

%Exclude 0 and 1 before applying logarithm and make sure to not get values
% > 1
SpaceColorEnergy = (SpaceColorProb + eps(0)) * (1 - eps(0));

%% Energy Conversion
Dataterms = -log(SpaceColorEnergy);

for i=1:nSGroups

    Dataterms_i = Dataterms(:,:,i);
    
    % Set Energy of current Scribbles to 0
    currS = S( S(:,CONST.COL_SGROUP) == i,:);
    Dataterms_i(sub2ind(size(Dataterms_i), currS(:,CONST.COL_SROW), currS(:,CONST.COL_SCOL))) = 0;
    
    % Set Energy of ScribblePoints of other groups to high value defined by
    % ENERGY_INF
    otherS = S( S(:,CONST.COL_SGROUP) ~= i,:);
    Dataterms_i(sub2ind(size(Dataterms_i), otherS(:,CONST.COL_SROW), otherS(:,CONST.COL_SCOL))) = PARAMS.ENERGY_INF;
    
    Dataterms(:,:,i) = Dataterms_i;
end

end