\chapter{Theoretical Background}
\label{ch:theoretics}
The theoretical part is split into a section with general ideas covering
basic components of this work and a successive buildup to the proposed segmentation algorithm.

\section{Basic Concepts}
\label{sec:basics}
The first section of the theoretical chapter hands basic information for the
mathematical and image processing concepts that build up and help to formalize
concepts that are necessary for this work.

\subsection{Image Gradient}
\label{subsec:gradient}
At first, the image gradient is defined. Let $I : \Omega \rightarrow
\mathbb{R}^d $ be an image where $\Omega \subset \mathbb{R}^2$ denotes the
domain of $I$ with height $I_H$ and width $I_W$ and the information contained
by each pixel $I(x) \in \mathbbm{R}^d, \, d \in \mathbb{N}$ with the common
values of $d = 1$ for gray and $d = 3$ for colored images. Then the gradient
\begin{equation}
	\nabla I(x,y) = \left(\frac{\partial I}{\partial x}, \frac{\partial I}{\partial
	y}\right)
\end{equation}
is defined as a combination of the horizontal and vertical image differences.
For a discrete image $I$ with $d = 1$ and using forward differences, the
gradient is defined as
\begin{equation}
	\nabla I(x,y) \coloneqq \left\{
	\begin{array}{ll}
	\left(I(x+1, y) - I(x,y), I(x, y+1) - I(x,y)\right) & \mbox{if } x < I_W, y <
	I_h\\
	\left(0, I(x, y+1) - I(x,y)\right) & \mbox{if } x = I_W, y < I_H\\
	\left(I(x+1, y) - I(x,y), 0\right) & \mbox{if } x < I_W, y = I_H\\
	\left(0,0\right) & \mbox{if } x = I_w, y = I_h\\
	\end{array} \right.
  \label{eq:discr_gradient}
\end{equation}
where all values in the last column of $\frac{\partial I}{\partial x}$ and
the last row of $\frac{\partial I}{\partial y}$ are set to $0$.
Alternative gradient approaches using the Sobel operator \cite{kanopoulos1988design} or the Prewitt
operator \cite{dong2008color} exist but are not considered any further in this
work. Furthermore, a gradient norm providing a scalar value per
pixel representing the gradient magnitude is obtained by 
\begin{equation}
 \lVert \nabla I \rVert \coloneqq \sqrt{\left(\frac{\partial I}{\partial
 x}\right)^2 +\left(\frac{\partial I}{\partial y}\right)^2}\,.
 \label{eq:gradient}
\end{equation}
In addition, the definition is extended for $d > 1$. The square root of
squared sums for each color channel provides the corresponding
directional gradient information. As an example, the horizontal gradient
for an RGB image $I$ results in
\begin{equation}
 \frac{\partial I_{RGB}}{\partial x} \coloneqq \sqrt{\left(\frac{\partial
 I_R}{\partial x}\right)^2 + \left(\frac{\partial I_G}{\partial x}\right)^2 +
 \left(\frac{\partial I_B}{\partial x}\right)^2}
\end{equation}
with the vertical gradient defined accordingly. Alternative implementations
of a color image gradient norm like the conversion to a greylevel image
first and then calculating the gradient are also possible
solutions. If not stated differently, the above equation is used to calculate
the gradient of an image $I$ with $d > 1$.\\

\subsection{Divergence}
\label{subsec:divergence}

In multidimensional vector analysis, the divergence of a $n$-dimensional vector
field $F$ is a scalar field that can contain positive and negative values.
It is defined as the sum of the partial derivatives of $F$ leading to 
\begin{equation}
\divergence F = \sum_{i=1}^n \frac{\partial F}{\partial
x_i}\,.
\label{eq:general-vector-field}
\end{equation}
This work uses the divergence in context of a 2D
discrete gradient meaning $n = 2$ and is defined with backward
differences as
\begin{equation}
	\begin{array}{lll}
	\divergence F(x,y) & = & \left\{
	\begin{array}{lll}
	F^1(x,y) - F^1(x-1, y) & \mbox{if } 1 < x < I_W &\\
	F^1(x,y) & \mbox{if } x = 1\\
	- F^1(x-1, y) & \mbox{if } x = I_W\\
	\end{array} \right.\\
	\\
	& + & \left\{
	\begin{array}{lll}
	F^2(x,y) - F^2(x, y-1) & \mbox{if } 1 < y < I_H\\
	F^2(x,y) & \mbox{if } y = 1\\
	- F^2(x, y-1) & \mbox{if } y = I_H\\
	\end{array} \right.
	\end{array}
  \label{eq:discr_divergence}
\end{equation}
with $F^1$ and $F^2$ being the first and second entry of the corresponding
vector.

\subsection{Segmentation}
\label{subsec:segmentation}
For an image $I : \Omega \rightarrow \mathbbm{R}^d$, a segmentation is a
division of the image domain in $n$ pairwise disjoint regions:
\begin{equation}
 	\Omega \coloneqq {\displaystyle\bigcup_{i=1}^n}\, \Omega_i, \quad \Omega_i
	\cap \Omega_j = \emptyset, \; \forall i \neq j
  \label{eq:segmentation_general}
\end{equation}
\noindent
where the regions are defined as $\Omega_i =\{x\;|\;u(x) = i\}$. The function $u
:\Omega \rightarrow \{1, \ldots, n\}$ is a labeling where every pixel gets
assigned a natural number corresponding to the segmentation region it belongs to.
\cref{fig:general_segmentation} gives a simple example of this definition for $n
= 2$ regions.\\

\begin{figure}[htb]\centering
	\begin{tabular}{cc}
%###
	\begin{subfigure}[t]{0.5\textwidth}\centering
		\input{figure/segmentation/general_segmentation.tikz}
		\caption{Pairwise Disjoint Regions $\Omega_1$ and $\Omega_2$}
	\end{subfigure} &
	\begin{subfigure}[t]{0.5\textwidth}\centering
		\input{figure/segmentation/general_labeling.tikz}
		\caption{Labeling u}
	\end{subfigure}
%###
	\end{tabular}
	\caption{Explained segmentation with labeling}
	\label{fig:general_segmentation}
\end{figure}
\subsection{Simplex}
\label{subsec:simplex}
A k-simplex $S^k$ is a $k$-dimensional polytope defined by $k+1$ affinely
independent vertices $v_0, \ldots, v_k \in \mathbb{R}^{k+1}$
\cite{harzheim1978einfuhrung} resulting in 
\begin{equation}
S^k \coloneqq \left\{(x_1, \ldots, x_{k+1})^T \in \mathbb{R}^{k+1} | \; x = \sum_{i
= 1}^{k+1} \alpha_i \cdot v_i \; | \; \alpha_i \geq 0, \, \sum_{i =
1}^{k+1}
\alpha_i = 1 \right\}\,.
\end{equation}
\noindent
Additionally, a unit k-simplex $\Delta^k$ adds the restriction that all elements
of $v_i$ are the $k+1$ orthonormal unit vectors $\{e_1, e_2, e_3, \ldots, e_k,
e_{k+1}\}$ resulting in 
$$ \Delta^k \coloneqq \left\{(\alpha_1, \ldots, \alpha_{k+1})^T \in
\mathbb{R}^{k+1} | \; \alpha_i \geq 0, \; \sum_{i = 1}^{k+1} \alpha_i = 1
\right\} \,.$$
\noindent
Simplices have several use cases including image processing problems like
\cite{tai2013wavelet} or \cite{bugeau2014variational} where the elements of a
vector $y = (y_1, \ldots, y_{k+1}\}$ should sum up to one, because
the elements of $y$ are interpreted as probabilities for distinct results of
an event: $\sum_{i=1}^k y_k \stackrel{!}{=} 1$.

\subsubsection*{Projection onto a Simplex}
If this equation does not hold, a projection $\Pi$ onto the unit
k-simplex $\Delta^k$ is necessary to keep the vector's probabilistic
interpretation feasible. To do so, the vector 
\begin{equation}
x \coloneqq \Pi_{\Delta^k} (y) = \argmin_{x \in \Delta^k} \lVert x-y \rVert
\label{eq:simplex_projection}
\end{equation}
is defined as the vector that lies on $\Delta^k$, that has the closest
euclidean distance to $y$. A robust numerical implementation of the projection
was proposed in \cite{chen2011projection} with complexity $\mathcal{O}(n\log n)$.

\begin{figure}[t]\centering
%###
    \begin{subfigure}[t]{0.23\textwidth}\centering
        \input{figure/simplex/simplex0_example.tikz}
        \caption{$\Delta^0: Point$}
    \end{subfigure}
    \begin{subfigure}[t]{0.23\textwidth}\centering
        \input{figure/simplex/simplex1_example.tikz}
        \caption{$\Delta^1: Line$}
    \end{subfigure}
    \begin{subfigure}[t]{0.23\textwidth}\centering
        \input{figure/simplex/simplex2_example.tikz}
        \caption{$\Delta^2: Triangle$}
    \end{subfigure}
    \begin{subfigure}[t]{0.23\textwidth}\centering
        \input{figure/simplex/simplex3_example.tikz}
        \caption{$\Delta^3: Tetrahedron$}
    \end{subfigure}
%###
    \caption{Examples of low order unit simplices}
\end{figure}


\subsection{Indicator Function}
\label{subsec:indicator_fct}
An indicator function is a function that indicates if the argument belongs to a
specific set. Consider the function
$$	\mathbbm{1}_I(x) \coloneqq \left\{\begin{array}{ll}
		1 & \mbox{if } x \in I \\
		0 & \mbox{else }
	\end{array} \right.
  \label{eq:indicator_function}
$$
which results in $1$ if $x$ is inside the predefined set $I$ meaning it
indicates the membership of the variable $x$ to $I$.

\subsection{Total and Bounded Variation}
\label{subsec:bounded_total_variation}

The total variation of a  one dimensional real-valued function $f$ on an
interval $I = [a, b] \subset \mathbb{R}$ corresponds to the path length
on the corresponding y-axis in its graphical
representation \cite{encyclopaedia}. If the length along this path is finite,
the function $f$ has a bounded variation on the selected interval $I$.
Let $P$ be any partition $x_1, x_2, \ldots, x_n \in I$ with $x_1 < x_2 < \ldots
< x_n$, then the total variation of $f$ is defined as
\begin{equation}
	TV(f) \coloneqq \sup_P \sum_i \lvert f(x_{i+1}) - f(x_i) \rvert\,.
  \label{eq:tv}
\end{equation}
On top of that, let $f \in C^1_c$ be a differentiable function with compact
support \cite{vladimirov119generalized} that is defined on $\Omega \subset
\mathbb{R}^n$, the total variation of $f$ can be expressed as \cite{evans2015measure}
\begin{equation}
	TV(f, \Omega) \coloneqq \left\{ \int_\Omega f \divergence \xi \; : \; \xi \in
	C_c^\infty(\Omega, \mathbb{R}^n), \; \lVert \xi \rVert \leq 1 \right\}\,.
\end{equation}
If $TV(f, \Omega) < \infty$, then the function $f$ has bounded variation on the
set $\Omega$. The space of all $C^1$ functions with bounded variation is denoted
with $BV(\Omega)$.
\subsection{Kernel Density Estimation}
\label{subsec:kde}
Kernel density estimation is a way to obtain the usually hidden probability
density function $\phi$ of a random variable $X$ by combining kernels derived
from an independent and identically distributed sample set $\{x_1,\ldots,
x_n\}$ of $X$. The method was introduced by Emanuel Parzen
\cite{parzen1962estimation} and is sometimes referred to as ``Parzen Density
Estimator''. To get an estimation $\tilde{\phi}$, the kernels $K_\sigma$ with
bandwidth $\sigma$ are superimposed and normalized to 
\begin{equation}
\tilde{\phi}_\sigma(x) \coloneqq \frac{1}{n} \sum_{i=1}^n K_\sigma(x - x_i)
\label{eq:kde}
\end{equation}
with $K_\sigma(x)$ being any kernel function. A commonly used kernel function
is the Gaussian kernel
\begin{equation}
\mathcal{N}_{\mu; \sigma^2}(x) = \frac{1}{\sqrt{2\pi} \, \sigma}
\; e^{-\frac{(x - \mu)^2}{2\sigma^2}}\,.
\label{eq:gaussian_kernel}
\end{equation}
Alternative kernels that can be used in kernel density estimation are \cite{altman1992introduction}
\begin{itemize}
  \item{\makebox[4cm]{Cauchy Kernel:\hfill} $\mathcal{C}(x) = \frac{1}{\pi(1
  + x^2)}$}
  \item{\makebox[4cm]{Picard Kernel:\hfill} $\mathcal{P}(x) =
  \frac{1}{2}e^{-\lvert t \rvert}$}
\end{itemize}
The estimated probability density function converges to the underlying
probability density function if the bandwidth $\sigma$ of the kernels is chosen
correctly. Usually, finding the correct bandwidth $\sigma$ is more crucial for
a probability estimation than the chosen kernel function 
\cite{nadaraya1965non}.

\begin{figure}[htbp]\centering
    \setlength\figureheight{0.2\textwidth}
    \setlength\figurewidth{0.4\textwidth}
%###
	\begin{tabular}{cc}
		    \input{figure/kde/kde_2.tikz} & \input{figure/kde/kde_4.tikz}\\
	        (a) $n = 2$ & (b) $n = 4$ \\
		    \input{figure/kde/kde_10.tikz} & \input{figure/kde/kde_20.tikz}\\
	        (c) $n = 10$ & (d) $n = 20$ \\
		    \input{figure/kde/kde_100.tikz} &  \input{figure/kde/kde_1000.tikz}\\
	        (d) $n = 100$ & (e) $n = 1000$ \\
	        \\
	    	\multicolumn{2}{c}{\input{figure/kde/legend.tex}}\\ 
	    	%\multicolumn{2}{c}{(f) Legend}\\ 
    \end{tabular}
%###
    \caption{Kernel density estimation for different $n$}
    \label{fig:kde}
\end{figure}

\subsubsection*{Example}
\cref{fig:kde} presents the idea of a kernel density estimator for different
sample set sizes $n$. The (usually hidden) black probability density function
$\phi = \frac{1}{3} \left(\mathcal{N}_{5;1} + 2\cdot\mathcal{N}_{10;1}\right)$ of the
random variable $X$ is to be estimated. A random sample $\{x_1, \ldots,
x_n\}$ of $X$ is taken and the normalized Gaussian kernel functions
with $\mu_i = x_i$ are superimposed as shown in \cref{eq:kde}. Here,
$\sigma=1$ is chosen arbitrarily. The estimated density function $\tilde{\phi}$ is obtained  by
superposition of the normalized kernel functions. The more samples are drawn
from the random variable, the closer is the probability estimator $\tilde{\phi}$
to the underlying probability density function $\phi$.
\subsection{Variational Methods}
\label{subsec:variational_method}
Variational methods are used in multidimensional analysis to optimize
a functional for a certain set. A functional is a mapping from a
vector space into a real or complex scalar field. Typically,
the vector space is a space of functions that are restricted in a certain
way \cite{kolmogorov1957elements}.\\

\noindent
Variational methods can be used to find solutions for complex problems. A simple
example problem is the shortest connection of two points in three dimensional
space which has the trivial solution of a straight line. However, by introducing
constraints to the connection for example by restricting the path to be on a
certain surface, the solution becomes non-trivial \cite{bolza2013vorlesungen}.\\

\noindent
The image segmentation algorithm presented in this work uses a variational
formulation for the energy term to determine the optimal segmentation
result for specific restrictions to the functional.
\subsubsection*{Gradient Descent}
\label{subsubsec:gradient_descent}
To solve a problem that was formed using the previously mentioned approach, it
is common to use numerical approximations to find near-optimal solutions if no
analytical solution can be found. A possible approach to solve the problem is to
use the gradient descent method. It is a first order numerical method to obtain
a local minimum to a $n$-dimensional differentiable function $f$. This
iterative approach uses an arbitrary initialization point $x_0$ and calculates
the successive $x_{i+1}$ by following the steepest descent of $f$ 
at the current point $x_i$\cite{ortega1970iterative}. The following point is
obtained as 
\begin{equation}
x_{i+1} = x_{i} - \tau \cdot \nabla f(x_{i})\,.
\end{equation}
By choosing any initial point $x_0$, the algorithm leads to a better
approximation of a local minimum in each iteration step, if the step size
$\tau$ is chosen in a reasonable range \cite{kantorovich1964funktionalanalysis}.
However, it is usually desired to find the absolute minimum to a given problem
which cannot be guaranteed by this algorithm.
\subsubsection*{Convex Problems}
If an optimal solution should be found for a function using a numerical
approach, the problem can be converted into a convex problem to ensure that
the approximated solution is close to the global minimum. The
optimization of a convex problem is more favorable than optimizing a non-convex
problem using the aforementioned approach, because the result does not depend
on the initialization of the variables. All convex problems only have one
optimum. This optimum can also be located on a plateau meaning it
can still lead to multiple solutions.
\subsubsection*{Convex Relaxation}
Convex relaxation is a method to convert any given mathematical problem into a
convex problem by replacing its set of variables by their convex hull as well
as replacing the non-convex functions with their convex closure
\cite{preparata2012computational}.\\

\noindent
The resulting solution of a convex problem yields an optimization that
does not need to be equal to the solution of the original problem. This
means that after solving the generalized problem, a backprojection to the
original variable set is necessary to obtain a near-optimal solution to
the original problem \cite{bertsekas2003convex}.
\section{Segmentation Algorithm}
\label{sec:segmentation-algorithm}

After the explanation of all basic mathematical concepts, the following pages
deal with the general idea and further explanations of the segmentation
algorithm that is used in this thesis.

\subsection{Overview}
\label{sec:segmentation-algorithm:overview}

The following section restates the most important ideas of the
segmentation algorithm \cite{nieuwenhuis-cremers-pami12_2}. It is characterized
by the idea that not only the color of user selected pixels of
an image, called scribbles, are used in the creation of a dataterm, but also
their location. The algorithm defines a powerful dataterm for pixel-wise
estimation of every probability to belong to a certain segmentation region. The
overall diagram of the segmentation algorithm approach is shown in
\cref{img:algorithmOverview}.\\

\noindent
Additionally the algorithm uses a regularization term to limit the total
boundary length of the computed segmentation result. It also uses a variational
approach to find solutions for the segmentation problem. Moreover, the problem
is restated to obtain a convex energy minimization problem that gets solved by the
primal-dual algorithm introduced in \cite{Pock.2009}. This section reproduces
the most important ideas and equations of the segmentation algorithm. This work
adopts the notation of the original publication of the algorithm as stated in
\cite{nieuwenhuis-cremers-pami12_2}.

\begin{figure}[htb]
\centering
%###
\input{figure/org_alg/overview.tex}
%###
\caption{General segmentation algorithm overview}
\label{img:algorithmOverview}
\end{figure}
\subsection{Scribbles}
The algorithm requires user interaction meaning some points of the image need to
be classified by hand in order to use the segmentation algorithm. The user
selected points are called scribbles. In other related sources, they may also be
referred to as seeds. In the context of this work, a scribble combines
information for color and space from a user selected pixel as well as its
corresponding segment. Let 
\begin{equation}
S_{i} \coloneqq \left\{\colvec{x_{ij};I_{ij}},\;
j = 1, \ldots, m_i\right\}
\end{equation}
be a scribble group with $m_i$ being the total number of scribbles for a
segment $\Omega_i = \{x \condprob u(x) = i\}$ as well as corresponding pixel
locations $ {x}_{ij}$ and colors ${I}_{ij}$ for $j = 1, \ldots, m_i$. Also, let
$s_{ij} \in S_i$ denote a particular scribble of this group.
\subsubsection*{Dataterm}
The following section rebuilds the data fidelity term that is used by the
segmentation algorithm starting with the basic approach that combines color
and spatial information of user selected scribbles. A main approach of
\cite{nieuwenhuis-cremers-pami12_2} is to express the segmentation problem as
the maximization of the joint probability 
\begin{equation}
 \tilde{\mathcal{P}}(I(x),\,x\;|\;u(x) = i) = \frac{1}{m_i}
 \sum_{j=1}^{m_i} K_\sigma \colvec{x - x_{ij};I - I_{ij}}
  \label{eq:condprobPDF}
\end{equation}
of finding the color $I(x)$ combined
with its location $x$ for a labeling $u$. This can be simplified to the
normalized sum of weighted kernels at the centers of the color and spatial scribble locations as shown in
\cref{subsec:kde}. Furthermore $K$ can be defined as a multiplication of the
color and the spatial probability estimation $k_\sigma$ and $k_\rho$ with varying bandwidths $\sigma$
and $\rho$ for separable kernels resulting in 
\begin{equation}
 \tilde{\mathcal{P}}(I(x),\,x\;|\;u(x) = i) = \frac{1}{m_i}
 \sum_{j=1}^{m_i} K_{\rho_i}(x - x_{ij}) \cdot K_\sigma(I - I_{ij})\,,
 \label{eq:finalprob_sep_kernels}
\end{equation}
which leads to the final dataterm 
\begin{equation}
	f_i = -\log \tilde{\mathcal{P}}(I(x),\,x\;|\;u(x) = i)
	\label{eq:dataterm}
\end{equation}
by taking the negative natural logarithm of
the estimated probabilities. In the
original algorithm proposal, Gaussian kernels are used as well as in this
implementation.\\
Moreover, the standard deviation $\rho_i$ of the spatial kernel $K_{\rho_i}(x -
x_{ij})$ varies for every pixel according to its minimum distance to the
current scribble group $S_i$ as
\begin{equation}
\rho_i = \alpha \min_{s_{ij} \in S_i} \lVert x - x_{s_{ij}}\rVert\,,
\label{eq:rho}
\end{equation}
where $x_{s_{ij}}$ denotes the location of a scribble point and $\alpha$ is a
parameter controlling the spatial kernel width.
This tolerates pixels with a higher spatial distance to the selected scribbles to have a higher
color variation. In other words, a pixel that is further away from the scribble
group can have a larger color distance to obtain the same probability
estimation as a closer pixel with a more similar color.
\subsection{Regularization Term}
The previously discussed dataterm leads to an estimation for
every pixel to belong to a segmentation $\Omega_i$, but it still does not give
satisfying results if the labeling $u$ is chosen by simply comparing the
probability estimations for each pixel which is shown in
\cref{fig:only_dataterm} with images from the database introduced in
\cref{subsec:icg}. Images from this database will occur in several exemplary
figures throughout the thesis. The first column shows the used images with an
overlay of the user input scribbles for two regions. Red indicates the foreground and blue the
background region. The segmentation results show many misclassified pixels
resulting in a very large number of unconnected small regions for a specific
segment. This happens because of noise or insufficient illumination in the
picture, but those small regions may also have a larger probability to belong
to a different region even though the user scribbles had a different intention. To overcome the large number of unconnected regions, a
regularization term is introduced. It is used to favor labeling solutions that
have a shorter overall boundary length which is achieved if the small regions
disappear. This is calculated by
\begin{equation}\label{eq:std_regularization}
	\mathcal{R}(\Omega) = \sum_{i=1}^n Per_g(\Omega_i)
\end{equation}
where $Per_g(\cdot)$ calculates the perimeter of the segmentation with
additional use of a weighting function $ g(x) = \exp(-\gamma \lVert \nabla I(x)
\rVert)$ that favors segmentation edges to correspond to image edges. 
\begin{figure}[htb]\centering
	\begin{tabular}{cc}
	\includegraphics[width=.33\textwidth]{image/ProbabilityThresh/stier_scrib.png}
	& \includegraphics[width=.33\textwidth]{image/ProbabilityThresh/stier.png}\\
	\includegraphics[width=.33\textwidth]{image/ProbabilityThresh/elephant_scrib.png}
	& \includegraphics[width=.33\textwidth]{image/ProbabilityThresh/elephant.png}\\
	(a) Original Image with Scribbles & (b) Probabilities
	$\hat{\mathcal{P}}_1 > \hat{\mathcal{P}}_2$\\

	\end{tabular}
		\caption{Exemplary thresholded probabilities from dataterm}
	\label{fig:only_dataterm}
\end{figure}
\subsubsection*{Variational Expression}
To use a variational method as shown in \cref{subsec:variational_method}, the
previously obtained \cref{eq:std_regularization} needs to be reformulated.
For this purpose, the regularization term gets rephrased. Let $\theta \in
BV(\Omega, \{0, 1\})^n$ be an indicator function as shown in
\cref{subsec:indicator_fct} defined as 
\begin{equation}
	\text{with }\, \theta_i(x) = \left\{
	\begin{array}{ll}
		1 & \mbox{if } x \in \Omega_i \\
		0 & \mbox{else }
	\end{array} \right.\,,
\end{equation}
then the regularization term can be restated by interpreting the perimeter of
the binary set using total variation as depicted in
\cref{subsec:bounded_total_variation} and the coarea formula
\cite{federer1969geometric} to reveal 
\begin{equation}\label{eq:reformulation_regularizer}
		\sum_{i=1}^n Per_g(\Omega_i) = \sum_{i=1}^n TV_g ( \Omega_i ) = 2 \sup_{\xi
		\in \mathcal{K}_g} \left( - \sum_{i=1}^n \int_\Omega \theta_i \divergence \xi_i \dx \right)\\
\end{equation}
with $\mathcal{K}_g = \left\{ \xi \in C_c^1(\Omega,\left.
\mathbb{R}^2)^n \right| \lVert \xi_i \rVert \leq \frac{g(x)}{2},\; x \in
\Omega, \; i = 1, \ldots, n \right\}$. When these prerequisites are put
together, the energy of any image $I$ with a $n$ region segmentation $\Omega_i,
\; i = 1, \ldots, n\,$ is stated as 
\begin{equation}
    \mathcal{E}(\Omega) = \lambda \sum_{i=1}^{n} \int_{\Omega_i} f_i\,\dx
    - \int_\Omega \theta_i \; \divergence \xi_i \dx\,.
\label{eq:final_formula_no_minimization}
\end{equation}
\noindent
where $\; \mathcal{B} = \left\{ \theta \in  \BV( \Omega, \{0,1\})^n \;
\left| \; \sum_{i=1}^n \right. \theta_i = 1 \right\} $.
\subsubsection*{Convex Problem Conversion}
To calculate the minimum of the above energy that corresponds to the best
segmentation result, the problem is solved using a variational approach on a
convex relaxed problem as shown in \cref{subsec:variational_method}. The energy
in \cref{eq:final_formula_no_minimization} is relaxed in $\mathcal{B}$ using convex relaxation
to obtain the final minimization formula
\begin{equation}
    \min_{\theta \in \mathcal{B}}\,\mathcal{E}(\theta) =
    \min_{\theta \in \mathcal{B}} \; \sup_{\xi \in \mathcal{K}_g} \left\{
    \sum_{i=1}^{n} \lambda \int_{\Omega} \theta_i f_i \dx - \int_\Omega
    \theta_i\, \divergence \xi_i \dx \right\}
\label{eq:final_formula}
\end{equation}
with $\mathcal{\tilde{B}} = \left\{ \theta \in  \BV( \Omega,
[0,1])^n \; \left| \; \sum_{i=1}^n \right. \theta_i = 1 \right\}$ being the
relaxed set for the variable $\theta$ and $\;\mathcal{K}_g = \left\{ \xi \in
C_c^1(\Omega, \left.
\mathbb{R}^2)^n \right| \lVert \xi_i \rVert \leq \frac{g(x)}{2},\; x \in \Omega, \; i = 1, \ldots, n
\right\}$. Note the transition from $\mathcal{B}$ to $\tilde{\mathcal{B}}$
meaning $\theta$ for a specific pixel can now accept values in $[0,1]$ instead
of only binary values $\{0, 1\}$. Also, $\xi$ is a differentiable function
with compact support \cite{vladimirov119generalized} used to calculate the
perimeter of every region $\Omega_i$.
\subsubsection*{Primal-Dual-Algorithm}
\label{par:primal_dual}
After obtaining the final energy formula, the algorithm suggests a numerical
implementation featuring the Primal-Dual-Algorithm as indicated in
\cite{Pock.2009}. The algorithm has a convergence rate of at least
$\mathcal{O}(\frac{1}{n})$ . It uses a projected gradient descent on the primal
variable $\theta$ and a projected gradient ascent on the dual variable $\xi$
as
\begin{subequations}
\begin{alignat}{5}\label{eq:primdual}
 & \xi^{t+1}			= \Pi_{\mathcal{K}_g} &&\left( \xi^t + \tau_d \frac{\partial
 \mathcal{E}}{\partial \xi} \right) &&= \Pi_{\mathcal{K}_g} (\xi^t + \tau_d
 \nabla \bar{\theta}^t)\\
 & \theta^{t+1}			= \Pi_{\tilde{\mathcal{B}}} &&\left( \theta^t - \tau_p
 \frac{\partial \mathcal{E}}{\partial \theta} \right)  &&=
 \Pi_{\tilde{\mathcal{B}}} \left( \theta^t + \tau_p (\divergence \xi^{t+1} -
 \lambda f) \right) \\
 & \bar{\theta}^{t+1} = && 2\,\theta^{t+1} - \theta^t \label{eq:overrelax} &
 \end{alignat}
\end{subequations}
with the projections
\begin{subequations}
\begin{alignat}{2}
	\Pi_{\mathcal{K}_g}(x) &= \frac{x}{\max(g(x)/2, \lVert x \rVert)},\; x \in
	\mathbb{R}^2\\
	\Pi_{\tilde{\mathcal{B}}}(x) &=  \argmin_{y \in \Delta^{n-1}} \lVert y-x
	\rVert
 \end{alignat}
\end{subequations}
\noindent
where the first equation projects $x$ onto a circle by clipping the vector if it
is larger than the radius $r = g(x) / 2$. The second more complex projection
corresponds to the projection onto a simplex that is explained in
\cref{subsec:simplex}. Furthermore, \cref{eq:overrelax} introduces an
over-relaxation step of the iteration result $\theta^{t+1}$. This means that the
primal variable $\theta^{t+1}$ is extrapolated for faster convergence
\cite{pock2011diagonal}.\\

\noindent
After the iteration algorithm terminates, the solution $\theta \in
\tilde{\mathcal{B}}$ has to get backprojected to the non-convex original set
$\mathcal{B}$. This is done by thresholding every entry of $\theta_i$ by
choosing the maximum value $\argmax_i \theta_i$. The backprojection is optimal
for $n = 2$ regions \cite{chan2006algorithms} and nearly optimal for $n > 2$
\cite{nieuwenhuis-cremers-pami12_2}.

\subsubsection*{Convergence Criterion}
\label{par:convergence_crit}
The iteration process is terminated if the relative change in the primal-dual
gap falls below some threshold. The primal-dual gap in the $t$-th step defined
as
\begin{subequations}\label{eq:primal-dual-gap}
\begin{alignat}{2}
	\mathcal{G}_{pd}^t & \coloneqq \mathcal{E}_p^t - \mathcal{E}_d^t
	\tag{\ref{eq:primal-dual-gap}}\\
	\mathcal{E}_p & \coloneqq \sum_{i=1}^n
	\int_\Omega \lambda \theta_i f_i \dx + g(x) \lVert \nabla \theta_i \rVert\\
	\mathcal{E}_d & \coloneqq \int_\Omega \min_{i}(\lambda f_i - \divergence
	\xi_i)\dx
 \end{alignat}
\end{subequations}
is the energy difference between primal and dual energy in the $t$-th
optimization step.\\

\noindent
It is worth mentioning that on the one hand, the primal energy $\mathcal{E}_p$
is minimized using the gradient descent for the variable $\theta$. Therefore the
value of $\mathcal{E}_p$ is decreasing for every iteration step. On the other
hand, the dual variable $\xi$ is maximized using the gradient ascent for the
dual energy $\mathcal{E}_d$ which is constantly increasing as shown in 
\cref{im:primdualgap}. To get a criterion for iteration abortion, the relative
change in the primal dual gap is considered. If the change in the primal dual
gap going from step $t$ to step $t+1$ is less than a specified threshold
$\mathcal{G}_{thresh}$, the optimization loop should exit if
$$ \mathcal{G}_{pd}^{t} (1 - \mathcal{G}_{thresh}) < \mathcal{G}_{pd}^{t+1}\,.$$
Other possible convergence criteria like the relative change of the primal
energy $\mathcal{E}_p$ itself or a fixed minimum primal-dual gap size can also
be used. This work implements the previously mentioned method of primal-dual
gap size change.

\begin{figure}[htb]\centering
%###
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
  \input{figure/convergence/primaldual1.tex}
%###
  \caption{Typical Convergence of Primal and Dual Energy}
  \label{im:primdualgap}
\end{figure}

\subsection{Parameters}
\label{subsec:org_parameters_intro}
The following parameters can significantly alter
the final segmentation quality or the runtime of the algorithm. The next
pages describe the meaning of each mentioned parameter in detail and give
examples on their impact to the segmentation result. Firstly, the
proposed parameter values from the author's implementation
\cite{Nieuwenhuis.2013c} are shown in \cref{tab:org_params}. Note that $\lambda$
is defined differently in the implementation leading to a different parameter value.
Instead, the proposed value of $\lambda$ is adopted from the original
publication \cite{nieuwenhuis-cremers-pami12_2}.

\begin{table}[htb]
\centering
\begin{tabular}{|c|c|l|}
\hline
Parameter & Proposed Value & Description\\
\hline
$\lambda$ 	& 	0.008 	& Dataterm Weight\\
$\sigma$ 		& 	5 		& Controls the Color Kernel Width\\
$\alpha$  	& 	5 		& Controls the Spatial Kernel Width\\
$\gamma$  	& 	5 		& Original Image Gradient Influence\\
$E_\infty$  & 	1000 	& Scribble Energy\\
$\mathcal{G}_{thresh}$ & 0.0001 & Iteration Abortion Criterion\\
$B$  		& 	5 		& Brushsize\\
\hline
\end{tabular}
\caption{Original Algorithm Parameters}
\label{tab:org_params}
\end{table}

\subsubsection{Dataterm Weight}

$\lambda$ controls the relative weight of the dataterm compared to the
regularization Term. The larger $\lambda$, the more influence
gets the dataterm on the final segmentation. Due to the fact that the
regularizer punishes the overall perimeter length of all segments combined, a
larger value of $\lambda$ makes the final segmentation have a longer boundary
and a higher influence of the dataterm. \cref{fig:params_lamba_exs} shows different segmentation results
when altering the dataterm weight parameter $\lambda$.

\begin{figure}[htb]\centering

%###
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_lambda_exs/obst.png}
        \caption{Original Image with Scribbles}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_lambda_exs/0p8}
        \caption{$\lambda$ = 0.8}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_lambda_exs/0p008}
        \caption{$\lambda$ = 0.008}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_lambda_exs/0p00008}
        \caption{$\lambda$ = 0.00008}
    \end{subfigure}
%###

  \caption{Segmentation Results for different $\lambda$}
  \label{fig:params_lamba_exs}

\end{figure}

\subsubsection{Color Kernel Width}
The parameter $\sigma$ influences the width of the color kernel function as a
part of the dataterm construction. The smaller $\sigma$, the
narrower the color estimator meaning only very similar colors receive
high probabilities for the color estimation. \cref{fig:params_sigma_exs} gives
a qualitative impression of the color kernel width by showing combined spatial
and color probability for different $\sigma$. The brighter a pixel is drawn, the
higher the probability to belong to the presented scribble group. Setting $\sigma$
to a very large value removes the color influence completely. This would make
the dataterm only dependent on the spatial distance of a pixel to the scribble
group.

\begin{figure}[!htb]\centering

%###
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_sigma_exs/scrib.png}
        \caption{Original Image with Scribbles}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_sigma_exs/sigma_5.png}
        \caption{Estimated Probability with $\sigma = 5$}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_sigma_exs/sigma_20.png}
        \caption{Estimated Probability with $\sigma = 20$}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_sigma_exs/sigma_50.png}
        \caption{Estimated Probability with $\sigma = 50$}
    \end{subfigure}
%###

  \caption{Exemplary Probability Estimation for different
  $\sigma$}
  \label{fig:params_sigma_exs}

\end{figure}



\subsubsection{Spatial Kernel Width}
Following up on $\sigma$ for the color kernel, the parameter $\alpha$ is
responsible for the spatial kernel width. The smaller the pixel's minimum
distance to the nearest scribble group, the smaller the standard deviation
$\rho_i$ for this particular pixel. $\alpha$ scales the distance to obtain the
value for the standard deviation. The smaller the value of $\alpha$, the
narrower the spatial kernel meaning only pixels that are very close to a
scribble get high probability estimations.

\subsubsection{Gradient Affinity}
The parameter $\gamma$ lets the segmentation solution favor image edges that are
calculated using the gradient as shown in \cref{eq:std_regularization}. The
smaller the parameter $\gamma$, the more important is the original image
gradient's magnitude to the segmentation result. \cref{fig:gamma_2d} shows
$g(x)$ for different $\gamma$. Note that the final segmentation result will
favor regions with a low value of $g(x)$. \cref{fig:params_gamma_exs}
visualizes the influence of the parameter where dark regions of the image are
preferred in the optimization algorithm.
\begin{figure}[htb]\centering
    \setlength\figureheight{0.25\textwidth}
    \setlength\figurewidth{0.9\textwidth}
%###
	    \input{image/param_gamma_exs/getg.tex}
%###
        \caption{Function $g(x)$ for different $\gamma$}
        \label{fig:gamma_2d}
\end{figure}
\begin{figure}[htb]\centering

%###
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_gamma_exs/truck.png}
        \caption{Original Image}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_gamma_exs/gamma_1.png}
        \caption{$\gamma$ = 1}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_gamma_exs/gamma_5.png}
        \caption{$\gamma$ = 5}
    \end{subfigure}
    \begin{subfigure}[t]{.24\textwidth}\centering
        \includegraphics[width=\textwidth]{image/param_gamma_exs/gamma_20.png}
        \caption{$\gamma$ = 20}
    \end{subfigure}
%###
  \caption{Exemplary $g(x)$ for different $\gamma$}
  \label{fig:params_gamma_exs}

\end{figure}
\subsubsection{Scribble Energy}

The energy of user selected scribbles in the dataterm are set manually after
the probability estimation. It should have small values for scribbles that
belong to the current segment $\Omega_i$ and very high values for scribbles
that were classified to another segment $\Omega_j$ to make it less
likely for a user selected scribble to get reclassified in a different segment.
This means that a smaller scribble energy lets scribbles be relabeled more often. If not
mentioned differently, this work chooses to set the dataterm energy of
scribbles that belong to the current group to $0$ and scribbles of any other
group to $E_\infty = 1000$.

\subsubsection{Brush Size}
\label{subsubsec:brushsize}
If the user would select on a pixel-by-pixel scale, it is
hard to capture all of the color variety of the region. To simplify this
process, a number of surrounding pixels also get selected automatically along
with the initially selected user pixel. The number of neighboring pixels is
controlled by the brush size $B$. This way more pixels get chosen which makes
it more likely to cover all color variations of a segment. Also, a larger brush
size reduces the performance of the algorithm leading to a trade off
between these two components. To overcome this issue, not all pixels that are
selected in the previous step actually get used as scribble input pixels. A
density parameter controls the fraction of previously selected pixels that is
chosen as an input to the segmentation algorithm.

\subsubsection{Stepsizes of Primal-Dual Algorithm}
The parameters $\tau_p$ and $\tau_d$ define the gradient descent step sizes of
the primal-dual algorithm . Just like simple gradient descent approaches, small values of
$\tau$ lead to slower convergence while too large values can lead to divergence. The values
$\tau_p = 0.25$ and $\tau_d = 0.5$ are chosen according to
\cite{pock2011diagonal}.
