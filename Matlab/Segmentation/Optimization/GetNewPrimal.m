function [ theta_np1 ] = GetNewPrimal( theta_n, xi_np1, f )
%GETNEWPRIMAL Summary of this function goes here
%   Detailed explanation goes here

[height, width, n] = size(f);

theta_np1 = zeros(height, width, n);
for i = 1:n
    theta_np1(:,:,i) = theta_n(:,:,i) + PARAMS.TAU_P .* (GetDivergence(xi_np1, i) - PARAMS.LAMBDA .* f(:,:,i));
end
    
% ProjectionB
theta_np1 = GetProjectionB(theta_np1);

end

