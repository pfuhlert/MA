addpath(genpath('../'))
close all
clear
clc

%Interval
Interval = [-5:0.1:20];

%randn('seed',1) % use for reproducibility
data=[2*randn(1000,1)+5;randn(1000,1)+10]; % normal mixture with two humps

% Samples
N = 2;
X=[2*randn(N/2,1)+5;randn(N/2,1)+10]; % normal mixture with two humps


%Functions
phi=@(x)(exp(-.5*x.^2)/sqrt(2*pi)); % normal pdf
ksden=@(x)mean(phi((x-data)/1)/1); % kernel density 

h = figure;
fplot(ksden,[min(Interval),max(Interval)],'k-'); % plot kernel density with rule of thumb 
hold on
plotBlack = findobj('Type', 'line');
plotBlack.LineWidth = 1.5;

myFun = zeros(1, length(Interval));
for i=1:length(X)
    ksden2=@(x)mean(phi(x-X(i))/length(X)); % kernel density       
    
    plotBlue = plot(Interval, phi(Interval-X(i))/length(X), 'b-.');
    
    tt = phi((Interval-X(i))/1)/length(X);
    myFun = myFun + tt;
end
plotRed = plot(Interval, myFun, 'r-','LineWidth',1.5);
scatter(X, zeros(length(X), 1), 300, 'bx')
leg = legend([plotBlack plotRed plotBlue], '$\phi$', '$\tilde{\phi}$', '$\frac{1}{n} \mathcal{N}_1(x)$', ...
    'Orientation', 'horizontal');
set(leg,'Interpreter','Latex');
set(gca,'XTickLabel','')
set(gca,'YTickLabel','')

%matlab2tikz([CONST.TEXFIGUREPATH '/kde/' 'kde_' num2str(N) '.tikz'], 'height', '\figureheight', 'width', '\figurewidth', 'showInfo', false);