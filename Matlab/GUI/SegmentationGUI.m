function varargout = SegmentationGUI(varargin)
% SegmentationGUI MATLAB code for SegmentationGUI.fig
%      SegmentationGUI, by itself, creates a new SegmentationGUI or raises the existing
%      singleton*.
%
%      H = SegmentationGUI returns the handle to a new SegmentationGUI or the handle to
%      the existing singleton*.
%
%      SegmentationGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SegmentationGUI.M with the given input arguments.
%
%      SegmentationGUI('Property','Value',...) creates a new SegmentationGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SegmentationGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SegmentationGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SegmentationGUI

% Last Modified by GUIDE v2.5 07-Jul-2016 12:43:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SegmentationGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SegmentationGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before SegmentationGUI is made visible.
function SegmentationGUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SegmentationGUI (see VARARGIN)

%Get Global handler for Program
global GUIhandle;
GUIhandle = hObject;

% Set Image directory
global IMAGE_DIR
%IMAGE_DIR = '../../Libraries/icgbench/images/';
IMAGE_DIR = '../Images/UKE/images_crop/';
%IMAGE_DIR = '../Images/';

% Close all open figures
close_open_figures();

% Add evrything to path from parent folder 
addpath(genpath('../')) 

% Choose default command line output for SegmentationGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Delete previously defined global variables
clearvars -global S imageName I gtLabels theta;

close_open_figures();

% --- Outputs from this function are returned to the command line.
function varargout = SegmentationGUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in btn_clear_all.
function btn_clear_all_Callback(~, ~, handles)
% hObject    handle to btn_clear_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Clear loaded Image, Scribbles and GTData
clearvars -global S imageName I gtLabels theta;

close_open_figures();

% Clear axis in GUI
imshow([], 'Parent', handles.axes_orig);
imshow([], 'Parent', handles.axes_gt);
imshow([], 'Parent', handles.axes_scrib);
imshow([], 'Parent', handles.axes_segment);


clear;
clc;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject,'WindowStyle','normal')

% --- Executes on button press in btn_scrib_save.
function btn_scrib_save_Callback(hObject, eventdata, handles)
% hObject    handle to btn_scrib_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global S;
global imageName;
global IMAGE_DIR;

if(isempty(S))
    uiwait(warndlg('No scribbles found!'));
    return
end

% If more than two scribble groups exist
if(length(unique(S(:,1))) <= 1)
     uiwait(errordlg('Minimum 2 Regions needed to save'));
     return;
end
    
%Save under same path name + .mat
matName = strcat(imageName,'.mat');
[file,path] = uiputfile('*.mat','Save file', [IMAGE_DIR matName]);    



save([path file], 'S');
  

disp('Save Complete');

% To save an image of curr image+ scribbles
%F = getframe(handles.axes_scrib);
%Image = frame2im(F);
%imwrite(Image, 'Image.png')

% --------------------------------------------------------------------
function MenuLoad_Callback(hObject, eventdata, handles)
% hObject    handle to MenuLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handlesF = getframe(handles.axes1); and user data (see GUIDATA)
global imageName;
global IMAGE_DIR;
global I;

% Load Params if not done previously
if(isempty(PARAMS.ALPHA) || isempty(PARAMS.SIGMA) || isempty(PARAMS.GAMMA) || isempty(PARAMS.LAMBDA) || isempty(PARAMS.ENERGY_INF))
   warning('Initializing all PARAMS to PARAMS.Init');
   PARAMS.Init;
end

% With a new image, a whole new segmentation should start
btn_clear_all_Callback(0, 0, handles)

[I, imageName] = LoadImage(IMAGE_DIR);

% Show in original window and scribble window
iptsetpref('ImshowBorder','tight');
axes(handles.axes_orig);
imshow(I);
axes(handles.axes_scrib);
imshow(I * CONST.MASKIMAGE_MIN_BRIGHT);

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function MenuExit_Callback(hObject, eventdata, handles)
% hObject    handle to MenuExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all

% --- Executes on button press in btn_calcseg.
function btn_calcseg_Callback(hObject, eventdata, handles)
% hObject    handle to btn_calcseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta;
global imageName;
global S;
global I;

% Close all (old) openend figures
close_open_figures();

%Imagename empty?
if(~exist('imageName', 'var') || length(imageName) <= 1)
    uiwait(errordlg('Image File not loaded properly. Load before proceeding. Returning...'));
    return;
end

%Scribbles empty?
if(~exist('S', 'var') || length(S) < 1)
    uiwait(errordlg('Scribble File not loaded properly. Load before proceeding. Returning...'));
    return;
end

% Resizing for performance
I_resized = ResizeI(I);
S_resized = ResizeS(S);

% If more than x scribblePts are used,
% randomly choose x of them
if(length(S_resized) > CONST.MAX_SCRIBBLE_PTS)
    
    % Drop some random Scribble points for faster performance
    % By choosing n indices with largest random value
    
    choice = questdlg(['Number of ScribblePts (' num2str(length(S_resized)) ') > ' ...
        num2str(CONST.MAX_SCRIBBLE_PTS) '. Randomly use ' num2str(CONST.MAX_SCRIBBLE_PTS) ...
        ' for better Performance?'], 'Warning', 'Yes', 'No', 'Yes');
    
    % Handle response    
    if(strcmp(choice,'Yes'))
        S_resized = ReduceScribbles(S_resized);
    end
    
end

axes(handles.axes_segment);


% Start Segmentation
theta = SegmentImage(I_resized, S_resized);

% Show Result
axes(handles.axes_segment);

% If previously downscaled, upscale again
theta_upscaled = imresize(theta, [size(I,1) size(I,2)]);
% the upscaled image has values in [0, 1] -> Threshold
theta_upscaled = floor(theta_upscaled + 0.5);

ShowSegmentation(I, theta_upscaled);

% --- Executes on button press in btn_gt_icg_load.
function btn_load_gt_icg_Callback(hObject, eventdata, handles)
% hObject    handle to btn_scrib_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Load Params if not done previously
warning('Initializing all PARAMS to Original Value');
PARAMS.InitOriginal;

global gtLabels;
[gtFile, pathName, FilterIndex] = uigetfile({'*.gt', 'GroundTruth File (*.gt)'}, 'Select ICG Groundtruth File', '../../Libraries/icgbench/groundtruth');

if(FilterIndex == 0)
    warning('Nothing selected. Returning...');
    return
end

if(length(gtFile) == 1)
    uiwait(errordlg('GT-file had errors. Returning...'));
    return;
end

% Add path
gtFile = [pathName gtFile];

[gtLabels, ScribbleMap, ~, ~] = readFromFile(gtFile);

% Loaded GT files use other notation so add 1
% (0 represents no group in this impl, not -1)
ScribbleMap = ScribbleMap + 1;
gtLabels = ~gtLabels;

if(any(any(ScribbleMap > CONST.MAX_REGIONS)))
    uiwait(errordlg(['Image has more than the allowed number of ' num2str(CONST.MAX_REGIONS) ' regions. Returning...']));
    return;
end

[~,fileName,~] = fileparts(gtFile);

global imageName;
global I;
global S;

% Example file 'image_0001_9050.gt' should lead to 
% Image name 'image_0001.jpg' with the following line
imageNameSplit = strsplit(fileName, '_');
imageName = [strjoin(imageNameSplit(1:end-1), '_') '.jpg'];

pathNameSplit = strsplit(pathName, '\');
pathName = [strjoin(pathNameSplit(1:end-2), '/') '/images/'];

I = imread([pathName imageName]);

axes(handles.axes_orig);
imshow(I);

axes(handles.axes_scrib);
ScribbleImage = DrawScribbles(ScribbleMap, I);
ShowScribbleMap(I, ScribbleImage);
S = ScribbleMapToIndex(ScribbleMap);

axes(handles.axes_gt);
ShowSegmentation(I, gtLabels);
%imshow(gtLabels);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pushbutton10
activeButton =  get(get(handles.btngrp_scribbleGroup,'SelectedObject'),'Tag');

% Get as integer
scribbleGroup = uint8(str2double(activeButton(length('radiobutton')+1:end)));

global S;
global I;

% Set Color depending on number
currColor = NumberToColor(scribbleGroup);

[height, width, ~] = size(I);

% Get new path from user input
currCurve = get_pencil_curve(currColor);

% Clip X-Axis (2nd column)
currCurve = currCurve((currCurve(:,2) <= height), :);
currCurve = currCurve((currCurve(:,2) > 0), :);
% Clip Y-Axis (1st column)
currCurve = currCurve((currCurve(:,1) <= width), :);
currCurve = currCurve((currCurve(:,1) > 0), :);

% To add indices, no of pixels of whole path needed.
currCurveLen = size(currCurve, 1);
scribbleGroupVector = double(repelem(scribbleGroup, currCurveLen)');

% Merge information into Scribble Output
currScribble = [scribbleGroupVector currCurve(:,2) currCurve(:,1)];

currScribble = unique(currScribble, 'rows');

% Apply brushsize
currScribble = ApplyBrushsize(currScribble, I);

%Append new Scribble to S
if(isempty(S)) 
    S = currScribble;
else
    S = [S; currScribble];
end

% --- Executes on button press
function btn_scrib_load_Callback(hObject, eventdata, handles)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Try to get ScribbleFileName from imageName.
% e.g. 'image.jpg' should have scribbles in 'image.jpg.mat'

% Check if image is loaded properly
global imageName
global IMAGE_DIR
global I

if(~exist('imageName', 'var') || isempty(imageName))
    errordlg('Image not loaded. Returning...')
    return;
end

% Try standard naming convention for scribbleFile name

scribbleFileName = [imageName '.mat'];

S = LoadScribbles(IMAGE_DIR, scribbleFileName);

% Draw Scribbles on top of Image I
if(~exist('I', 'var') || isempty(I))
    errordlg('Image not loaded. Returning...')
    return;
end

ScribbleMap = ScribbleIndexToMap(I, S);
[height, width, ~] = size(I);
axes(handles.axes_scrib);
ScribbleImage = DrawScribbles(ScribbleMap, I);
% Visualize Result
ShowScribbleMap(I, ScribbleImage);

% --- Executes on button press in btn_comptogt.
function btn_comptogt_Callback(hObject, eventdata, handles)
% hObject    handle to btn_comptogt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global imageName;
global gtLabels;
global theta;

if(~exist('gtLabels', 'var'))
    errordlg('GT could not be found. Returning...');
    return;
end

if(~exist('theta', 'var'))
    errordlg('theta could not be found. Returning...');
    return;
end   

% Possible due to resizing of original image, the masks could differ in
% size, so resize the loaded one
if(size(theta, 1) ~= size(gtLabels, 1))
    gtLabels = imresize(gtLabels, size(theta, 1) / size(gtLabels, 1));
    gtLabels = imresize(gtLabels, size(theta, 2) / size(gtLabels, 2));
end

diceScore = calculateDiceScore(theta, gtLabels);
%edgeBasedScore = EvaluateEdgeSimple(theta-1, gtLabels-1);

uiwait(msgbox(['Dice Score: ' num2str(round(diceScore*100, 2)) '%'], 'CreateMode', 'modal'));
   
function close_open_figures()

%close all openend figures if there are any
set(0,'DefaultFigureWindowStyle','normal')
global GUIhandle;
set(GUIhandle, 'HandleVisibility', 'off');
close all;
set(GUIhandle, 'HandleVisibility', 'on');

% --- Executes on button press in btn_gt_load.
function btn_gt_load_Callback(hObject, eventdata, handles)
% hObject    handle to btn_gt_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gtLabels;
global imageName;
global I;

% Try to load GT file automatically, if its not present, open filebox
if(~exist('imageName', 'var') || isempty(imageName))
    errordlg('Image not loaded. Returning...')
    return;
end

imageNameSplit = strsplit(imageName, '.');
gtFileName = strjoin([[strjoin(imageNameSplit(1:end-1), '.') '_gt'] '.' imageNameSplit(end)], '');

if(~exist(gtFileName, 'file'))
    global IMAGE_DIR;
    [gtFileName, gtFilePath, FilterIndex] = uigetfile({'*_gt.png', 'Groundtruth Image (*_gt.png)'}, 'Select File for Groundtruth', IMAGE_DIR);
    if(FilterIndex == 0)
        warning('Nothing selected. Returning...');
        return
    end
    gtFileName = [gtFilePath gtFileName];
else
    uiwait(msgbox('GT File found from Image name.'));
end

gtLabels = imread(gtFileName);

axes(handles.axes_gt);
ShowSegmentation(I, gtLabels);

% --- Executes on button press in btn_calc_edge.
function btn_calc_edge_Callback(hObject, eventdata, handles)
% hObject    handle to btn_calc_edge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gtLabels;
global theta;

if(~exist('gtLabels', 'var'))
    errordlg('GT could not be found. Returning...');
    return;
end

if(~exist('theta', 'var'))
    errordlg('theta could not be found. Returning...');
    return;
end   

% Possible due to resizing of original image, the masks could differ in
% size, so resize the loaded
if(size(theta, 1) ~= size(gtLabels, 1))
    gtLabels = imresize(gtLabels, size(theta, 1) / size(gtLabels, 1));
    gtLabels = imresize(gtLabels, size(theta, 2) / size(gtLabels, 2));
end

score = EvaluateEdge(theta, gtLabels);

uiwait(msgbox(['Edge Score: ' num2str(round(score*100, 2)) '%'], 'CreateMode', 'modal'));

function menuFile_Callback(hObject, eventdata, handles)


% --- Executes on button press in btn_save_segmentation.
function btn_save_segmentation_Callback(hObject, eventdata, handles)
% hObject    handle to btn_save_segmentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path, FilterIndex] = uiputfile('*.png','Save Image File');    

if(FilterIndex == 0)
    warning('Clicked Cancel. Returning...');
    return;
end

F = getframe(handles.axes_segment);
FrameImage = frame2im(F);

% remove border (color = (240, 240, 240)) on top/bottom
ImageNoBorders = FrameImage(~all(FrameImage(:, floor(size(FrameImage, 2)/2), :) == 240, 3), :, :);

% remove border (color = (240, 240, 240)) on left/right
ImageNoBorders = ImageNoBorders(:, ~all(ImageNoBorders(floor(size(ImageNoBorders, 1)/2), :, :) == 240, 3), :);

imwrite(ImageNoBorders,  [path file]);


% --- Executes on button press in btn_save_scribs.
function btn_save_scribs_Callback(hObject, eventdata, handles)
% hObject    handle to btn_save_scribs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path, FilterIndex] = uiputfile('*.png','Save Image File');    

if(FilterIndex == 0)
    warning('Clicked Cancel. Returning...');
    return;
end

F = getframe(handles.axes_scrib);
FrameImage = frame2im(F);

% remove border on top/bottom
ImageNoBorders = FrameImage(~all(FrameImage(:, floor(size(FrameImage, 2)/2), :) == 240, 3), :, :);

% remove border on left/right
ImageNoBorders = ImageNoBorders(:, ~all(ImageNoBorders(floor(size(ImageNoBorders, 1)/2), :, :) == 240, 3), :);

imwrite(ImageNoBorders,  [path file]);

