\documentclass[english, printversion, nomenclature, notitle]{tuvisionthesis} % TUVISION template

% MY packages
\usepackage{algorithm}% http://ctan.org/pkg/algorithm

\usepackage{amsmath}

\usepackage{bbm}

\usepackage{caption}

\usepackage{color}

\usepackage{fancyvrb}

\usepackage{filecontents}

\usepackage{graphicx}

\usepackage{hyperref}
\usepackage{algpseudocode}
\usepackage[capitalise, noabbrev]{cleveref}

\usepackage{mathtools}
\usepackage{marginnote}

\usepackage{pgfplots}
\usepgfplotslibrary{external}
\pgfplotsset{compat=1.12}

\usepackage{pifont}

\usepackage[binary-units=true]{siunitx}

\usepackage{standalone}

\usepackage{subcaption}

\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathreplacing, fit, plotmarks, spy, calc,
external, matrix, positioning}

\usepackage{inputenc}

\usepackage{wasysym}
% End of my packages

% MY Commands
% For matlab2tikz
% proposed in https://tux.ti1.tu-harburg.de/moin/Matlab2Tikz
\newlength\figureheight
\newlength\figurewidth

% change marginpar to marginnote as proposed here: 
% http://tex.stackexchange.com/questions/208662/floats-lost-error/208701#208701
\newcommand{\points}[1]{% Print points in margin
  \leavevmode\marginnote{\makebox[\marginparwidth][r]{[#1]}}\ignorespaces}

% For pseudocode
% http://tex.stackexchange.com/questions/163768/write-pseudo-code-in-latex
\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother

% For Todo list https://tux.ti1.tu-harburg.de/moin/ToDoLaTex
% Command for inserting a todo item
% \newcommand{\todo}[1]{%
%   \tikzexternaldisable
%   % Add to todo list
%   \addcontentsline{tdo}{todo}{\protect{#1}}%
%   %
%   \begin{tikzpicture}[remember picture, baseline=-0.75ex]%
%     \node [coordinate] (inText) {};
%   \end{tikzpicture}%
%   %
%   % Make the margin par
%   \marginnote{%
%     \begin{tikzpicture}[remember picture]%
%       \definecolor{orange}{rgb}{1,0.5,0}
%       \draw node[draw=black, fill=orange, text width = 3cm] (inNote)
%       {#1};%
%     \end{tikzpicture}%
%   }%
%   %
%   \begin{tikzpicture}[remember picture, overlay]%
%     \draw[draw = orange, thick]
%       ([yshift=-0.2cm] inText)
%       -| ([xshift=-0.2cm] inNote.west)
%       -| (inNote.west);
%   \end{tikzpicture}%
%   \tikzexternalenable
%   %
% }%

% \makeatletter 
% \newcommand \listoftodos{\section*{Todo list} \@starttoc{tdo}}
% \newcommand\l@todo[2]{\par\noindent \textit{#2},
% \parbox{10cm}{#1}\par}

% redefine \VerbatimInput from
% http://tex.stackexchange.com/questions/85200/include-data-from-a-txt-verbatim
	\RecustomVerbatimCommand{\VerbatimInput}{VerbatimInput}%
	{fontsize=\footnotesize,
	 %
	 frame=lines,  % top and bottom rule only
	 framesep=2em, % separation between frame and text
	 rulecolor=\color{Gray},
	 %
	 label=\fbox{\color{Black}},
	 labelposition=topline,
	 %
	 commandchars=\|\(\), % escape character and argument delimiters for
	                      % commands within the verbatim
	 commentchar=*        % comment character
	}

% Avoid ``Overful hbox" warnings. From
% http://tex.stackexchange.com/questions/13715/how-to-suppress-overfull-hbox-warnings-up-to-some-maximum
\hfuzz=500.002pt 

% From http://tex.stackexchange.com/questions/5223/command-for-argmin-or-argmax
\DeclareMathOperator*{\argmin}{arg\,min} % Jan Hlavacek
\DeclareMathOperator*{\argmax}{arg\,max} % Jan Hlavacek
\DeclareMathOperator*{\condprob}{\, \vert \,}

\def\colvec#1{\left(\vcenter{\halign{\hfil$##$\hfil\cr \colvecA#1;;}}\right)}
\def\colvecA#1;{\if;#1;\else #1\cr \expandafter \colvecA \fi}


\expandafter\def\expandafter\normalsize\expandafter{%
    \normalsize
    \setlength\abovedisplayskip{40pt}
    \setlength\belowdisplayskip{40pt}
    \setlength\abovedisplayshortskip{40pt}
    \setlength\belowdisplayshortskip{40pt}
}

% Magnifiing images 
%
% From
% http://tex.stackexchange.com/questions/25414/how-to-create-magnified-subfigures-and-corresponding-boxes-for-portions-of-a-lar


\newif\ifblackandwhitecycle
\gdef\patternnumber{0}

\pgfkeys{/tikz/.cd,
    zoombox paths/.style={
        draw=orange,
        very thick
    },
    black and white/.is choice,
    black and white/.default=static,
    black and white/static/.style={ 
        draw=white,   
        zoombox paths/.append style={
            draw=white,
            postaction={
                draw=black,
                loosely dashed
            }
        }
    },
    black and white/static/.code={
        \gdef\patternnumber{1}
    },
    black and white/cycle/.code={
        \blackandwhitecycletrue
        \gdef\patternnumber{1}
    },
    black and white pattern/.is choice,
    black and white pattern/0/.style={},
    black and white pattern/1/.style={    
            draw=white,
            postaction={
                draw=black,
                dash pattern=on 2pt off 2pt
            }
    },
    black and white pattern/2/.style={    
            draw=white,
            postaction={
                draw=black,
                dash pattern=on 4pt off 4pt
            }
    },
    black and white pattern/3/.style={    
            draw=white,
            postaction={
                draw=black,
                dash pattern=on 4pt off 4pt on 1pt off 4pt
            }
    },
    black and white pattern/4/.style={    
            draw=white,
            postaction={
                draw=black,
                dash pattern=on 4pt off 2pt on 2 pt off 2pt on 2 pt off 2pt
            }
    },
    zoomboxarray inner gap/.initial=5pt,
    zoomboxarray columns/.initial=2,
    zoomboxarray rows/.initial=2,
    subfigurename/.initial={},
    figurename/.initial={zoombox},
    zoomboxarray/.style={
        execute at begin picture={
            \begin{scope}[
                spy using outlines={%
                    zoombox paths,
                    width=\imagewidth / \pgfkeysvalueof{/tikz/zoomboxarray columns} - (\pgfkeysvalueof{/tikz/zoomboxarray columns} - 1) / \pgfkeysvalueof{/tikz/zoomboxarray columns} * \pgfkeysvalueof{/tikz/zoomboxarray inner gap} -\pgflinewidth,
                    height=\imageheight / \pgfkeysvalueof{/tikz/zoomboxarray rows} - (\pgfkeysvalueof{/tikz/zoomboxarray rows} - 1) / \pgfkeysvalueof{/tikz/zoomboxarray rows} * \pgfkeysvalueof{/tikz/zoomboxarray inner gap}-\pgflinewidth,
                    magnification=3,
                    every spy on node/.style={
                        zoombox paths
                    },
                    every spy in node/.style={
                        zoombox paths
                    }
                }
            ]
        },
        execute at end picture={
            \end{scope}
            \node at (image.north) [anchor=north,inner sep=0pt] {\subcaptionbox{\label{\pgfkeysvalueof{/tikz/figurename}-image}}{\phantomimage}};
            \node at (zoomboxes container.north) [anchor=north,inner sep=0pt] {\subcaptionbox{\label{\pgfkeysvalueof{/tikz/figurename}-zoom}}{\phantomimage}};
     \gdef\patternnumber{0}
        },
        spymargin/.initial=0.5em,
        zoomboxes xshift/.initial=1,
        zoomboxes right/.code=\pgfkeys{/tikz/zoomboxes xshift=1},
        zoomboxes left/.code=\pgfkeys{/tikz/zoomboxes xshift=-1},
        zoomboxes yshift/.initial=0,
        zoomboxes above/.code={
            \pgfkeys{/tikz/zoomboxes yshift=1},
            \pgfkeys{/tikz/zoomboxes xshift=0}
        },
        zoomboxes below/.code={
            \pgfkeys{/tikz/zoomboxes yshift=-1},
            \pgfkeys{/tikz/zoomboxes xshift=0}
        },
        caption margin/.initial=4ex,
    },
    adjust caption spacing/.code={},
    image container/.style={
        inner sep=0pt,
        at=(image.north),
        anchor=north,
        adjust caption spacing
    },
    zoomboxes container/.style={
        inner sep=0pt,
        at=(image.north),
        anchor=north,
        name=zoomboxes container,
        xshift=\pgfkeysvalueof{/tikz/zoomboxes xshift}*(\imagewidth+\pgfkeysvalueof{/tikz/spymargin}),
        yshift=\pgfkeysvalueof{/tikz/zoomboxes yshift}*(\imageheight+\pgfkeysvalueof{/tikz/spymargin}+\pgfkeysvalueof{/tikz/caption margin}),
        adjust caption spacing
    },
    calculate dimensions/.code={
        \pgfpointdiff{\pgfpointanchor{image}{south west} }{\pgfpointanchor{image}{north east} }
        \pgfgetlastxy{\imagewidth}{\imageheight}
        \global\let\imagewidth=\imagewidth
        \global\let\imageheight=\imageheight
        \gdef\columncount{1}
        \gdef\rowcount{1}
        \gdef\zoomboxcount{1}
    },
    image node/.style={
        inner sep=0pt,
        name=image,
        anchor=south west,
        append after command={
            [calculate dimensions]
            node [image container,subfigurename=\pgfkeysvalueof{/tikz/figurename}-image] {\phantomimage}
            node [zoomboxes container,subfigurename=\pgfkeysvalueof{/tikz/figurename}-zoom] {\phantomimage}
        }
    },
    color code/.style={
        zoombox paths/.append style={draw=#1}
    },
    connect zoomboxes/.style={
    spy connection path={\draw[draw=none,zoombox paths] (tikzspyonnode) -- (tikzspyinnode);}
    },
    help grid code/.code={
        \begin{scope}[
                x={(image.south east)},
                y={(image.north west)},
                font=\footnotesize,
                help lines,
                overlay
            ]
            \foreach \x in {0,1,...,9} { 
                \draw(\x/10,0) -- (\x/10,1);
                \node [anchor=north] at (\x/10,0) {0.\x};
            }
            \foreach \y in {0,1,...,9} {
                \draw(0,\y/10) -- (1,\y/10);                        \node [anchor=east] at (0,\y/10) {0.\y};
            }
        \end{scope}    
    },
    help grid/.style={
        append after command={
            [help grid code]
        }
    },
}

\newcommand\phantomimage{%
    \phantom{%
        \rule{\imagewidth}{\imageheight}%
    }%
}
\newcommand\zoombox[2][]{
    \begin{scope}[zoombox paths]
        \pgfmathsetmacro\xpos{
            (\columncount-1)*(\imagewidth / \pgfkeysvalueof{/tikz/zoomboxarray columns} + \pgfkeysvalueof{/tikz/zoomboxarray inner gap} / \pgfkeysvalueof{/tikz/zoomboxarray columns} ) + \pgflinewidth
        }
        \pgfmathsetmacro\ypos{
            (\rowcount-1)*( \imageheight / \pgfkeysvalueof{/tikz/zoomboxarray rows} + \pgfkeysvalueof{/tikz/zoomboxarray inner gap} / \pgfkeysvalueof{/tikz/zoomboxarray rows} ) + 0.5*\pgflinewidth
        }
        \edef\dospy{\noexpand\spy [
            #1,
            zoombox paths/.append style={
                black and white pattern=\patternnumber
            },
            every spy on node/.append style={#1},
            x=\imagewidth,
            y=\imageheight
        ] on (#2) in node [anchor=north west] at ($(zoomboxes container.north west)+(\xpos pt,-\ypos pt)$);}
        \dospy
        \pgfmathtruncatemacro\pgfmathresult{ifthenelse(\columncount==\pgfkeysvalueof{/tikz/zoomboxarray columns},\rowcount+1,\rowcount)}
        \global\let\rowcount=\pgfmathresult
        \pgfmathtruncatemacro\pgfmathresult{ifthenelse(\columncount==\pgfkeysvalueof{/tikz/zoomboxarray columns},1,\columncount+1)}
        \global\let\columncount=\pgfmathresult
        \ifblackandwhitecycle
            \pgfmathtruncatemacro{\newpatternnumber}{\patternnumber+1}
            \global\edef\patternnumber{\newpatternnumber}
        \fi
    \end{scope}
}

% From %
% http://pleasemakeanote.blogspot.de/2009/08/how-to-highlight-text-in-latex.html
\newcommand{\highlight}[1]{\colorbox{orange}{#1}}

% From
% http://tex.stackexchange.com/questions/671/define-additional-math-operators-to-be-typeset-in-roman
\DeclareMathOperator{\divergence}{div}


% From
% 
\makeatletter
\newcounter{algorithmicH}% New algorithmic-like hyperref counter
\let\oldalgorithmic\algorithmic
\renewcommand{\algorithmic}{%
  \stepcounter{algorithmicH}% Step counter
  \oldalgorithmic}% Do what was always done with algorithmic environment
\renewcommand{\theHALG@line}{ALG@line.\thealgorithmicH.\arabic{ALG@line}}
\makeatother

% Additional dx
\newcommand{\dx}{\,\mathop{dx}}
\newcommand{\BV}{\mathop{BV}}

\pgfplotsset{
    box plot/.style={
        /pgfplots/.cd,
        black,
        only marks,
        mark=-,
        mark size=1em,
        /pgfplots/error bars/.cd,
        y dir=plus,
        y explicit,
    },
    box plot box/.style={
        /pgfplots/error bars/draw error bar/.code 2 args={%
            \draw  ##1 -- ++(1em,0pt) |- ##2 -- ++(-1em,0pt) |- ##1 -- cycle;
        },
        /pgfplots/table/.cd,
        y index=2,
        y error expr={\thisrowno{3}-\thisrowno{2}},
        /pgfplots/box plot
    },
    box plot top whisker/.style={
        /pgfplots/error bars/draw error bar/.code 2 args={%
            \pgfkeysgetvalue{/pgfplots/error bars/error mark}%
            {\pgfplotserrorbarsmark}%
            \pgfkeysgetvalue{/pgfplots/error bars/error mark options}%
            {\pgfplotserrorbarsmarkopts}%
            \path ##1 -- ##2;
        },
        /pgfplots/table/.cd,
        y index=4,
        y error expr={\thisrowno{2}-\thisrowno{4}},
        /pgfplots/box plot
    },
    box plot bottom whisker/.style={
        /pgfplots/error bars/draw error bar/.code 2 args={%
            \pgfkeysgetvalue{/pgfplots/error bars/error mark}%
            {\pgfplotserrorbarsmark}%
            \pgfkeysgetvalue{/pgfplots/error bars/error mark options}%
            {\pgfplotserrorbarsmarkopts}%
            \path ##1 -- ##2;
        },
        /pgfplots/table/.cd,
        y index=5,
        y error expr={\thisrowno{3}-\thisrowno{5}},
        /pgfplots/box plot
    },
    box plot median/.style={
        /pgfplots/box plot
    }
}


%% Appearence

% colors:
\definecolor{myred}{HTML}{EC302A}
\definecolor{myblue}{HTML}{2E489E}

% Table appearence
\renewcommand{\arraystretch}{1.3}

\newcommand{\matlab}{\textsc{Matlab} }
 	 	 	
% End of my commands

\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}

%\tikzexternalize[prefix=tikz/]

% correct appearance of calligraphic letters
\DeclareMathAlphabet{\mathcal}{OMS}{cmsy}{m}{n}

% Information that appears on title page
\author{Patrick Fuhlert}
\title{Variation Based Segmentation of Liver Surface from Monocular
Mini-Laparoscopic Sequences}


\category{Master Thesis}

