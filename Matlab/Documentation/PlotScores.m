function [ handler ] = PlotScores( paramVals, scores, paramName )
%PLOTSCORES Summary of this function goes here
%   Detailed explanation goes here

if(size(scores, 2) > 1)
    scores_mean = mean(scores, 1);
else
    scores_mean = scores;
end



% Is only DiceScore?
if(sum(sum(scores(:,:,2))) == 0)

    handler = semilogx(paramVals, scores_mean(:,:,1) * 100, '-o');
    ylabel('Dice Score [%]')
else    
    handler = figure;
    [hAx,hLine1,hLine2] = plotyy(paramVals,scores_mean(:,:,1), paramVals, scores_mean(:,:,2));
    hLine1.Marker = 'o';
    hLine2.Marker = 'o';
    ylabel(hAx(1),'DiceScore'); % left y-axis
    ylabel(hAx(2),'EdgeScore'); % right y-axis
    legend('DiceScore', 'EdgeScore');
end

grid on
xlabel(paramName)

%% Figure Beautify

%xlim( [0.1 100])

% Convert 10^-1 to 0.1 e.g.
set(gca,'XTickLabel',get(gca,'xtick'));

% Legend is modified in tikz
%legend('a');

%To export:
% matlab2tikz([CONST.TEXFIGUREPATH 'sweep_alpha.tikz'], 'height', '\figureheight', 'width', '\figurewidth');


end

