%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function [img] = setColor(img, mask, col, alpha)
% function [img] = setRegion(img, mask, col)
%
% We set the region in img defined by mask with the color col as color
% overlay. Assuming mask is a binary (logical) mask Mr. Santner. 
%
%**************************************************************************


if(nargin < 4)
    alpha = 0.4;
end

r = img(:,:,1);
g = img(:,:,2);
b = img(:,:,3);

r(mask) = uint8((alpha*col(1))) + (1 - alpha).*r(mask);
g(mask) = uint8((alpha*col(2))) + (1 - alpha).*g(mask);
b(mask) = uint8((alpha*col(3))) + (1 - alpha).*b(mask);

img(:,:,1) = r;
img(:,:,2) = g;
img(:,:,3) = b;

end