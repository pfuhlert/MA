function [score] = GetEdgeMetric(a, b)

    % Maximum Neighborhood for each pixel, see [0] for calculation
    k_neighborhood = ceil((PARAMS.EVAL_A + PARAMS.EVAL_C) / sqrt(2)) + 2;

    % Get Mask sizes
    [height, width] = size(a);

    % Reset score
    score = 0;
    
    %%  Eliminate all pixels without neighboring edges
    B = bwboundaries(a, 4);
    boundaryMap = zeros(size(a));
    
    for m=1:length(B),
       boundary = B{m};
       boundaryMap(sub2ind(size(a), boundary(:,1), boundary(:,2))) = 1;
    end
    
    edge_and_neighbors = logical(conv2(boundaryMap, ones(k_neighborhood * 2 + 1), 'same'));
    
    indices = find(edge_and_neighbors)';
    
    %% Start with leftover pixels
    for i = indices

        % Create Neighborhood by convolution
        N = zeros(height, width);
        N(i) = 1;
        
        % Get corresponding neighbors          
        N = conv2(N, ones(k_neighborhood * 2 + 1), 'same');
        N(i) = 0;
        
        % Get all Neighbor pixels that differ from current pixel mask
        TransitionEdges = N & (a(i) ~= a);

        if(~(any(any(TransitionEdges))))
           %Skip the rest of the loop because nothing will be added
           continue
        end
        
        % Get Weight+/- depending on 2nd mask
        Wplus  =  (TransitionEdges & (b(i) ~= b));
        Wminus  = (TransitionEdges & (b(i) == b));
        
        %Get Indices
        [WplusRows, WplusCols] = find(Wplus == 1);
        [WminusRows, WminusCols] = find(Wminus == 1);
        
        % Get Distances to current Pixel
        [y_i, x_i] = ind2sub(size(a), i); 

        WplusDists =  sqrt((WplusRows  - y_i).^2 + (WplusCols  - x_i).^2);
        WminusDists = sqrt((WminusRows - y_i).^2 + (WminusCols - x_i).^2);      
        
        % Calc Scores from Distance
        WplusScore = GetWplusScore(WplusDists);
        WminusScore = GetWminusScore(WminusDists);
      
        
        % Sum up E        
        score = score +  WplusScore + WminusScore;
        %score = score + length(WplusDists) * 1 + length(WminusDists) * -1;

    end
end
