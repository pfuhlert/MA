function [ xi_new ] = GetNewDual( xi, theta, R )
%GETNEWDUAL Summary of this function goes here
%   xi_n+1 = ProjectionKg(xi_n + TAU_D .* gradient(theta_bar_n)

[height, width, n, ~] = size(xi);
xi_new = zeros(height, width, n, 2);

for i=1:n
    [GradCol, GradRow] = GetGradient(theta(:,:,i));
    xi_new(:,:,i,1) = xi(:,:,i,1) + PARAMS.TAU_D .* GradRow;
    xi_new(:,:,i,2) = xi(:,:,i,2) + PARAMS.TAU_D .* GradCol;
end    
    
xi_new = GetProjectionK(xi_new, R);

end