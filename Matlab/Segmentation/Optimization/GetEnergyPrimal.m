function [ PrimalEnergy ] = GetEnergyPrimal( G, f, theta)
%GETENERGY Calculate Energy as in EQ 25 of [2]

% Third dimension of f, theta or xi equals no of ScribbleGroups
[height, width, n] = size(f);

% Theta should always be on the n-simplex
if( any(any(any(theta > 1 | theta < 0))) ) 
    error('PrimalEnergy is inf.. What now?!');
end

s1 = zeros(height, width);
s2 = zeros(height, width);
for i = 1:n
    
    s1 = s1 + theta(:,:,i) .* f(:,:,i);
    
    [gCol, gRow] = GetGradient(theta(:,:,i));
    GradNorm = sqrt(gCol.^2 + gRow.^2);
    s2 = s2 + GradNorm;

end

PrimalEnergy = sum(sum(PARAMS.LAMBDA .* s1 + G .* s2));

end


