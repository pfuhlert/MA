\chapter{Conclusions and Outlook}

\section{Conclusions}
The goal of this thesis was to investigate the image segmentation algorithm
published in \cite{nieuwenhuis-cremers-pami12_2} and apply it to
monocular mini-laparoscopic image sequences dividing the image in liver and
background pixels. A segmentation algorithm is needed in the the overall project
on 3D liver reconstruction of M. Sc. Sven Painer \cite{PAINER2016variation}
that he follows in his PhD thesis because a reliable classification on pixel
level would lead to less computational effort in the overall algorithm. The
implementation of the algorithm was done in \matlab and was successful.\\

\noindent
The overall implementation features a framework for the segmentation algorithm
that offers a graphical user interface to interact with the user. It allows
interactive scribble placement as well as the calculation of a segmentation and
compare it to two different evaluations. The implemented segmentation algorithm
leads to satisfying results with the aid of user selected scribbles. Thus,
placing additional scribbles after seeing the segmentation result might be
necessary to enhance the segmentation quality. Also, the algorithm was adapted
to mini-laparoscopic image sequences by independent parameter optimization and
a feature to extract glare points from the final segmentation. Additional
suggestions of scribble placement in a laparoscopic image were also given for
future users.

\noindent
To obtain qualitative and quantitative impressions of segmentation results, a
database of $115$ laparoscopic image sequences with corresponding
groundtruth data was manually extracted from laparoscopic videos. This data was
used to measure the algorithm's segmentation quality either with 
a region or edge based evaluation method where the first focuses on the
overlapping regions while the other targets the segmentation's edges.
Furthermore, the parameters of the algorithm were optimized for the special
use in laparoscopic images. The optimization shows slightly improved
segmentation qualities on the test images.\\

\noindent
The segmentation algorithm was also analyzed on
natural images from the Graz benchmark that offers an image database with user
scribbles and groundtruth data. The qualitative evaluation shows satisfying
results on the given images.\\

\noindent
Also, a starting guide for placing scribbles was introduced that helps the user
to place scribbles in the crucial locations to obtain a satisfying result. With this
help, high quality segmentations can be obtained from the laparoscopic images.\\



\section{Outlook}

This thesis does not cover automatic scribble selection as it would be necessary
in the scope of 3D liver reconstruction on video footage. So, this work requires
user interaction in the segmentation process. As a result, the integration in
the overall project concerning video material is a possible next step. Since the
3D reconstruction uses a transformation of certain pixel locations, this
information could be used to automatically find scribbles in a new image of a
sequence from previously selected pixels. Another step would be a robust
implementation that automatically selects scribbles for the laparoscopic image
avoiding all user interaction. This approach was already investigated in
\cite{Sinha.2013}. Since not all selected points of this approach will usually
be classified correctly, the value for $E_\infty$ could be lowered to let
scribbles be assigned to another group than initially intended if this is
necessary in the segmentation process.\\

\noindent
Future work could also focus on a more performance oriented implementation,
mainly to provide a more user friendly interface for interactive input. A
possible solution is to switch to a more sophisticated programming language in terms of
performance and preferably using a well suited image processing toolbox.

\noindent
Also, this work ignored a possible correlation between the algorithm's
parameters in the optimization process. Future work could investigate if this is
valid or do another form of parameter optimization. Especially for
parameter $\alpha$ the use of artificial scribbles may distort the estimation
significantly and should be revisited.\\

\noindent
Another possible modification of the original algorithm is to alter the
regularization term. By taking a closer look at the groundtruth data of the
training set, it becomes clear that all but one image contain only
one connected region with liver pixels and a larger region with background pixels.
While the original regularizer aims on minimizing the complete boundary length
of all segmentations, another more suitable regularization term might also
count the total number of distinct regions in the final segmentation and
penalize additional ones as proposed in \cite{yuan2010tv}. A possible
first step to avoid many small regions can also be done by applying a minimum
region size to the segmentation output. This would omit all regions which are
smaller than a minimum area and reclassify these pixels to the same
label of their surrounding segment.\\

\noindent
Finally, while the main algorithm uses each channel of the RGB image to
calculate the color distance as shown in \cref{eq:colorDist}, a possible
further development could use the CIE94 or CIEDE2000 \cite{melgosa2004relative}
color differences to obtain the color kernels in the dataterm creation. This
color difference transforms the color space in Lab coordinates extracting the
lightness in a separate channel. Due to the fact that different illumination
levels in an image can lead to worse segmentation qualities, this channel could
also get less influence or be neglected in the creation of the color kernels
for the dataterm.
