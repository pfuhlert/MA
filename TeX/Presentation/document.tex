\documentclass[10pt]{beamer}


\usetheme[progressbar=frametitle]{metropolis}

%\addfontfeature{Numbers={Monospaced}}

\definecolor{myred}{HTML}{EC302A}
\definecolor{myblue}{HTML}{2E489E}

\def\colvec#1{\left(\vcenter{\halign{\hfil$##$\hfil\cr \colvecA#1;;}}\right)}
\def\colvecA#1;{\if;#1;\else #1\cr \expandafter \colvecA \fi}

\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{tabu}
\usepackage{colortbl}
\usepackage{multirow}
\usepackage{mathtools}
%\usepackage[ansinew]{inputenc}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage{animate}

\usepackage{siunitx}
\usepackage{xmpmulti}
\usepackage{algorithm,algpseudocode}

\DeclareMathOperator*{\argmin}{arg\,min} % Jan Hlavacek
\DeclareMathOperator*{\argmax}{arg\,max} % Jan Hlavacek

\usetikzlibrary{shapes,arrows,fit,calc,positioning,patterns,
decorations.pathreplacing, decorations.pathmorphing}
\tikzset{
    invisible/.style={opacity=0},
    visible on/.style={alt={#1{}{invisible}}},
    alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
    },
  }
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\hfuzz=500.002pt 

\newlength\figureheight
\newlength\figurewidth

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Variation Based Segmentation of Liver Surface from Monocular
Mini-Laparoscopic Sequences}
\subtitle{Master Thesis}
\date{August 9, 2016}
\author{Patrick Fuhlert}
\institute{Hamburg-Harburg University of Technology}
\titlegraphic{}

%\usepackage[citestyle=authortitle,bibstyle=numeric,url=true,isbn=false,backend=biber,sorting=nyt]{biblatex}

\begin{document}

\maketitle

\begin{frame}{Table of Contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents%[hideallsubsections]
\end{frame}

\section{Introduction}
 
\begin{frame}{Context}

\begin{itemize}
 \setlength\itemsep{10pt} 
  	\item Part of the project ``Variation Based Dense 3D
Reconstruction'' on laparoscopic images
	\item Reduce computational effort by preselecting relevant parts of the image
	\item Main goal: Classify image in liver and background pixels
\end{itemize}

\end{frame}

\begin{frame}{Task}
\begin{itemize}
    \setlength\itemsep{10pt}
  \item Understand and implement the image segmentation
  algorithm published by Nieuwenhuis, Cremers (2013) \cite{nieuwenhuis-cremers-pami12_2}
  \item Adapt the implementation on laparoscopic images
  \item Evaluate the final algorithm
\end{itemize}
\end{frame}

\begin{frame}{Images}

\begin{columns}[T] % align columns
\begin{column}{.65\textwidth}

\vspace{1.3cm}

\begin{itemize}
  \setlength\itemsep{10pt} 
  \item Natural images database from another segmentation approach
  \cite{Santner2010}
  \item<2-> Laparoscopic images extracted from real patients of the UKE
  \item<2-> Manually created groundtruth data   
\end{itemize}

\end{column}%
\hfill%
\begin{column}{.3\textwidth}
\begin{figure}[htp]\centering
	\includegraphics[width=\textwidth]{icg.jpg}
\end{figure} 
\pause
\begin{figure}[htp]\centering
	\includegraphics[width=\textwidth]{liver.png}
\end{figure} 
\end{column}%
\end{columns}

\end{frame}

\section{Segmentation Algorithm}

\begin{frame}{Segmentation}
\begin{columns}[T] % align columns
\begin{column}{.65\textwidth}

	\begin{itemize}
	  \setlength\itemsep{1.5cm}
	  \item Divide Image in \alert{distinct} regions\\ $\Omega \coloneqq
	  {\displaystyle\bigcup_{i=1}^n}\, \Omega_i, \quad \Omega_i \cap \Omega_j = \emptyset, \; \forall i \neq j$

	  \item<2-> Expressed by labeling function\\ 
	  $u:\Omega_i =\{x\;|\;u(x) = i\}$
	\end{itemize}

\end{column}%
\hfill%
\begin{column}{.3\textwidth}
\begin{figure}[htp]\centering
	\input{figure/general_segmentation.tikz}
\end{figure} 
\pause
\begin{figure}[htp]\centering
	\input{figure/general_labeling.tikz}
\end{figure} 
\end{column}%
\end{columns}  
  
\end{frame}

\begin{frame}{Scribbles}
\begin{figure}[htbp]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.45\textwidth]{figure/scribs_bird.png} &
		\includegraphics[width=.45\textwidth]{figure/scribs_starfish.png}\\
	\end{tabular}
  	\caption{Scribbles and Background Dataterm Example}
\end{figure}
\pause
\begin{itemize}
  \item User input is required to resolve the intended segmentation result
  \item User selects and classifies pixels \alert{for each segment}
  \item Scribbles combine color and spatial information
\end{itemize}
\end{frame} 

\begin{frame}{Dataterm}
\begin{itemize}
  \setlength\itemsep{10pt}
  \item Assigns energy to every pixel
  \item Depends on probability to belong to a \alert{specific scribble group}
  \item Probability estimated by kernel density estimation
\end{itemize}
\end{frame} 

\begin{frame}{Kernel Density Estimator}

 \begin{figure}[htp]\centering
	\setlength\figureheight{0.3\textwidth}
	\setlength\figurewidth{0.8\textwidth}
 	\input{figure/kde_10.tikz}
 \end{figure}

\begin{itemize}
  \item Used to estimate the probability density function $\phi(x)$ of
  a random process $X$
  \item<2-> Draw sample set $\{x_1,\ldots, x_n\}$ of $X$
  \item<3-> Create Kernel functions with $\mu_i = x_i$ for every $i$
  \item<4-> Superimpose kernel functions 
  \item<5-> Provably converges for $n \rightarrow \infty$ for iid.
  samples \cite{nadaraya1965non}
\end{itemize}
\end{frame}

\begin{frame}{Probability Estimation}
\begin{figure}[htbp]\centering
		\includegraphics[width=.45\textwidth]{figure/scribs_bird.png}
\end{figure}
\begin{itemize}
  \item Scribbles used to characterize segment
  \item Estimate the probability of other pixels to belong to a label by
  their \alert{color} and \alert{location}
  \item<2-> Formulate KDE from scribbles and estimate for every
  pixel:$$\tilde{\mathcal{P}}(I(x), x\;|\;u(x) = i) =
  \frac{1}{m_i}\sum_{j=1}^{m_i} K_\sigma(I - I_{ij}) \cdot K_\rho(x - x_{ij})$$
  %\item<2-> Bandwidth $\rho$ obtained by \alert{minimum distance} to scribble
  %group
\end{itemize}
\end{frame}

\begin{frame}{Dataterm}

  \begin{figure}[htbp]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.45\textwidth]{figure/ducks_scrib.png} &
		\includegraphics[width=.45\textwidth]{figure/ducks_dataterm_2.png}\\
	\end{tabular}
  	\caption{Scribbles and Background Dataterm Example}
\end{figure}
\begin{itemize}
%   \setlength{\itemsep}{10pt}
  \item Get dataterm energy by taking the \alert{negative natural logarithm}
  $$f_i = -\log \tilde{\mathcal{P}} $$%(I(x), x\;|\;u(x) = i)$$
  \item<2-> Set energy of scribbles to $0$ or $E_\infty$ depending if they
  belong to the current scribble group or not
\end{itemize}
\end{frame}


% \begin{frame}{Dataterm Result}
% \begin{figure}[htbp]\centering
% 	\begin{tabular}{cc}
% 		\includegraphics[width=.45\textwidth]{figure/ducks_scrib.png} &
% 		\includegraphics[width=.45\textwidth]{figure/ducks_dataterm_2.png}\\
% 	\end{tabular}
%   	\caption{Scribbles and Background Dataterm Example}
% \end{figure}
% \end{frame}

\begin{frame}{Dataterm Result}
% \addtocounter{framenumber}{-1}
 	\begin{figure}[htbp]\centering
		\begin{tabular}{cc}
% 			\includegraphics[width=.45\textwidth]{figure/elephant_scrib.png} &
% 			\includegraphics[width=.45\textwidth]{figure/elephant.png} \\
			\includegraphics[width=.45\textwidth]{figure/ducks_scrib.png} &
			\includegraphics[width=.45\textwidth]{figure/ducks_thresh.png} \\
		\end{tabular}
  	\caption{Scribbles and Foreground Thresholded Dataterm
  	Example}	
	\end{figure}  
\end{frame}

\begin{frame}{Regularization}
\begin{itemize}
  \item Introduce a penalty for the total \alert{boundary length}
  \item Make segmentation also favor image edges
  \item<2-> Energy to minimize:
  $$ \mathcal{E}(\Omega) = \lambda \sum_{i = 1}^n \int_{\Omega_i} f_i (x)
  \,\mathop{dx} + \sum_{i = 1}^n \operatorname{Per}_g (\Omega_i)$$
  \item<2-> Minimize energy by choosing appropriate labeling function $u$
  \item<3-> Numerical solution with Primal-Dual Algorithm
  \cite{Pock.2009}
  \begin{itemize}
    \item Convex Relaxation
    \item Gradient Descent
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Algorithm Overview}
	\begin{figure}[htb]\centering
	%###
	\input{figure/org_alg/overview.tex}
	%###
	%\caption{Segmentation Algorithm Overview}
	\label{img:algorithmOverview}
	\end{figure}
\end{frame}

\begin{frame}{Intermediate Results}
\begin{figure}[htp]\centering
% For matlab2tikz
  \animategraphics[width=.5\linewidth]{5}{Optisteps_liver/}{0}{20}
  \caption{Regularization Iteration Process}
  \label{figureLabel}
\end{figure}
\end{frame}

\begin{frame}{Implementation}

\begin{itemize}
   \setlength\itemsep{10pt}  
	\item Matlab Graphical User Interface offers
	\begin{itemize}
	  \item Canvas
	  \item Calculate segmentation
	  \item Comparison
	\end{itemize}
	\item<2-> Laparoscopic image database
	\begin{itemize}
	  \item 115 images
	  \item Manually created groundtruth data
	\end{itemize}
\end{itemize}

\end{frame}

\section{Results}

\begin{frame}{Verification}
\begin{figure}[htb]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.4\textwidth]
		{figure/verification/monochrome/5_scrib.png} &
		\includegraphics[width=.4\textwidth]
		{figure/verification/monochrome/5_seg.png}\\
	\end{tabular}
		\caption{Spatial influence on segmentation results}
\end{figure}
\end{frame}

\begin{frame}{Verification}
\begin{figure}[htb]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.4\textwidth]
		{figure/verification/checkerboard/1_scrib.png} &
		\includegraphics[width=.4\textwidth]
		{figure/verification/checkerboard/1_seg.png}\\
	\end{tabular}
		\caption{Color influence on segmentation results}
\end{figure}
\end{frame}

\begin{frame}{Qualitative Results}

 	\begin{figure}[htbp]\centering
		\begin{tabular}{cc}
		\includegraphics[width=.45\textwidth]{figure/qualitative/butterfly_scrib} &
		\includegraphics[width=.45\textwidth]{figure/qualitative/butterfly_seg}\\
		\includegraphics[width=.45\textwidth]{figure/qualitative/stier_scrib} &
		\includegraphics[width=.45\textwidth]{figure/qualitative/stier_seg}
	\end{tabular}
		\caption{Segmentation results on natural images}
\end{figure}
\end{frame}

\begin{frame}{Qualitative Results}

 	\begin{figure}[htbp]\centering
		\begin{tabular}{cc}
 			\includegraphics[width=.33\textwidth]{figure/qualitative_liver/3_scrib.png}
 			& \includegraphics[width=.33\textwidth]{figure/qualitative_liver/3_seg.png}
 			\\
 			\includegraphics[width=.33\textwidth]{figure/qualitative_liver/17_scrib.png}
 			& \includegraphics[width=.33\textwidth]{figure/qualitative_liver/17_seg.png}
 			\\
		\end{tabular}
	\caption{Satisfying Segmentation Results}
	\end{figure}
\end{frame}

\begin{frame}{Scribble Placement}
\begin{figure}[htb]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/18_scrib.png}
		&
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/18_seg.png}
		\\
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/20_scrib.png}
		&
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/20_seg.png}
		\\
	\end{tabular}
	\caption{Unsuccessful scribble placing and suggested improvement}
	\label{fig:wrongscribbles}
\end{figure}
\end{frame}

\begin{frame}{Quantitative Results}

\begin{itemize}
   \setlength\itemsep{10pt} 
  \item Analyzed laparoscopic image database
  \item Performance enhancement by
	\begin{itemize}
	  \item Resize factor
	  \item Number of Scribbles
	\end{itemize}
  \item Evaluation scores obtained by $\operatorname{DiceScore}(A,B) =
  \frac{2 |A \cap B|}{|A| + |B|}$ \cite{Santner2010}
\end{itemize}


\end{frame}

\begin{frame}{Quantitative Results}

\begin{figure}[htb]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
	\input{figure/performance/time_resize_fac.tex}
	\caption{Algorithm Runtime for Varying Resize Factors}
\end{figure}

\end{frame}

\begin{frame}{Quantitative Results}

\begin{figure}[htb]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.9\textwidth}
	\input{figure/performance/time_scribble_no.tex}
	\caption{Algorithm Runtime for Varying Number of Scribbles}
\end{figure}

\end{frame}

\section{Conclusions and Outlook}

\begin{frame}{Conclusions}
\begin{itemize}
  \setlength\itemsep{10pt}
  \item Successful implementation of a framework for the published segmentation
  algorithm in Matlab
	\begin{itemize}
	  \item User interaction possible (Iterative results)
	  \item Evaluation
	\end{itemize}
	\item Laparoscopic database with 115 images and groundtruth data
  	\item Algorithm Adaption to laparoscopic images
	\begin{itemize}
	  \item Parameter optimization
	  \item Glare point extraction
	\end{itemize}

\end{itemize}
\end{frame}

\begin{frame}{Outlook}

\begin{itemize}
  \setlength\itemsep{10pt}
  \item Automatic Scribble selection to avoid user interaction
  \item Exploit spatial information of videos
  \item Performance oriented implementation
  \item Alternative regularization term
\end{itemize}
\end{frame}
   
\plain{Questions?}    

\appendix

\begin{frame}<beamer>[plain, noframenumbering]{Glare Point Extraction}
\begin{figure}[htbp]\centering
		\includegraphics[width=.33\textwidth]{figure/glarept/scrib2.png}
\end{figure}
\begin{itemize}
  \item Reflections of camera lighting
  \item These regions cannot be used in 3D reconstruction
  \item Automatic detection favorable
  \item<2-> Automatic scribbles placed using hysteresis
  \begin{itemize}
	  \item Include all pixels with brightness $B > T_1$
	  \item Add all pixels with brightness $B > T_2$ in surrounding region
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}<beamer>[plain, noframenumbering]{Glare Point Extraction}
\begin{figure}[htbp]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.45\textwidth]{figure/glarept/scrib2.png} &
		\includegraphics[width=.45\textwidth]{figure/glarept/seg2.png}\\
	\end{tabular}
  	\caption{Glare Point Extraction}
\end{figure}
\end{frame}

\begin{frame}<beamer>[plain, noframenumbering]{Scribble Placement}
\begin{figure}[htb]\centering
	\begin{tabular}{cc}
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/32_scrib.png}
		&
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/32_seg.png}
		\\
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/33_scrib.png}
		&
		\includegraphics[width=.33\textwidth]{figure/qualitative_liver/scrib_improve/33_seg.png}
		\\
	\end{tabular}
	\caption{Unsuccessful scribble placing and suggested improvement}
	\label{fig:wrongscribbles}
\end{figure}
\end{frame}

\begin{frame}<beamer>[plain, noframenumbering]{Primal-Dual Algorithm}

The Primal-Dual Algorithm has three steps:

\begin{subequations}
\begin{alignat}{5}\label{eq:primdual}
 & \xi^{t+1}			= \Pi_{\mathcal{K}_g} &&\left( \xi^t + \tau_d \frac{\partial
 \mathcal{E}}{\partial \xi} \right) &&= \Pi_{\mathcal{K}_g} (\xi^t + \tau_d
 \nabla \bar{\theta}^t)\\
 & \theta^{t+1}			= \Pi_{\tilde{\mathcal{B}}} &&\left( \theta^t - \tau_p
 \frac{\partial \mathcal{E}}{\partial \theta} \right)  &&=
 \Pi_{\tilde{\mathcal{B}}} \left( \theta^t + \tau_p (div\,\xi^{t+1} -
 \lambda f) \right) \\
 & \bar{\theta}^{t+1} = && 2\,\theta^{t+1} - \theta^t
 \end{alignat}
\end{subequations}
with the projections
\begin{subequations}
\begin{alignat}{2}
	\Pi_{\mathcal{K}_g}(x) &= \frac{x}{\max(g(x)/2, \lVert x \rVert)},\; x \in
	\mathbb{R}^2\\
	\Pi_{\tilde{\mathcal{B}}}(x) &=  \argmin_{y \in \Delta^{n-1}} \lVert y-x
	\rVert
 \end{alignat}
\end{subequations}

\end{frame}

\begin{frame}<beamer>[plain, noframenumbering]{Parameter
Optimization}
\begin{itemize}
  \item Parameters control Kernel widths, dataterm weight, \ldots
  \item Split Laparoscopic images in test and training set
  \item Optimize parameters on training set
  \item Evaluate on test set
\end{itemize}
\end{frame}
\begin{frame}<beamer>[plain, noframenumbering]{Parameter Optimization - Example}
\begin{figure}[htbp]\centering
    \setlength\figureheight{0.4\textwidth}
    \setlength\figurewidth{0.8\textwidth}
    \input{figure/sweep/sweep_sigma.tex}
    \caption{Parameter Sweep Scores for $\sigma$}
    \label{fig:sweep_sigma}
\end{figure}
\end{frame}
 
 
\begin{frame}<beamer>[plain, noframenumbering, allowframebreaks]
    \frametitle{References}
	\bibliographystyle{alpha}
	\bibliography{masterbib}
\end{frame}
\end{document} 