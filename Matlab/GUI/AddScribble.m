function scribbleList = AddScribble( scribbleList, scribbleGroup )
%ADDSCRIBBLE adds a scribble to scribbleList in group scribbleGroup
global scribbleColors;
global COLUMN_SCRIBBLEGROUP;
global COLUMN_SCRIBBLEELEMENT;

% Set Color depending on number
currColor = scribbleColors(scribbleGroup);

% Get new path from user input
currCurve = get_pencil_curve(currColor);

% Get Subset of all Scribbles in current group
allScribbleGroup = scribbleList(scribbleList(:,COLUMN_SCRIBBLEGROUP) == scribbleGroup,:);   
length(allScribbleGroup)

if(1<1)
    scribbleElement = 1;
else

    % Get all already existing elements and add inc for next element
    scribbleElement = length(unique(allScribbleGroup(:,COLUMN_SCRIBBLEELEMENT))) + 1;
end

% To add keys of group and element, no of pixels of path needed.
currCurveLen = length(currCurve);

% Create Vectors of same value for Group and Element
scribbleGroupVector = repelem(scribbleGroup, currCurveLen)';
scribbleElementVector = repelem(scribbleElement, currCurveLen)';

% Merge information into Scribble Output
currScribble(:,[3 4]) = currCurve;
currScribble(:,COLUMN_SCRIBBLEGROUP) = scribbleGroupVector;
currScribble(:,COLUMN_SCRIBBLEELEMENT) = scribbleElementVector;

%Append new Scribble to scribbleList
if(length(scribbleList) ~= 0) 
    scribbleList = [scribbleList; currScribble];
else
    scribbleList = currScribble;
end

end

