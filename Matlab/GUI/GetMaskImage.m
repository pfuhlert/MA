function [ ImageOut ] = GetMaskImage( I, BinaryMask )
%GETMASKIMAGE Summary of this function goes here
%   Detailed explanation goes here

[height, width, ~] = size(I);

BinaryMask3 = repmat(BinaryMask, 1,1,3);

ImageMask = ones(height, width, 3);

% Background image should be darkened to x% outside region:
ImageMask(BinaryMask3 == 0) = CONST.MASKIMAGE_MIN_BRIGHT;

ImageOut = double(I) .* ImageMask;
ImageOut = uint8(ImageOut);

end

