function [ S ] = LoadScribbles( path, scribbleFileName )
%LOADSCRIBBLES Summary of this function goes here
%   Detailed explanation goes here

if(~exist([path scribbleFileName], 'file'))
    [scribbleFileName, path, FilterIndex] = uigetfile({'*.mat', 'Scribble File (*.mat)'}, 'Select File for Scribbles', path);
    if(FilterIndex == 0)
        warning('Nothing selected. Returning...');
        S = [];
        return
    end    
else
    uiwait(msgbox('Scribble File loaded from Image name.'));
end

% Scribbles loaded in workspace
load([path, scribbleFileName]);

if(~exist('S', 'var') || isempty(S))
    warning('Scribble File not loaded successfully. Returning.');
    return;
end
     
end

