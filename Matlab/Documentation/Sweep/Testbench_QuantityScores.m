%% Prerequisites

clear
clc
close all
addpath(genpath('../../')) 
set(0,'DefaultFigureWindowStyle','Docked')

trainImagesPath = '../../Images/UKE/paramtweak/train/';
gtPath = '../../Images/UKE/images_crop/gt/';

% For current run, create a folder w current datetime
datetime = datestr(now, 'yyyy-mm-dd_HH-MM-SS');
resultsPath = ['../../Results/' datetime '/'];
mkdir(resultsPath);

% Also create a diary log
diary([resultsPath 'log.txt']);
diary on

% Print used Parameters on start of log file
disp(PARAMS)
disp(CONST)
overallTimer = tic;

lambdaValues = [0.0001:0.0001:0.001 0.002:0.001:0.02] ;
alphaValues = [0.5:0.5:10 11:1:15 20 30 40];
gammaValues = [0.5:0.5:10 11:1:15 20 30 40];
sigmaValues = [0.5:0.5:10 11:1:15 20 30 40];
energyValues = [10 25 50 100 150 200 250 300 400 500 750 1000 1250 1500 1750 2000 3000 5000 7500 10000];

allParamValues{1} = lambdaValues;
allParamValues{2} = alphaValues;
allParamValues{3} = gammaValues;
allParamValues{4} = sigmaValues;
allParamValues{5} = energyValues;

paramNames = {'LAMBDA', 'ALPHA', 'GAMMA', 'SIGMA', 'ENERGY_INF'}

for paramNumber = 2:length(allParamValues)
    
    %% Set Parameter and Values 
    % First Reset to original values
    PARAMS.InitOriginal
    paramName = paramNames{paramNumber};
    paramVals = allParamValues{paramNumber};
    
    disp(['Sweeping Parameter ' paramName ' with values:']);
    disp(num2str(paramVals));

    %% Do Calculation for one Parameter
    scores = GetSweepScores(trainImagesPath, gtPath, resultsPath, paramName, paramVals);

    %% Saving, Plotting 
    save([resultsPath 'scores_' paramName], 'scores', 'paramVals');

    handler = PlotScores(paramVals, scores, paramName);
    
    saveas(handler,[resultsPath paramName],'png');
end

toc(overallTimer)
disp('done.');
diary off

% When done, shut down in 10 minutes
%system('shutdown -s -t 600')
