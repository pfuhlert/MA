function [ GT ] = ResizeGT( GT )
%ResizeGT Resize GT

    if((0 < CONST.RESIZE_FACTOR) && (CONST.RESIZE_FACTOR < 1))
        warning(['Using RESIZE_FACTOR = ' num2str(CONST.RESIZE_FACTOR) ' on Groundtruth Image']);
        GT = imresize(GT, CONST.RESIZE_FACTOR, 'nearest');
    end
end



