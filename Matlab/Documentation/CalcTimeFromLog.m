
c1 = [0.4660    0.6740    0.1880];
c2 = [  0.8500    0.3250    0.0980];
    
paramVals = [100    200    300    400    500    600    750   1000   1250   1500   1750   2000   5000  10000];
%paramVals = paramVals.^2 .* 550000;
no_sweepvals = length(paramVals);
no_images = 29;

%% Start
fid = fopen('C:\Users\pfuhl\Desktop\no_scribs_times.txt');
test = textscan(fid, '%s %s %s %s');

A = [test{2} test{4}];

indexOpti = strcmp({A{:,1}}, 'Optimization');

listOptiTimes = {A{indexOpti, 2}};
ttt = cell2mat(listOptiTimes);
times = strsplit(ttt, 's');
D = cellfun(@str2num,times,'Un',0);
OptiTimes = cell2mat(D);


index2 = strcmp({A{:,1}}, 'Dataterm');
listDatatermtimes = {A{index2, 2}};
ttt = cell2mat(listDatatermtimes);
times = strsplit(ttt, 's');
D = cellfun(@str2num,times,'Un',0);
DatatermTimes = cell2mat(D);

% ith row corresponds to ith paramvalue
% 14 parameter, 29 files
DatatermTimes = reshape(DatatermTimes, no_sweepvals,no_images);
OptiTimes = reshape(OptiTimes, no_sweepvals, no_images);

% take sum on rows -> time for each parameter
DatatermCombined = mean(DatatermTimes, 2);
OptiTimesCombined = mean(OptiTimes, 2);





H=bar([DatatermCombined OptiTimesCombined], 'stacked');

  set(H(1),'facecolor',c1);
  set(H(2),'facecolor',c2);


ax = gca;
ax.XTickLabel = paramVals;

xlabel('Number of Scribbles');
ylabel('Avg. Runtime per Image [s]');

legend('Dataterm', 'Optimization', 'Location','northwest')

grid on
