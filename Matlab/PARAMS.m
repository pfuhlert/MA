classdef PARAMS 
%% Other Constants used in the implementation

% Static functions are used instead of constants to get non constant static variables (for parameter sweeping). If this is not necessary, delete the function and uncomment the attributes 
methods (Static)

    function [  ] = Init()
        %Init Sets all parameters to the same value as in [1]

        % Spatial Kernel Width -> Big Value = Less Spatial Influence
        %  via ROH_I = ALPHA * min(||x - x_Scribble||_2)
        PARAMS.ALPHA(30);      % 1.8 in [2], 5 in [3]

        % Color Kernel Width -> Big Value = Less Color Influence
        PARAMS.SIGMA(5);          % 1.3 in [2], 5 in [1]

        % Width of G(x) -> Big Value = More Gradient Influence
        PARAMS.GAMMA(3);          % 5 in [2]        

        % Balance between Dataterm and Regularizer
        %LAMBDA * DATATERM - REGULARIZER
        PARAMS.LAMBDA(0.003);

        % Energy of scribbles not in current segment
        PARAMS.ENERGY_INF(1000); % 1000 in [3]

        % Main iteration abort criterion
        PARAMS.OPTI_PRIMDUAL_MIN_DIFF(0.0005); % Set to -inf to disable      

        % For GUI Scribble Drawing
        PARAMS.BRUSHSIZE(15);    
        PARAMS.BRUSH_DENS(0.2);
    end
    
 function [  ] = InitOriginal()
        %InitOriginal Sets all parameters to the value as in [6]

        % Spatial Kernel Width -> Big Value = Less Spatial Influence
        %  via ROH_I = ALPHA * min(||x - x_Scribble||_2)
        PARAMS.ALPHA(5);      % 1.8 in [2], 5 in [3]

        % Color Kernel Width -> Big Value = Less Color Influence
        PARAMS.SIGMA(5);          % 1.3 in [2], 5 in [1]

        % Width of G(x) -> Big Value = More Edgephile
        PARAMS.GAMMA(5);          % 5 in [2]        

        % Balance between Dataterm and Regularizer
        %LAMBDA * DATATERM - REGULARIZER
        PARAMS.LAMBDA(0.008);

        % Energy of scribbles not in current segment
        PARAMS.ENERGY_INF(1000); % 1000 in [3]

        % Main iteration abort criterion
        PARAMS.OPTI_PRIMDUAL_MIN_DIFF(0.0001); % Set to -inf to disable      

        % For GUI Scribble Drawing
        PARAMS.BRUSHSIZE(5);    
        PARAMS.BRUSH_DENS(0.5);            
    end    

    function [  ] = Get()
        % All static const parameters can be printed like that:
        disp(PARAMS); 
        fprintf('%19s: %f\n', 'ALPHA', PARAMS.ALPHA)
        fprintf('%19s: %f\n', 'SIGMA', PARAMS.SIGMA)
        fprintf('%19s: %f\n', 'GAMMA', PARAMS.GAMMA)
        fprintf('%19s: %f\n', 'LAMBDA', PARAMS.LAMBDA)
        fprintf('%19s: %f\n', 'ENERGY_INF', PARAMS.ENERGY_INF)
        fprintf('%19s: %f\n', 'BRUSHSIZE', PARAMS.BRUSHSIZE)
        fprintf('%19s: %f\n', 'OPTI_PRIMDUAL_MIN_DIFF', PARAMS.OPTI_PRIMDUAL_MIN_DIFF)

    end        
        
        
     function [out] = LAMBDA(val)
        persistent LAMBDA;
        
        if(nargin)
            LAMBDA = val;
            out = LAMBDA;
        else
            out = LAMBDA;
        end
     end
  
      function [out] = ALPHA(val)
         persistent ALPHA;
         
         if(nargin)
             ALPHA = val;
             out = ALPHA;
         else
             out = ALPHA;
         end
      end
   
      function [out] = SIGMA(val)
         persistent SIGMA;
         
         if(nargin)
             SIGMA = val;
             out = SIGMA;
         else
             out = SIGMA;
         end
      end
   
      function [out] = GAMMA(val)
         persistent GAMMA;
         
         if(nargin)
             GAMMA = val;
             out = GAMMA;
         else
             out = GAMMA;
         end
      end   
   

      function [out] = ENERGY_INF(val)
         persistent ENERGY_INF;
         
         if(nargin)
             ENERGY_INF = val;
             out = ENERGY_INF;
         else
             out = ENERGY_INF;
         end
      end 
   
      function [out] = BRUSHSIZE(val)
         persistent BRUSHSIZE;
         
         if(nargin)
             BRUSHSIZE = val;
             out = BRUSHSIZE;
         else
             out = BRUSHSIZE;
         end
      end
      
      function [out] = BRUSH_DENS(val)
         persistent BRUSH_DENS;
         
         if(nargin)
             BRUSH_DENS = val;
             out = BRUSH_DENS;
         else
             out = BRUSH_DENS;
         end
      end
      
      function [out] = OPTI_PRIMDUAL_MIN_DIFF(val)
         persistent OPTI_PRIMDUAL_MIN_DIFF;
         
         if(nargin)
             OPTI_PRIMDUAL_MIN_DIFF = val;
             out = OPTI_PRIMDUAL_MIN_DIFF;
         else
             out = OPTI_PRIMDUAL_MIN_DIFF;
         end
      end        
end  
   



   
    properties (Constant)
        
        %% Segmentation Algorithm
        
            % Spatial Kernel Width -> Big Value = Less Spatial Influence
            %  via ROH_I = ALPHA * min(||x - x_Scribble||_2)
			%ALPHA = 5;      % 1.8 in [2], 5 in [3]
            
            % Color Kernel Width -> Big Value = Less Color Influence
            %SIGMA = 5;          % 1.3 in [2], 5 in [1]
            
            % Width of G(x) -> Big Value = More Edgephile
            %GAMMA = 5;          % 5 in [2]        

            % Balance between Dataterm and Regularizer
            %LAMBDA * DATATERM - REGULARIZER
            %LAMBDA = 0.001;

            % Stepsize Primal Variable
            TAU_P = 0.25;       % 0.25 in [1] and [2]

            % Stepsize Dual Variable
            TAU_D = 0.5;        % 0.5 in [1] and [2]
        
            % Energy of scribbles not in current segment
            %ENERGY_INF = 1000; % 1000 in [3]
            
            % Main iteration abort criterion
            %OPTI_PRIMDUAL_MIN_DIFF = 0.0001; % Set to -inf to disable      
        
            % For GUI Scribble Drawing
            %BRUSHSIZE = 8; 
            %BRUSH_DENS = 0.3;
            
        %% Edge Evaluation as described in [4]
        
            % Distance Curve Parameters
            EVAL_A = 2;
            EVAL_C = 10;
            EVAL_B = (PARAMS.EVAL_A + PARAMS.EVAL_C) / 2;

            % Weight of Precision / Recall 
            F_MEASURE_ALPHA = 0.5;
       
    end
end


        