classdef CONST
%% Other Constants used in the implementation
        
    properties (Constant = true)
        
        %% Performance varying PARAMETERS
        
            % Number of scribble points randomly selected from total scribbles
            MAX_SCRIBBLE_PTS = 1000;

            % Image resize factor
            RESIZE_FACTOR = 0.5;

            % Maximum number of iterations
            OPTI_MAX_STEPS = 100;

            % To not overuse RAM, do Dataterm calculations in Slices
            SLICE_N = 1;        
   
        %% Debugging
            DEBUG_OUTPUT = true;
            OPTI_OUTPUT_EVERY_N = CONST.OPTI_MAX_STEPS / 10;
        
        %% Other 
            % Auto Glarepoint extraction
            EXTRACT_GLAREPOINTS = true;
            % Percentage of extracted scribbles
            GLAREPOINT_SCRIB_PCT = 0.1;

            % Scribble Index Columns
            COL_SGROUP  = 1;
            COL_SROW    = 2;
            COL_SCOL    = 3;

            % Maximum number of regions to be segmented
            MAX_REGIONS = 10;

            % Image Plots for Debugging
            SHOW_IMG_ORG            = false;
            SHOW_IMG_SCRIBBLES      = false;
            SHOW_IMG_DATATERM       = false;
            SHOW_ANIMATE_ITERATION  = false;     
                SAVE_ANIMATE_ITERATION  = false;  % This only works if SHOW_ANIMATE_ITERATION is true
            SHOW_ENERGY_PLOT        = false;
            SHOW_OUTPUT_REGIONS     = false;
  
        %% Show results with these overlays
        
            % Brightness of the image where scribbles are drawn on
            MASKIMAGE_MIN_BRIGHT = 0.8;

            % alpha channel of the image with segmentations on top
            MASKIMAGE_ALPHA = 0.3;

    end
end