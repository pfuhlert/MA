\chapter{Results}
\label{ch:results}
\section{Verification}

The following section shows segmentations of combinations for test case
scribbles and images that give an opportunity to verify the implementation. It
also highlights the influence of the corresponding submodules that can alter
the segmentation result significantly.

\subsection*{Spatial Estimation}

To test the spatial dependency of the algorithm, an all gray image containing no
distinct color information is used. This way, the algorithm should only
consider the spatial distance to the scribbles to check whether a specific pixel
belongs to the region or not. \cref{fig:results_1color} shows that all pixels
that are close to a scribble are selected to belong to the scribble's group
which is consistent with the theoretical approach of the algorithm. Also, the
number of scribbles does not influence the region they are assigned to as shown
in (b) and (c) meaning the normalization of the kernel functions works as
expected. Additionally, the segmentation works for the multiple region case as
depicted in (d) that corresponds to a Voronoi Tesselation \cite{Voronoi1908}.

\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/1_scrib.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/1_seg.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/3_scrib.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/3_seg.png} \\
		\multicolumn{2}{c}{(a)} & \multicolumn{2}{c}{(b)}\\
		
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/4_scrib.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/4_seg.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/5_scrib.png} &
		\includegraphics[width=.2\textwidth]
		{image/verification/monochrome/5_seg.png}\\
		\multicolumn{2}{c}{(c)} & \multicolumn{2}{c}{(d)}\\
		
	\end{tabular}
		\caption{Spatial influence on segmentation results}
	\label{fig:results_1color}
\end{figure}

\subsection*{Color Estimation}

To show the color influence of the algorithm, the spatial dependency of the
pixels is minimized by only choosing two pixels as scribbles 
that are close to each other. \cref{fig:results_checkerboard} shows two
examples of separation only based on color. The image (a), one scribble
is on a light, and one on a dark square of a checkerboard. Image (b) depicts
that even though the blue scribble is closer to the lower right region, it is
assigned to the red segmentation because of its color. The algorithm
correctly segments the image so that all light squares belong to the blue and
all dark squares belong to the red segment. The color of each
pixel is the only valuable criterion for the segmentation result in the given
examples. Moreover, this means that a distinct region that is assigned to a
scribble group does not necessarily need to contain a scribble. 
\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]{image/verification/checkerboard/1_scrib.png}&
		\includegraphics[width=.2\textwidth]{image/verification/checkerboard/1_seg.png}&
		\includegraphics[width=.2\textwidth]{image/verification/checkerboard/2_scrib.png}&
		\includegraphics[width=.2\textwidth]{image/verification/checkerboard/2_seg.png}\\
		\multicolumn{2}{c}{(a)} & \multicolumn{2}{c}{(b)}\\
	\end{tabular}
		\caption{Color influence on segmentation results}
	\label{fig:results_checkerboard}
\end{figure}
\subsection*{Gradient Influence}
The next criterion controls how strongly the segmentation depends
on the original image's gradient magnitude. As shown in
\cref{fig:results_gradient}, the segmentation uses image edges to align the
segmentation result. As shown in the images, neither color nor spatial
information for pixels lets the segmentation prefer the red or the blue segment
around the vertical center of the image. Due to the image gradient,
the black region completely belongs to the red region of the segmentation. This
means that for both images, the black regions completely belong to the
respective red region even though the spatial distance to the blue scribble is
smaller for some pixels. This makes the segmentation align with the image
gradient, which is a more preferable result.\\

\noindent
Note that all of the images were made without aborting the loop by not using the
minimum primal-dual gap size change. The images were calculated by
fixing the optimization algorithm to terminate at a reasonable amount of
steps. Otherwise the algorithm would have terminated earlier and would not
have considered the image edges as actual segmentation boundaries. This
demonstrates that the influence of the gradient is much smaller than the
scribble color and location.

\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]{image/verification/gradient/scrib_1.png}&
		\includegraphics[width=.2\textwidth]{image/verification/gradient/seg_1.png}&
		\includegraphics[width=.2\textwidth]{image/verification/gradient/scrib_2.png}&
		\includegraphics[width=.2\textwidth]{image/verification/gradient/seg_2.png}\\
		\multicolumn{2}{c}{(a)} & \multicolumn{2}{c}{(b)}\\
	\end{tabular}
		\caption{Gradient influence on segmentation results}
	\label{fig:results_gradient}
\end{figure}

\subsection*{Limitations}
As a more general aspect, the segmentation algorithm presented in this work is
not able to find illusory contours in contrast to other segmentation algorithms
like Graph Cut Segmentation as shown in \cite{Bauch.2012}. The proposed
algorithm needs some kind of color, spatial or gradient differences for the
scribble groups to get a successful segmentation result. Lacking all of those
properties, the segmentation fails for illusory contours like the Kanizsa
Triangle \cite{kanizsa19551987} or a similar rectangular example as shown in \cref{fig:results_illusion}.

\begin{figure}[htb]\centering
	\begin{tabular}{ccc}
		\includegraphics[width=.2\textwidth]{image/verification/fail/triangle_scrib.png}
		&
		\includegraphics[width=.2\textwidth]{image/verification/fail/triangle_gt.png}
		&
		\includegraphics[width=.2\textwidth]{image/verification/fail/triangle_seg.png}\\ 
		
		\includegraphics[width=.2\textwidth]{image/verification/fail/quadrat_scrib.png}
		&
		\includegraphics[width=.2\textwidth]{image/verification/fail/quadrat_gt.png}
		&
		\includegraphics[width=.2\textwidth]{image/verification/fail/quadrat_seg.png}\\
		
		(a) Scribbles & (b) Expected & (c) Calculated
	\end{tabular}
		\caption{Illusory contours with expected and calculated segmentations}
	\label{fig:results_illusion}
\end{figure}

\section{Quantitative Results}

\cref{fig:quantitative_results_laparo} shows the segmentation results for the
test data set of the laparoscopic image database containing 86 images with
artificially generated scribbles as introduced in \cref{sec:artificial_scribbles}. The images
were calculated with a resize factor of 0.5 and 1000 scribbles. Since no other
algorithm data for this database exists that could be compared, the optimized
parameter set obtained in \cref{sec:paramopti} is compared to the initial parameter set
of the presented algorithm as depicted in \cref{tab:org_params}. The mean dice
score of the original data set results in $95.31\%$ whereas the adapted
parameter set slightly increases the value to $95.63\%$. The individual results
for each image can be found in \cref{fig:quantitative_results_laparo} and a box
plot of the result is shown in \cref{fig:quantitative_results_boxplot}. It can
be observed that the adapted solution was able to slightly increase the lower
scores of the test set while all values above the median remain nearly
unchanged.

\begin{figure}[htb]\centering
%%%
	\setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.85\textwidth}
	\begin{tabular}{c}
		\input{figure/quant_results/dicescore_images.tex}
	\end{tabular}
	%%%
	\caption{Dice scores for all laparoscopic test images with original
	and adapted parameters}
	\label{fig:quantitative_results_boxplot}
\end{figure}

\begin{figure}[htb]\centering
%%%
	\setlength\figureheight{0.35\textwidth}
    \setlength\figurewidth{0.9\textwidth}
		\input{figure/quant_results/boxplot_diceScores.tex}
	%%%
	\caption{Boxplots on dice scores for original and adapted parameter
	set on laparoscopic test images}
	\label{fig:quantitative_results_laparo}
\end{figure}

\section{Qualitative Results}

This section gives sample images of segmentation results on images with natural
as well as laparoscopic content. Besides small misclassified regions in the
results, the segmentation is valid to the human eye and corresponds to the
selected scribbles.

\subsection*{Natural Images}
\cref{fig:natural_results} gives an impression of the algorithm's segmentation
on natural images from the ICG-Benchmark database from \cref{subsec:icg}. It shows
manually selected scribbles and the corresponding segmentation results. Even
though some errors exist, the algorithm gives good results on natural images
without further parameter optimization. Furthermore, the algorithm also works
in the $n > 2$ regions case. However, misclassifications of small regions exist.
An idea to improve the segmentation quality would be to decrease the minimum
primal-dual gap size $\mathcal{G}_{thresh}$ at cost of computation time.
\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.22\textwidth]{image/qualitative/butterfly_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/butterfly_seg} &
		\includegraphics[width=.22\textwidth]{image/qualitative/stier_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/stier_seg}\\
		\includegraphics[width=.22\textwidth]{image/qualitative/bug_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/bug_seg} &
		\includegraphics[width=.22\textwidth]{image/qualitative/stier2_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/stier2_seg}\\
		\includegraphics[width=.22\textwidth]{image/qualitative/frog_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/frog_seg} &
		\includegraphics[width=.22\textwidth]{image/qualitative/bird_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/bird_seg}\\
		\includegraphics[width=.22\textwidth]{image/qualitative/weg_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/weg_seg} &
		\includegraphics[width=.22\textwidth]{image/qualitative/paprika_scrib} &
		\includegraphics[width=.22\textwidth]{image/qualitative/paprika_seg}\\
		(a) Image & (b) Segmentation & (a) Image & (b) Segmentation
	\end{tabular}
		\caption{Segmentation results on natural images}
	\label{fig:natural_results}
\end{figure}

\subsection*{Laparoscopic Images}

\subsubsection*{Satisfying Segmentations}

Some well segmented images to the human eye and corresponding scribbles are
shown in \cref{fig:laparoscopic_results}. Note that the scribbles followed
the iterative scribble selection approach that was mentioned in
\cref{sec:manualplacement}. Additionally, the iteration process for a
laparoscopic image is visualized in \cref{fig:laparoscopic_results_optisteps}.
It can be seen that even though a large number of distinct regions exist at the
beginning of the optimization, the regularizer helps forming the only relevant
segment containing all liver pixels.

\begin{figure}[htbp]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/11_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/11_seg} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/13_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/13_seg}\\
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/15_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/15_seg} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/2_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/2_seg}\\
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/16_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/16_seg} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/3_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/3_seg}\\
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/4_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/4_seg} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/10_scrib} &
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/10_seg}\\		
		(a) Scribbles & (b) Result & (a) Scribbles & (d) Result
	\end{tabular}
		\caption{Exemplary segmentation results on laparoscopic images}
	\label{fig:laparoscopic_results}
\end{figure}

\begin{figure}[htbp]\centering
	\begin{tabular}{ccc}
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/0.png} &
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/2.png} &
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/4.png}\\
		(a) $t = 1$ & (b) $t = 2$ & (c) $t = 4$\\ 
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/8.png} &
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/12.png} &
		\includegraphics[width=.3\textwidth]{image/Optisteps_liver/20.png}\\
		(d) $t = 8$ & (e) $t = 12$ & (f) $t = 20$\\
	\end{tabular}
		\caption{Intermediate results on a laparoscopic image}
	\label{fig:laparoscopic_results_optisteps}
\end{figure}

\subsubsection*{Glare Point Extraction}
Also, the automatic extraction of glare points delivers satisfactory results
even though it seems to decrease the segmentation quality. Two examples can be
found in \cref{fig:laparoscopic_glare}.
\begin{figure}[htbp]\centering
	\begin{tabular}{ccc}
		\includegraphics[width=.3\textwidth]{image/glarept/scrib1.png} &
		\includegraphics[width=.3\textwidth]{image/glarept/seg1.png}\\
		\includegraphics[width=.3\textwidth]{image/glarept/scrib2.png} &
		\includegraphics[width=.3\textwidth]{image/glarept/seg2.png}\\
		(a) Scribbles & (b) Segmentation\\
	\end{tabular}
		\caption{Segmentation results with glare point extraction}
	\label{fig:laparoscopic_glare}
\end{figure}

\subsubsection*{Critical Test Images}
Although the algorithm mostly leads to good segmentation results, there are
still drawbacks in certain laparoscopic images from the database. Some crucial
images are presented in \cref{fig:laparoscopic_crucial}. These images were
segmented with $1000$ scribbles and a resize factor of $0.5$. Image (a) shows
an example where the regularizer has high influence on the algorithm's output.
The segmented area is very smooth and does not really correspond to
the image content, but has a nearly minimal perimeter. An explanation of this is the
low color difference in the important region between foreground and background.
Next, Image (b) includes a trocar that complicates the segmentation process
affecting the background estimator and the gradient between the two
regions.
As a result, the segmentation only partly covers the liver and includes a large region that clearly belongs to the background. Lastly, Image
(c) lacks of strong gradients between foreground and background at the right
and top part of the liver. As a result, the segmentation in this region is not
clearly defined, however even a fully manual segmentation in this region would
be problematic.\\

\noindent
To conclude, the most crucial images lack of clear color differences between
foreground and background. As a consequence, no clear gradient separating the
regions is present in the images. Furthermore, medical instruments like the
trocar lead to more challenging segmentations.

\begin{figure}[htbp]\centering
	\begin{tabular}{ccc}
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/23_scrib}
		&
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/24_scrib}
		&
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/27_scrib}
		
		\\
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/23_seg}
		&
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/24_seg}
		&
		\includegraphics[width=.3\textwidth]{image/qualitative_liver/crucial/27_seg}
		
		\\
		(a) & (b) & (c)\\
	\end{tabular}
		\caption{Segmentation results on critical test images}
	\label{fig:laparoscopic_crucial}
\end{figure}

\subsubsection*{Artifacts in Dark Regions}
Another characteristic of some segmentation results is found
at the borders of the circular laparoscope where many small regions exist. An
example for that can be seen in \cref{img:org_alg_artifacts}. A possible
explanation could be that the illumination of the image is very low in these
regions leading to a wrong probability estimation of these regions that distort
the dataterm and lead to wrong segmentation results. An idea to
curtail  this problem would be to ignore all segmented regions that are
smaller than a minimum size and relabel them the same as their surrounding.\\

\begin{figure}[htbp]\centering
	\input{figure/org_alg_deficits/artifacts.tex}
	\caption{Small regions on camera borders}
	\label{img:org_alg_artifacts}	
\end{figure}

\subsubsection*{Border Accuracy}

\begin{figure}[htbp]\centering
	\input{figure/org_alg_deficits/gradient_consistency.tex}
	\caption{Imprecise liver edge}
	\label{img:org_alg_gradient_consistency}
\end{figure}

\cref{img:org_alg_gradient_consistency} depicts how the gradient between
liver and background may not be consistent with the segmentation. Note that a
consistent gradient is hard to achieve for example due to blurred images and
partially low brightness levels. To improve the segmentation, additional
scribbles close to the expected gradient for both regions can be added.

\section{Scribble Placement}

As previously mentioned, the scribble placement is most crucial for the
algorithm's segmentation result. \cref{fig:wrongscribbles} illustrates some
examples where the segmentation fails due to wrong placing or not enough
selected pixels. Moreover, a second set of scribbles is shown that improves the
segmentation result. The segmentation depicted in (b) of the first row lacks
scribbles in important liver locations like the darker left side of the liver as
well as in the background below the medical instrument. After adding
scribbles to the mentioned regions, the segmentation result improves
significantly as depicted in Image (d). The background scribbles in Image (a) of
the second row do not cover the background color below the liver pixels. That is
why this region is misclassified by the algorithm as shown in Image (b). After
adding scribbles to the region, the segmentation quality in Image (d) is
increased.
\begin{figure}[htb]\centering
	\begin{tabular}{cccc}
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/18_scrib.png}
		&	
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/18_seg.png}
		&
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/20_scrib.png}
		&
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/20_seg.png}
		\\
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/32_scrib.png}
		&	
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/32_seg.png}
		&
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/33_scrib.png}
		&
		\includegraphics[width=.2\textwidth]{image/qualitative_liver/scrib_improve/33_seg.png}
		\\
		(a) Scribbles & (b) Result & (c) Improved Scribbles & (d) Improved Result
	\end{tabular}
	\caption{Unsuccessful scribble placing and suggested improvement}
	\label{fig:wrongscribbles}
\end{figure}

\section{Performance}
\label{sec:results_performance}

Due to the implementation in \matlab, the computation times are too high to
provide a truly user interactive application. The user has to wait up to several
minutes between intermediate segmentation results to improve the scribble
placement on an ordinary computer. Nonetheless, the performance of the algorithm
was analyzed considering the main parts that influence its overall runtime. 

\subsection*{Resize Factor}
At first, the resize factor is altered in
\cref{fig:performance_results_resolution} resulting in a varying number of
pixels that require computation operations throughout the algorithm. The larger
the image size, the longer takes the optimization and the dataterm creation step
of the algorithm. The average number of pixels in the analyzed images is $N =
0.55$ megapixels. Note that the width and also the height of the
original image is resized using the resize factor. The computation times were
obtained using $1000$ scribbles.
\begin{figure}[htb]\centering 
	\setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.95\textwidth}
	\input{figure/performance/time_resize_fac.tex}
	\caption{Algorithm performance on different resolutions}
	\label{fig:performance_results_resolution}
\end{figure}

\subsection*{Number of Scribbles}
Secondly, the number of scribbles is also crucial for the performance. The
amount of scribbles mainly influences the computation time of the dataterm
construction whereas the optimization time remains approximately constant as
depicted in \cref{fig:performance_results_scribbles}. The analysis was done with
a resize factor of $0.5$.

\begin{figure}[htb]\centering
    \setlength\figureheight{0.3\textwidth}
    \setlength\figurewidth{0.95\textwidth}
	\input{figure/performance/time_scribble_no.tex}
	\caption{Algorithm Runtime for a varying number of scribbles}
	\label{fig:performance_results_scribbles}
\end{figure}

\subsection*{Remark}
The computation of the performance results was done on a powerful
Intel\textsuperscript{\textregistered} Xeon\textsuperscript{\textregistered}
CPU with \SI{2.6}{\giga\hertz} containing $28$ cores and \SI{64}{\giga\byte} of
RAM.
