%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function varargout = icg_multiseg(varargin)
% ICG_MULTISEG M-file for icg_multiseg.fig
%      ICG_MULTISEG, by itself, creates a new ICG_MULTISEG or raises the existing
%      singleton*.
%
%      H = ICG_MULTISEG returns the handle to a new ICG_MULTISEG or the handle to
%      the existing singleton*.
%
%      ICG_MULTISEG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ICG_MULTISEG.M with the given input arguments.
%
%      ICG_MULTISEG('Property','Value',...) creates a new ICG_MULTISEG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before icg_multiseg_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to icg_multiseg_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help icg_multiseg

% Last Modified by GUIDE v2.5 08-Mar-2010 11:04:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @icg_multiseg_OpeningFcn, ...
                   'gui_OutputFcn',  @icg_multiseg_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before icg_multiseg is made visible.
function icg_multiseg_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to icg_multiseg (see VARARGIN)

% Choose default command line output for icg_multiseg
handles.output = hObject;
path(path,'../tools');

handles.data = [];
%Predefine colors 
handles.colors = [255 0 0; ...
                          0 255 0; ...
                          0 0 255; ...
                          255 255 0; ...
                          0 255 255; ...
                          255 0 255; ...
                          170 0 0; ...
                          0 170 0; ...
                          0 0 170; ...
                          170 170 0; ...
                          0 170 170; ...
                          170 0 170; ...
                          85 0 0; ...
                          0 85 0; ...
                          0 0 85; ...
                          85 85 0; ...
                          0 85 85;...
                          85 0 85 ...
                          ];
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes icg_multiseg wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = icg_multiseg_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in load_image_button.
function load_image_button_Callback(hObject, eventdata, handles)
% hObject    handle to load_image_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% clear data structure
handles.data = [];

% get filename
[filename, path] = uigetfile('*.jpg','Select an image');
if isequal(filename, 0)
   return;
else
   set(handles.info_text,'String', sprintf('Loading file %s\n', filename));
end

% load the input image
input_image = imread(fullfile(path, filename));
handles.data.input_image = input_image;
handles.data.input_filename = fullfile(path, filename);

% create labels and seed buffers
handles.data.labels = zeros(size(input_image(:,:,1)));
handles.data.seeds = zeros(size(input_image(:,:,1))) - 1;

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);

% --- Executes on button press in load_gt_button.
function load_gt_button_Callback(hObject, eventdata, handles)
% hObject    handle to load_gt_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get filename
[filename, path] = uigetfile('*.gt','Select a ground truth file');
if isequal(filename, 0)
   return;
else
   set(handles.info_text,'String', sprintf('Trying to load file %s\n', filename));
end

[labels, seeds, user_name, image_name] = readFromFile(fullfile(path, filename));

% clear data structure
handles.data = [];

% load the input image
input_image = imread(fullfile([path '../images'], image_name));
handles.data.input_image = input_image;
handles.data.input_filename = fullfile(path, image_name);

% create labels and seed buffers
handles.data.labels = labels;
handles.data.seeds = seeds;

% set image name and label list
set(handles.edit_user_id, 'String', user_name);
t = num2str(0 : length(unique(labels)) - 1);
list = strread(t, '%s');
set(handles.label_list, 'Value', 1);
set(handles.label_list, 'String', list);

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);



% --- Executes on button press in save_gt_button.
function save_gt_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_gt_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end
if(~isfield(handles.data,'labels'))
    return;
end

% create seeds for the background if necessary
labels = uint8(handles.data.labels);
seeds = handles.data.seeds;
if (sum(seeds == 0) < 1)
    seeds = setRandomizedBackgroundSeeds(labels, seeds);
end

% check if everything is correct
num_labels = length(get(handles.label_list,'String'));
if(num_labels == 1)
    warndlg('You have not created any labels yet');
    return;
end

for i = 1:num_labels
    % are there seeds with that label
    if (sum(seeds == i - 1) < 1)
        warndlg(sprintf('You have not defined any seeds for label %d', i-1));
        return;    
    end    
    % are there regions with that label
    if (sum(labels == i - 1) < 1)
        warndlg(sprintf('You have not defined any regions for label %d', i-1));
        return;    
    end    
end

% save data
user_name = get(handles.edit_user_id, 'String');
[pathstr, filename, ext] = fileparts(handles.data.input_filename);
save_filename = sprintf('%s%s%s_%s.gt',pathstr, filesep, filename, user_name);
saveToFile(save_filename, user_name, [filename ext], labels, seeds)

set(handles.info_text,'String', sprintf('Saved groundtruth file %s sucessfully!', save_filename)); 

% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in label_list.
function label_list_Callback(hObject, eventdata, handles)
% hObject    handle to label_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns label_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from label_list

if(isempty(handles.data));
    return;
end
if(~isfield(handles.data,'labels'))
    return;
end

%get the currently selected label 
cur_label = getCurrentLabelFromListBox(handles);

% refresh display
refreshDisplay(handles.img_axes, handles.data.input_image, ...
               handles.data.labels, handles.data.seeds, handles.colors,...
               cur_label);

% %highlight the current label in the image 
% h = findobj(handles.img_axes, 'Color', 'c');
% delete(h);
% 
% mask = double(handles.data.labels == cur_label);
% if(sum(mask(:)) > 1)
%     ind = find(edge(mask) ~= 0);
%     [x,y] = ind2sub(size(handles.data.labels),ind);
%     hold(handles.img_axes,'on');
%     plot(handles.img_axes, y, x, '.c');
% end

% write status of this label to the infobox
if (cur_label == 0)
    str1 = sprintf('Selected label 0 (background)\nThe background region consists of every pixel not within other regions. You can define seed(s) for this label, if not, random seeds are chosen');
else
    str1 = sprintf('Selected label %d\nYou have to define region(s) and seed(s) for this label', cur_label);
end
exists_labels = sum(handles.data.labels(:) == cur_label) > 0;
exists_seeds = sum(handles.data.seeds(:) == cur_label) > 0;
if (~exists_labels && ~exists_seeds)
    str2 = sprintf('\nAlready defined: nothing');
elseif (exists_labels && ~exists_seeds)
    str2 = sprintf('\nAlready defined: region');
elseif (~exists_labels && exists_seeds)
    str2 = sprintf('\nAlready defined: seeds'); 
else
    str2 = sprintf('\nAlready defined: region and seeds'); 
end
str = strvcat(str1, str2);
set(handles.info_text,'String', str);   

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function label_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to label_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in new_label_button.
function new_label_button_Callback(hObject, eventdata, handles)
% hObject    handle to new_label_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end

list = get(handles.label_list,'String');
num_labels = length(list);
if(~iscell(list))
    list = {list};
end

if(num_labels >= size(handles.colors,1))
    warndlg('Your have reached the maximum number of labels', 'Warning', 'modal');
    return;
end

list{end+1} = num2str(num_labels);
set(handles.label_list, 'Value', num_labels + 1);
set(handles.label_list, 'String', list);

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);



% --- Executes on button press in delete_label_button.
function delete_label_button_Callback(hObject, eventdata, handles)
% hObject    handle to delete_label_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end

%get the highlighted label
cur_label = getCurrentLabelFromListBox(handles);
if(cur_label == 0)
    return;
end

%get the region for that label and update all images
mask = handles.data.labels == cur_label;
handles.data.labels(mask) = 0;

%delete those labels from the seed image too
mask = handles.data.seeds == cur_label;
handles.data.seeds(mask) = -1;

%refresh seed and label mask
mask = handles.data.labels > cur_label;
handles.data.labels(mask) = handles.data.labels(mask) -1 ;

mask = handles.data.seeds > cur_label;
handles.data.seeds(mask) = handles.data.seeds(mask) -1 ;

%refresh the list
list = get(handles.label_list,'String');
num_labels = length(list);

t = num2str(0 : num_labels-2);
list = strread(t, '%s');
set(handles.label_list, 'Value', cur_label);
set(handles.label_list, 'String', list);

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);



% --- Executes on button press in reset_seed_button.
function reset_seed_button_Callback(hObject, eventdata, handles)
% hObject    handle to reset_seed_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end

if(~isfield(handles.data,'seeds'))
    return;
end

cur_label = getCurrentLabelFromListBox(handles);

mask = handles.data.seeds == cur_label;
handles.data.seeds(mask) = -1;

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);


% --- Executes on button press in reset_region_button.
function reset_region_button_Callback(hObject, eventdata, handles)
% hObject    handle to reset_region_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end
if(~isfield(handles.data,'labels'))
    return;
end

cur_label = getCurrentLabelFromListBox(handles);

mask = handles.data.labels == cur_label;
handles.data.labels(mask) = 0;

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);


% --- Executes on button press in add_seed_button.
function add_seed_button_Callback(hObject, eventdata, handles)
% hObject    handle to add_seed_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end

if(~isfield(handles.data,'seeds'))
    return;
end

cur_label = getCurrentLabelFromListBox(handles);

str = sprintf('Define your seed for label %d\n', cur_label);
set(handles.info_text,'String', str);

%pop up the figure
str = sprintf('Define seeds for label %d\n', cur_label);
figh = figure('Name', str);
imshow(handles.data.input_image);
points_h = imfreehand('Closed', false);
points = wait(points_h);
close(figh);


x = [];
y = [];

% add blobs of a certain size
blob_size = 0;
for xb = -blob_size:blob_size
    for yb = -blob_size:blob_size  
        x = [x;points(:,1)+xb];
        y = [y;points(:,2)+yb];
    end
end

mask = x < 1 & y < 1 & x > size(handles.data.input_image,1) & y > size(handles.data.input_image,2);
points = [x y];
points(mask,:)  = [];
for i = 1:length(points)
    x = round(points(i,1));
    y = round(points(i,2));
    handles.data.seeds(y,x) = cur_label;
end

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);


% --- Executes on button press in add_region_button.
function add_region_button_Callback(hObject, eventdata, handles)
% hObject    handle to add_region_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(isempty(handles.data));
    return;
end

if(~isfield(handles.data, 'seeds'))
    return;
end

cur_label = getCurrentLabelFromListBox(handles);

%pop up the axis with image
str = sprintf('Define a region for label %d\n', cur_label);
figh = figure('Name', str);
imshow(handles.data.input_image);

mask = roipoly;
close(figh);

%now fill up the various images with it
handles.data.labels(mask) = cur_label;

%refresh gui display and update handles
label_list_Callback(handles.label_list, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_user_id_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_user_id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_user_id_Callback(hObject, eventdata, handles)
% hObject    handle to edit_user_id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_user_id as text
%        str2double(get(hObject,'String')) returns contents of edit_user_id as a double


function current_label = getCurrentLabelFromListBox(handles)

current_label = [];
%get the highlighted label
index_selected = get(handles.label_list,'Value');
if(isempty(index_selected))
    return;
end

list = get(handles.label_list,'String');
if(length(list) == 1)
    list = {list};
end
item_selected = list{index_selected}; 
current_label = str2double(item_selected); 