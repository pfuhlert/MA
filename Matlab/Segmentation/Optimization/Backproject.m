function [ theta_binarized ] = Backproject( theta )
%BINARIZE theta bei min(argmax_i(u_i(x)) as in [1] section 4.5.3

[height, width, n] = size(theta);

% 3rd dimension maximum should be associated with that group idx
% It also takes only the first occurence of the maximum as idx
[~, Idx] = max(theta, [], 3);

Idx = repmat(Idx, 1,1,n);

theta_binarized = zeros(height, width,  n);

for i = 1:n
    isCurrIdx = (Idx == i);
    theta_binarized(:,:,i) = isCurrIdx(:,:,i);
end

end