%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function image = decodeImageRLE(string, width, height)
% function image = decodeImageRLE(string, width, height)
%
% this function decodes an image encoded with encodeImageRLE. 
% Attention: encodeImageRLE returns a string, this function needs a list

input = str2num(string);

data{1} = input(1:2:end);
data{2} = input(2:2:end);

image_linear = rle(data);
image_t = reshape(image_linear', width, height);
image = image_t';