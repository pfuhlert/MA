addpath(genpath('../'))
clear
close all
set(0,'DefaultFigureWindowStyle','Docked')

%% Train Classifier

path = '../Images/UKE/images_crop_sweep/train/';
[LogRegClassifier, pcaCoeff] = GetSIFTClassifier(path);

%% Test Image

testImageName = '../Images/UKE/images_crop_sweep/test/pat001_Video004_00101.png';

Image = imread(testImageName);
testImage = CropImage(Image);

    % Create artificial frame to get every pixel without borders

    [height, width, ~] = size(testImage);
    [xgrid, ygrid] = meshgrid(1:2:width, 1:2:height);


    fCust = [];
    fCust(1,:) = xgrid(:)';
    fCust(2,:) = ygrid(:)';
    fCust(3,:) = repmat(10, 1, length(xgrid(:)));
    fCust(4,:) = zeros(1, length(xgrid(:)));
    
% Get corresponding descriptors    
[~,d] = vl_sift(im2single(rgb2gray(testImage)), 'frames', fCust);

% Project the test data to same pca dimensions.
dataCentered = bsxfun(@minus,single(d'), mean(single(d)',1));
testDescriptor = dataCentered * pcaCoeff;

Prob = glmval(LogRegClassifier, testDescriptor, 'logit');

ProbImage = ones(height, width) / 2;
ProbImage(sub2ind(size(ProbImage), round(fCust(2,:)), round(fCust(1,:)))) = Prob;

LCandidates = (ProbImage > 0.9);
BCandidates = (ProbImage < 0.1);

%Image = imread(testImage);
%Image = testImage;%ResizeI(Image);

xOffset = ceil((size(Image, 2) - size(LCandidates, 2)) / 2);
yOffset = ceil((size(Image, 1) - size(LCandidates, 1)) / 2);

% Resize Candidates to match image size

SeedMap = LCandidates + 2*single(BCandidates);
SeedMapBig = zeros(size(Image, 1), size(Image, 2));
SeedMapBig(1:size(SeedMap, 1), 1:size(SeedMap, 2)) = SeedMap;
SeedMapBig = circshift(SeedMapBig, [yOffset, xOffset]);
%SeedMapBig(1:200, 1:200) = 2;


S = ScribbleMapToIndex(SeedMapBig);

figure;
ShowScribbleMap(Image, ScribbleIndexToMap(Image, S));

% S = ReduceScribbles(S);
% Image = ResizeI(Image);
% S = ResizeS(S);

%SegmentImage(Image, S);