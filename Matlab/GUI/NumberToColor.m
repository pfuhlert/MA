function [ color ] = NumberToColor( i )
%NOTOCOLOR Convert Number to Color

i = mod(i, 11);

switch(i)
    case 0
        color = [0 0 0];
    case 1
        color = [1 0 0];
    case 2
        color = [0 0 1];
    case 3
        color = [0 1 0];
    case 4
        color = [0 1 1];
    case 5
        color = [1 0 1];
    case 6
        color = [1 1 0];
    case 7
        color = [1 1 1];
    case 8
        color = [0.5 0.5 1];
    case 9
        color = [1 0.5 0.5];
    case 10
        color = [0.5 1 0.5];     
end


end

