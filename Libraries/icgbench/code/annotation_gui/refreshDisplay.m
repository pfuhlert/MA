%
% Copyright (c) ICG. All rights reserved.
%
% Institute for Computer Graphics and Vision
% Graz University of Technology / Austria
%
% This software is distributed WITHOUT ANY WARRANTY; without even
% the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% PURPOSE.  See the above copyright notices for more information.
%
% Author     : Jakob Santner
% EMail      : santner@icg.tugraz.at
%

function [] = refreshDisplay(axis_hdl, input_img, labels, seeds, colors, highlight)
%function [] = refreshDisplay(axis_hdl, input_img, labels, seeds, colors, highlight)

if nargin <6
    highlight = 0
end

% img = input_img;
un_labels = unique(labels(:));
for i = 1 : length(un_labels)    
    %set the region
    mask = labels == un_labels(i);    
    if un_labels(i) == highlight
        input_img = setColor(input_img, mask, colors(un_labels(i) + 1,:), 0.7);
    else
        input_img = setColor(input_img, mask, colors(un_labels(i) + 1,:), 0.3);
    end
end

un_seeds = unique(seeds(:));
for i = 1 : length(un_seeds)   
    %set the seed
    mask = seeds == un_seeds(i);
    if(un_seeds(i) == -1)
        continue;
    end
    input_img = setColor(input_img, mask, colors(un_seeds(i) + 1,:),1.0);     
end

% output ground truth and seeds optionally
% grdtr = uint8(zeros(size(input_img)));
% scrbl = uint8(zeros(size(input_img)));
% for i = 1 : length(un_labels)    
%     %set the region
%     mask = labels == un_labels(i); 
%     grdtr = setColor(grdtr, mask, colors(un_labels(i) + 1,:), 1.0);
% end
% un_seeds = unique(seeds(:));
% for i = 1 : length(un_seeds)   
%     %set the seed
%     mask = seeds == un_seeds(i);
%     if(un_seeds(i) == -1)
%         continue;
%     end
%     scrbl = setColor(scrbl, mask, colors(un_seeds(i) + 1,:),1.0);     
% end
% imwrite(grdtr, 'gt.png');
% imwrite(scrbl, 'sd.png');

imshow(input_img, 'Parent', axis_hdl);

end