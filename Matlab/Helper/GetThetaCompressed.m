function [ theta_compressed ] = GetThetaCompressed( theta )
%GETTHETACOMPRESSED Summary of this function goes here
%   Detailed explanation goes here
% Get Z coordinate as matrix
[~,~,ZCoordinate] = meshgrid(1:size(theta,2), 1:size(theta,1), 1:size(theta,3));

% Multiply with Theta mask
theta_weightedZ = theta .* ZCoordinate;

% The third dimension can be compressed
theta_compressed = sum(theta_weightedZ, 3);

% Reverse order of segments to match colors
theta_compressed = max(max(theta_compressed)) - theta_compressed;


end

